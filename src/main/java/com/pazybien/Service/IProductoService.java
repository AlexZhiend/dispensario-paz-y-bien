package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.Producto;

public interface IProductoService extends ICRUDService<Producto>{
	Producto buscarNombre(String nombre);
	Producto buscarPorID(int id);
	byte[] generarReporteProducto();
	byte[] generarReportePventas(String fechainicio,String fechafin);
	byte[] generarReporteXLSPventas(String fechainicio,String fechafin);
	byte[] generarReporteXLSProductos();
}
