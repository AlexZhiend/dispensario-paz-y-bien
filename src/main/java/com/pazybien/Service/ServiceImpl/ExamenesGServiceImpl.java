package com.pazybien.Service.ServiceImpl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IExamenesGDAO;
import com.pazybien.Model.ExamenesG;
import com.pazybien.Service.IExamenesGService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ExamenesGServiceImpl implements IExamenesGService{

	@Autowired
	private IExamenesGDAO iExamenesGDAO;

	@Override
	public ExamenesG registrar(ExamenesG examenesG) {
		// TODO Auto-generated method stub
		return iExamenesGDAO.save(examenesG);
	}

	@Override
	public ExamenesG actualizar(ExamenesG examenesG) {
		// TODO Auto-generated method stub
		return iExamenesGDAO.save(examenesG);
	}

	@Override
	public List<ExamenesG> listar() {
		// TODO Auto-generated method stub
		return iExamenesGDAO.findAll();
	}

	@Override
	public ExamenesG buscarId(int id) {
		// TODO Auto-generated method stub
		return iExamenesGDAO.findOne(id);
	}

	@Override
	public byte[] generarTotalLab(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/totalLaboratorio.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iExamenesGDAO.reporteTotalExamenes(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
}
