package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IPersonalMedicoDAO;
import com.pazybien.Model.PersonalMedico;
import com.pazybien.Service.IPersonalMedicoService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class PersonalMedicoServiceImpl implements IPersonalMedicoService{

	@Autowired
	private IPersonalMedicoDAO ipersonalmedicodao;

	@Override
	public PersonalMedico registrar(PersonalMedico personalMedico) {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.save(personalMedico);
	}

	@Override
	public PersonalMedico actualizar(PersonalMedico personalMedico) {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.save(personalMedico);
	}

	@Override
	public PersonalMedico buscarId(String identificador) {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.findPersonalMedicoByDnipersonalmedico(identificador);
	}

	@Override
	public List<PersonalMedico> listar() {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.findAll();
	}

	@Override
	public List<PersonalMedico> listarsolomedicos() {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.listarsolomedicos();
	}

	@Override
	public List<PersonalMedico> listarEcoRad() {
		// TODO Auto-generated method stub
		return ipersonalmedicodao.listarRadioEco();
	}

	@Override
	public byte[] generarAtencionEcoRad1(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ecoyrad.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ipersonalmedicodao.medicoEcoRad1(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionEcoRad1XLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ecoyrad.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ipersonalmedicodao.medicoEcoRad1(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionEcoRad2(String fechainicio, String fechafin, String dnipersonalmedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ecoyrad.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ipersonalmedicodao.medicoEcoRad2(fechainicio, fechafin,dnipersonalmedico)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGEcoRad2XLSX(String fechainicio, String fechafin, String dnipersonalmedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ecoyrad.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ipersonalmedicodao.medicoEcoRad2(fechainicio, fechafin,dnipersonalmedico)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	

}
