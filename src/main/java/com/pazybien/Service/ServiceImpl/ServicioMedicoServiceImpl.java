package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IServicioMedicoDAO;
import com.pazybien.Model.ServicioMedico;
import com.pazybien.Service.IServicioMedicoService;


@Service
public class ServicioMedicoServiceImpl implements IServicioMedicoService{

	@Autowired
	private IServicioMedicoDAO iServicioMedicoDAO;

	@Override
	public ServicioMedico registrar(ServicioMedico servicioMedico) {
		// TODO Auto-generated method stub
		return iServicioMedicoDAO.save(servicioMedico);
	}

	@Override
	public ServicioMedico actualizar(ServicioMedico servicioMedico) {
		// TODO Auto-generated method stub
		return iServicioMedicoDAO.save(servicioMedico);
	}

	@Override
	public List<ServicioMedico> listar() {
		// TODO Auto-generated method stub
		return iServicioMedicoDAO.findAll();
	}

	@Override
	public ServicioMedico buscarId(int id) {
		// TODO Auto-generated method stub
		return iServicioMedicoDAO.findOne(id);
	}
	
}
