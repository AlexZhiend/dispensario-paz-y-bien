package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IPacienteDAO;
import com.pazybien.DAO.IResultadosDAO;
import com.pazybien.Model.Paciente;
import com.pazybien.Model.Resultados;
import com.pazybien.Service.IResultadosService;

@Service
public class ResultadosServiceImpl implements IResultadosService{

	@Autowired
	private IResultadosDAO iresultadosdao;
	
	@Autowired
	private IPacienteDAO ipacientedao;
	
	@Override
	public Resultados registrar(Resultados resultados) {
		// TODO Auto-generated method stub
		resultados.getDetalleexamen().forEach(x -> x.setResultados(resultados));
		return iresultadosdao.save(resultados);
	}

	@Override
	public Resultados actualizar(Resultados t) {
		// TODO Auto-generated method stub
		return iresultadosdao.save(t);
	}

	@Override
	public List<Resultados> listar() {
		// TODO Auto-generated method stub
		return iresultadosdao.findAll();
	}

	@Override
	public Resultados buscarId(int id) {
		// TODO Auto-generated method stub
		return iresultadosdao.findOne(id);
	}

	@Override
	public List<Resultados> buscarPorPaciente(int idpaciente) {
		// TODO Auto-generated method stub
		Paciente paciente = ipacientedao.findPacienteByIdpaciente(idpaciente);
		
		return iresultadosdao.findResultadosByPaciente(paciente);
	}

	@Override
	public void eliminarResultados(int id) {
		// TODO Auto-generated method stub
		iresultadosdao.delete(id);
	}

}
