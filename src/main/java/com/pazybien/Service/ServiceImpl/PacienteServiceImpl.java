package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IPacienteDAO;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService{

	@Autowired
	private IPacienteDAO ipacientedao;

	@Override
	public Paciente registrar(Paciente paciente) {
		// TODO Auto-generated method stub
		return ipacientedao.save(paciente);
	}

	@Override
	public Paciente actualizar(Paciente paciente) {
		// TODO Auto-generated method stub
		return ipacientedao.save(paciente);
	}

	@Override
	public Paciente buscarPorid(int id) {
		// TODO Auto-generated method stub
		return ipacientedao.findPacienteByIdpaciente(id);
	}

	@Override
	public List<Paciente> listar() {
		// TODO Auto-generated method stub
		return ipacientedao.findAll();
	}

	@Override
	public Paciente buscarInterno() {
		// TODO Auto-generated method stub
		return ipacientedao.ultimoInterno();
	}

	@Override
	public Paciente buscarExterno() {
		// TODO Auto-generated method stub
		return ipacientedao.ultimoExterno();
	}

	@Override
	public Paciente buscarhcl(int hcl, String nombresyapellidos) {
			
		return ipacientedao.buscarHcl(hcl,nombresyapellidos);
	}

	@Override
	public Paciente buscarId(String identificador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Paciente> BuscarPorNombre(String nombresyapellidos) {
		// TODO Auto-generated method stub
		return ipacientedao.buscarPacientePorNombre(nombresyapellidos);
	}
}
