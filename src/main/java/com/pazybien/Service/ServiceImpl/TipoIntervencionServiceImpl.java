package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.ITipoIntervencionDAO;
import com.pazybien.Model.TipoIntervencion;
import com.pazybien.Service.ITipoIntervencionService;

@Service
public class TipoIntervencionServiceImpl implements ITipoIntervencionService{

	@Autowired
	private ITipoIntervencionDAO itipointervenciondao;

	@Override
	public TipoIntervencion registrar(TipoIntervencion tipoIntervencion) {
		// TODO Auto-generated method stub
		return itipointervenciondao.save(tipoIntervencion);
	}

	@Override
	public TipoIntervencion actualizar(TipoIntervencion tipoIntervencion) {
		// TODO Auto-generated method stub
		return itipointervenciondao.save(tipoIntervencion);
	}

	@Override
	public List<TipoIntervencion> listar() {
		// TODO Auto-generated method stub
		return itipointervenciondao.findAll();
	}

	@Override
	public TipoIntervencion buscarId(int id) {
		// TODO Auto-generated method stub
		return itipointervenciondao.findOne(id);
	}

}
