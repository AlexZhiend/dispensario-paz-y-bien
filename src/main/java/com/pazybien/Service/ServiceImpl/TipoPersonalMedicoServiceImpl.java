package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.ITipoPersonalMedicoDAO;
import com.pazybien.Model.TipoPersonalMedico;
import com.pazybien.Service.ITipoPersonalMedicoService;

@Service
public class TipoPersonalMedicoServiceImpl implements ITipoPersonalMedicoService{

	
	@Autowired
	private ITipoPersonalMedicoDAO itipopersonalmedicodao;

	@Override
	public TipoPersonalMedico registrar(TipoPersonalMedico tipoPersonalMedico) {
		// TODO Auto-generated method stub
		return itipopersonalmedicodao.save(tipoPersonalMedico);
	}

	@Override
	public TipoPersonalMedico actualizar(TipoPersonalMedico tipoPersonalMedico) {
		// TODO Auto-generated method stub
		return itipopersonalmedicodao.save(tipoPersonalMedico);
	}

	@Override
	public List<TipoPersonalMedico> listar() {
		// TODO Auto-generated method stub
		return itipopersonalmedicodao.findAll();
	}

	@Override
	public TipoPersonalMedico buscarId(int id) {
		// TODO Auto-generated method stub
		return itipopersonalmedicodao.findTipoPersonalMedicoByIdtipopersonalmedico(id);
	}
	
}
