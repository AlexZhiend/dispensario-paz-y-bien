package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.ITipoPacienteDAO;
import com.pazybien.Model.TipoPaciente;
import com.pazybien.Service.ITipoPacienteService;

@Service
public class TipoPacienteServiceImpl implements ITipoPacienteService{

	@Autowired
	private ITipoPacienteDAO itipopacientedao;

	@Override
	public TipoPaciente registrar(TipoPaciente tipoPaciente) {
		// TODO Auto-generated method stub
		return itipopacientedao.save(tipoPaciente);
	}

	@Override
	public TipoPaciente actualizar(TipoPaciente tipoPaciente) {
		// TODO Auto-generated method stub
		return itipopacientedao.save(tipoPaciente);
	}

	@Override
	public List<TipoPaciente> listar() {
		// TODO Auto-generated method stub
		return itipopacientedao.findAll();
	}

	@Override
	public TipoPaciente buscarId(int id) {
		// TODO Auto-generated method stub
		return itipopacientedao.findTipoPacienteByIdtipopaciente(id);
	}

}
