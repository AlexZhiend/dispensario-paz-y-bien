package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.ICategoriaExamenMedicoDAO;
import com.pazybien.DAO.IExamenMedicoDAO;
import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.ExamenMedico;
import com.pazybien.Service.IExamenMedicoService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class ExamenMedicoServiceImpl implements IExamenMedicoService{

	@Autowired
	private IExamenMedicoDAO iexamenmedicodao;
	
	@Autowired
	private ICategoriaExamenMedicoDAO icategoriaexamenmedicodao;

	@Override
	public ExamenMedico registrar(ExamenMedico examenMedico) {
		// TODO Auto-generated method stub
		return iexamenmedicodao.save(examenMedico);
	}

	@Override
	public ExamenMedico actualizar(ExamenMedico examenMedico) {
		// TODO Auto-generated method stub
		return iexamenmedicodao.save(examenMedico);
	}

	@Override
	public List<ExamenMedico> listar() {
		// TODO Auto-generated method stub
		return iexamenmedicodao.listarordenado();	
	}

	@Override
	public ExamenMedico buscarId(int id) {
		// TODO Auto-generated method stub
		return iexamenmedicodao.findOne(id);
	}

	@Override
	public List<ExamenMedico> buscarPorCategoria(int id) {
		// TODO Auto-generated method stub
		CategoriaExamenMedico categoriaexamenmedico = icategoriaexamenmedicodao.findCategoriaExamenMedicoByIdcategoriaexamenmedico(id);
		
		return iexamenmedicodao.findExamenMedicoByCategoriaexamenmedico(categoriaexamenmedico);
	}

	@Override
	public byte[] generarReporteGeneral(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/LabReport.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iexamenmedicodao.reporteDeLaboratorioGeneral(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarReporteGeneralXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/LabReport.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iexamenmedicodao.reporteDeLaboratorioGeneral(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	
}
