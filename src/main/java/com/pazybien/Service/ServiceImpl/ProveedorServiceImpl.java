package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IProveedorDAO;
import com.pazybien.Model.Proveedor;
import com.pazybien.Service.IProveedorService;

@Service
public class ProveedorServiceImpl implements IProveedorService{
	
	@Autowired
	private IProveedorDAO iproveedordao;

	@Override
	public Proveedor registrar(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return iproveedordao.save(proveedor);
	}

	@Override
	public Proveedor actualizar(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return iproveedordao.save(proveedor);
	}

	@Override
	public List<Proveedor> listar() {
		// TODO Auto-generated method stub
		return iproveedordao.findAll();
	}

	@Override
	public Proveedor buscarId(int id) {
		// TODO Auto-generated method stub
		return iproveedordao.findOne(id);
	}
	

}
