package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.ICategoriaExamenMedicoDAO;
import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Service.ICategoriaExamenMedicoService;

@Service
public class CategoriaExamenMedicoServiceImpl implements ICategoriaExamenMedicoService{

	@Autowired
	private ICategoriaExamenMedicoDAO icategoriaexamenmedicodao;

	@Override
	public CategoriaExamenMedico registrar(CategoriaExamenMedico categoriaExamenMedico) {
		// TODO Auto-generated method stub
		return icategoriaexamenmedicodao.save(categoriaExamenMedico);
	}

	@Override
	public CategoriaExamenMedico actualizar(CategoriaExamenMedico categoriaExamenMedico) {
		// TODO Auto-generated method stub
		return icategoriaexamenmedicodao.save(categoriaExamenMedico);
	}

	@Override
	public List<CategoriaExamenMedico> listar() {
		// TODO Auto-generated method stub
		return icategoriaexamenmedicodao.findAll();
	}

	@Override
	public CategoriaExamenMedico buscarId(int id) {
		// TODO Auto-generated method stub
		return icategoriaexamenmedicodao.findCategoriaExamenMedicoByIdcategoriaexamenmedico(id);
	}

	@Override
	public List<CategoriaExamenMedico> BuscarCategoriaPorComprobante(int idcomprobantepago) {
		// TODO Auto-generated method stub
		return icategoriaexamenmedicodao.buscarCatePorCompro(idcomprobantepago);
	}
	

}
