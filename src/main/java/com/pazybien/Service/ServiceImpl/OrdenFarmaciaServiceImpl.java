package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IOrdenFarmaciaDAO;
import com.pazybien.EntityReport.TotalxDiaFarmacia;
import com.pazybien.Model.OrdenFarmacia;
import com.pazybien.Service.IOrdenFarmaciaService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class OrdenFarmaciaServiceImpl implements IOrdenFarmaciaService{

	@Autowired
	private IOrdenFarmaciaDAO iOrdenFarmaciaDAO;

	@Override
	public OrdenFarmacia registrar(OrdenFarmacia ordenFarmacia) {
		
		ordenFarmacia.getDetalleordenF().forEach(x -> x.setOrdenfarmacia(ordenFarmacia));
		
		return iOrdenFarmaciaDAO.save(ordenFarmacia);
	}
		
	@Override
	public OrdenFarmacia actualizar(OrdenFarmacia ordenFarmacia) {
		
		ordenFarmacia.getDetalleordenF().forEach(x -> x.setOrdenfarmacia(ordenFarmacia));
		
		return iOrdenFarmaciaDAO.save(ordenFarmacia);
	}

	@Override
	public List<OrdenFarmacia> listar() {
		return iOrdenFarmaciaDAO.listarvigentes();
	}

	@Override
	public OrdenFarmacia buscarultimo() {
		// TODO Auto-generated method stub
		return iOrdenFarmaciaDAO.buscarnumero();
	}

	@Override
	public byte[] generarReporteOrdenFarmacia(int numeroorden) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ordenfarmacia.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.reporteOrdenFPaciente(numeroorden)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public TotalxDiaFarmacia totalxdia(String fecha) {
		// TODO Auto-generated method stub
		return iOrdenFarmaciaDAO.totalxdia(fecha);
	}

	@Override
	public byte[] generarRDetalladoFarmacia(String fechaordenfarmacia) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/farmaciadetallado.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.detalladoFarmacia(fechaordenfarmacia)));
			data = JasperExportManager.exportReportToPdf(print);
            
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarRDetalladoFarmaciaXLSX(String fechaordenfarmacia) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/farmaciadetallado.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.detalladoFarmacia(fechaordenfarmacia)));
			
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public OrdenFarmacia buscarVigente(int numeroorden) {
		// TODO Auto-generated method stub
		return iOrdenFarmaciaDAO.buscarvigentesXnumero(numeroorden);
	}

	@Override
	public byte[] generarNCfarmaciaReport(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.NCFarmaciaReport(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarNCfarmaciaReportXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iOrdenFarmaciaDAO.NCFarmaciaReport(fechainicio, fechafin)));
			
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}


}
