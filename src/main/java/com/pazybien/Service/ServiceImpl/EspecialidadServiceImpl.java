package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IEspecialidadDAO;
import com.pazybien.Model.Especialidad;
import com.pazybien.Service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService{
	
	@Autowired
	private IEspecialidadDAO iespecialidaddao;

	@Override
	public Especialidad registrar(Especialidad especialidad) {
		// TODO Auto-generated method stub
		return iespecialidaddao.save(especialidad);
	}

	@Override
	public Especialidad actualizar(Especialidad especialidad) {
		// TODO Auto-generated method stub
		return iespecialidaddao.save(especialidad);
	}

	@Override
	public List<Especialidad> listar() {
		// TODO Auto-generated method stub
		return iespecialidaddao.findAll();
	}

	@Override
	public Especialidad buscarId(int id) {
		// TODO Auto-generated method stub
		return iespecialidaddao.findEspecialidadByIdespecialidad(id);
	}
	
	
}
