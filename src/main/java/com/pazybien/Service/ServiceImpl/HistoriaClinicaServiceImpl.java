package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IHistoriaClinicaDAO;
import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.EntityReport.Medicoxdia;
import com.pazybien.Model.HistoriaClinica;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.IHistoriaClinicaService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class HistoriaClinicaServiceImpl implements IHistoriaClinicaService{

	@Autowired
	private IHistoriaClinicaDAO ihistoriaclinicadao;
	
	@Override
	public HistoriaClinica registrar(HistoriaClinica historiaClinica) {
		
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.save(historiaClinica);
	}
	
	@Override
	public HistoriaClinica actualizar(HistoriaClinica historiaClinica) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.save(historiaClinica);
	}

	@Override
	public List<HistoriaClinica> listar() {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.findAll();
	}

	@Override
	public HistoriaClinica buscarId(Paciente paciente) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.findHistoriaClinicaByPaciente(paciente);
	}

	@Override
	public byte[] generarHistoria(int idhistoriaclinica) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/histo.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ihistoriaclinicadao.reporteHistoriaPaciente(idhistoriaclinica)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarHistoriaVacia(int idhistoriaclinica) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/pdfhistoria.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null,new JRBeanCollectionDataSource(ihistoriaclinicadao.reporteHistoriaVacia(idhistoriaclinica)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public List<Medicoxdia> listarMedicosxdia(String fechaexpediciconhistoriaclinica) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.reporteMedicoxdia(fechaexpediciconhistoriaclinica);
	}

	@Override
	public void eliminarHistoria(int id) {
		// TODO Auto-generated method stub
		ihistoriaclinicadao.delete(id);
	}

	@Override
	public HistoriaClinica buscarHistoriaPorId(int id) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.findOne(id);
	}

	@Override
	public byte[] RepMedicoxreconsulta(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reconsultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null,new JRBeanCollectionDataSource(ihistoriaclinicadao.medicoReconsulta(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] RepMedicoxreconsultaXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reconsultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ihistoriaclinicadao.medicoReconsulta(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] RepMedicoxreconsulta2(String fechainicio, String fechafin, String reconsulta) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reconsultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null,new JRBeanCollectionDataSource(ihistoriaclinicadao.medicoReconsulta2(fechainicio, fechafin, reconsulta)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] RepMedicoxreconsulta2XLSX(String fechainicio, String fechafin, String reconsulta) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reconsultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(ihistoriaclinicadao.medicoReconsulta2(fechainicio, fechafin, reconsulta)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public List<HistoriaClinica> listarXfecha(String fechainicio, String fechafin) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.listarHistoriaXfecha(fechainicio, fechafin);
	}

	@Override
	public List<HistoriaClinica> listarXPaciente(String nombresyapellidos) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.listarHistoriaXPaciente(nombresyapellidos);
	}

	@Override
	public List<HistoriaClinica> listarporHcl(int hcl) {
		// TODO Auto-generated method stub
		return ihistoriaclinicadao.historiaPorHcl(hcl);
	}



}
