package com.pazybien.Service.ServiceImpl;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IDetalleExamenDAO;
import com.pazybien.Model.DetalleExamen;
import com.pazybien.Service.IDetalleExamenService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class DetalleExamenServiceImpl implements IDetalleExamenService{

	@Autowired
	private IDetalleExamenDAO iDetalleExamenDAO;
	
	@Override
	public DetalleExamen registrar(DetalleExamen detalleExamen) {
		
		return iDetalleExamenDAO.save(detalleExamen);
	}

	@Override
	public DetalleExamen actualizar(DetalleExamen detalleExamen) {

		return iDetalleExamenDAO.save(detalleExamen);
	}

	@Override
	public List<DetalleExamen> listar() {

		return iDetalleExamenDAO.findAll();
	}

	@Override
	public DetalleExamen buscarId(int id) {
		// TODO Auto-generated method stub
		return iDetalleExamenDAO.findOne(id);
	}

	@Override
	public byte[] generarReporteExamenesg(int idpaciente, String fecha) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/ExamenPac.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(iDetalleExamenDAO.reporteExamenesgPaciente(idpaciente,fecha)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}
