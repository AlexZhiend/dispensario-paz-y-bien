package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;


import com.pazybien.DAO.IComprobantePagoDAO;
import com.pazybien.EntityReport.AtencionLabReport;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.IComprobantePagoService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class ComprobantePagoServiceImpl implements IComprobantePagoService{

	@Autowired
	private IComprobantePagoDAO icomprobantemedicodao;
	
	@Override
	public ComprobantePago registrar(ComprobantePago comprobantePago) {
		
		comprobantePago.getDetallecomprobante().forEach(x -> x.setComprobantepago(comprobantePago));
		
		return icomprobantemedicodao.save(comprobantePago);
	}
	
	@Override
	public ComprobantePago actualizar(ComprobantePago comprobantePago) {

		comprobantePago.getDetallecomprobante().forEach(x -> x.setComprobantepago(comprobantePago));
		
		return icomprobantemedicodao.save(comprobantePago);
	}

	public List<ComprobantePago> listar() {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.findAll();
	}

	@Override
	public ComprobantePago buscarPorNumero(int numerorecibocomprobante) {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.BuscarVigentesPorNumero(numerorecibocomprobante);
	}

	@Override
	public ComprobantePago buscarultimo() {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.buscarnumero();
	}

	@Override
	public byte[] generarReporteComprobanteU(int numerorecibocomprobante) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Comprobante.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteComprobanteU(numerorecibocomprobante)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarCajaG(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Caja.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteCajaG(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencion(String fechaatencion) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reporteMedicos.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencion(fechaatencion)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public void eliminarComprobante(int id) {
			
		icomprobantemedicodao.delete(id);
	}

	@Override
	public ComprobantePago buscarCp(int id) {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.findOne(id);
	}

	@Override
	public List<ComprobantePago> listarvigentes() {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.listarvigentes();
	}

	@Override
	public List<AtencionLabReport> listaras(String fechaatlab) {
		// TODO Auto-generated method stub
		return icomprobantemedicodao.ReporteAtencionesdeLab(fechaatlab);
	}

	@Override
	public byte[] generarCajaDetallado(String fechacomprobante) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/CajaDetallado.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.ReporteDetalladoCaja(fechacomprobante)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarCajaGXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Caja.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteCajaG(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionXLSX(String fechaatencion) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/reporteMedicos.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencion(fechaatencion)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarCajaDetalladoXLSX(String fechacomprobante) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/CajaDetallado.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.ReporteDetalladoCaja(fechacomprobante)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionG(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionGeneral(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionGeneral(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGMedicos(String fechainicio, String fechafin, String descripcionserviciomedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionMedicos(fechainicio, fechafin, descripcionserviciomedico)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGXLSXMedicos(String fechainicio, String fechafin, String descripcionserviciomedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionMedicos(fechainicio, fechafin, descripcionserviciomedico)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGServ(String fechainicio, String fechafin, String denominacionserviciomedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionServicio(fechainicio, fechafin, denominacionserviciomedico)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarAtencionGXLSXServ(String fechainicio, String fechafin, String denominacionserviciomedico) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Reportegeneralatencion.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.reporteAtencionServicio(fechainicio, fechafin, denominacionserviciomedico)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarNCCajaciaReport(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.NCFarmaciaReport(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarNCCajaReportXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(icomprobantemedicodao.NCFarmaciaReport(fechainicio, fechafin)));
			
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}
