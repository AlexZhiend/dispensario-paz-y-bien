package com.pazybien.Service.ServiceImpl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IRecaudoDAO;
import com.pazybien.EntityReport.CajaRecaudo;
import com.pazybien.Model.Recaudo;
import com.pazybien.Service.IRecaudoService;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Service
public class RecaudoServiceImpl implements IRecaudoService{
	
	@Autowired
	private IRecaudoDAO irecaudodao;

	@Override
	public Recaudo registrar(Recaudo t) {
		// TODO Auto-generated method stub
		return irecaudodao.save(t);
	}

	@Override
	public Recaudo actualizar(Recaudo t) {
		// TODO Auto-generated method stub
		return irecaudodao.save(t);
	}

	@Override
	public List<Recaudo> listar() {
		// TODO Auto-generated method stub
		return irecaudodao.findAll();
	}

	@Override
	public Recaudo buscarId(int id) {
		// TODO Auto-generated method stub
		return irecaudodao.findOne(id);
	}

	@Override
	public CajaRecaudo recaudoXdiaCaja(String fechacomprobante) {
		// TODO Auto-generated method stub
		return irecaudodao.RecaudoCaja(fechacomprobante);
	}

	@Override
	public Recaudo recaudoXfecha(String fecha) {
		// TODO Auto-generated method stub
		return irecaudodao.RecaudoXfecha(fecha);
	}

	@Override
	public byte[] totalrecaudopdf(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/recaudo.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(irecaudodao.TotalRecaudo(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] totalrecaudoXLSX(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/recaudo.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(irecaudodao.TotalRecaudo(fechainicio, fechafin)));
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] FormatoRecaudo(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/FormatoCaja.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(irecaudodao.TotalRecaudo(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarVentasIngresosReport(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(irecaudodao.TotalVentasIngresos(fechainicio, fechafin)));
			data = JasperExportManager.exportReportToPdf(print);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public byte[] generarVentasIngresosReportXlsx(String fechainicio, String fechafin) {
		byte[] data = null;
		try {
			File file = new ClassPathResource("/reports/Notas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(irecaudodao.TotalVentasIngresos(fechainicio, fechafin)));
			
            ByteArrayOutputStream outputStream1 = new ByteArrayOutputStream();
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream1);
            exporter.exportReport();
			
			data = outputStream1.toByteArray();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
			
}
