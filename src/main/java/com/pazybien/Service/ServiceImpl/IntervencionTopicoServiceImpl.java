package com.pazybien.Service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pazybien.DAO.IIntervencionTopicoDAO;
import com.pazybien.Model.IntervencionTopico;
import com.pazybien.Service.IIntervencionTopicoService;

@Service
public class IntervencionTopicoServiceImpl  implements IIntervencionTopicoService{

	@Autowired
	private IIntervencionTopicoDAO iintervenciontopicodao;
		
	@Override
	public IntervencionTopico registrar(IntervencionTopico intervencionTopico) {
		// TODO Auto-generated method stub
		return iintervenciontopicodao.save(intervencionTopico);
	}

	@Override
	public IntervencionTopico actualizar(IntervencionTopico intervencionTopico) {
		// TODO Auto-generated method stub
		return iintervenciontopicodao.save(intervencionTopico);
	}

	@Override
	public List<IntervencionTopico> listar() {
		// TODO Auto-generated method stub
		return iintervenciontopicodao.findAll();
	}

	@Override
	public IntervencionTopico buscarId(int id) {
		// TODO Auto-generated method stub
		return iintervenciontopicodao.findOne(id);
	}

}
