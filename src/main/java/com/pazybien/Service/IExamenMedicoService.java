package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.ExamenMedico;
import com.pazybien.Model.Paciente;

public interface IExamenMedicoService extends ICRUD2Service<ExamenMedico>{

	List<ExamenMedico> buscarPorCategoria(int id);
	byte[] generarReporteGeneral(String fechainicio,String fechafin);
	byte[] generarReporteGeneralXLSX(String fechainicio,String fechafin);

}
