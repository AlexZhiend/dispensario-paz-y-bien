package com.pazybien.Service;

import com.pazybien.EntityReport.CajaRecaudo;
import com.pazybien.Model.Recaudo;

public interface IRecaudoService extends ICRUD2Service<Recaudo>{
	
	CajaRecaudo recaudoXdiaCaja(String fechacomprobante);
	
	Recaudo recaudoXfecha(String fecha);
	
	byte[] totalrecaudopdf(String fechainicio,String fechafin);
	
	byte[] totalrecaudoXLSX(String fechainicio,String fechafin);
	
	byte[] FormatoRecaudo(String fechainicio,String fechafin);
	
	byte[] generarVentasIngresosReport(String fechainicio,String fechafin);
	byte[] generarVentasIngresosReportXlsx(String fechainicio,String fechafin);
}
