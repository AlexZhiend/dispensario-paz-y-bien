package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.Paciente;

public interface IPacienteService extends ICRUDService<Paciente>{
	
	Paciente buscarInterno();
	Paciente buscarExterno();
	Paciente buscarPorid(int id);
	Paciente buscarhcl(int hcl,String nombresyapellidos);
	List<Paciente> BuscarPorNombre(String nombresyapellidos);
}
