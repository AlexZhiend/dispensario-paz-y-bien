package com.pazybien.Service;

import java.util.List;

import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.EntityReport.Medicoxdia;
import com.pazybien.Model.HistoriaClinica;
import com.pazybien.Model.Paciente;


public interface IHistoriaClinicaService{

	HistoriaClinica registrar(HistoriaClinica historiaClinica); 
	
	HistoriaClinica actualizar(HistoriaClinica historiaClinica);
	
	HistoriaClinica buscarId(Paciente paciente); 
	
	List<HistoriaClinica> listar();
	
	List<HistoriaClinica> listarporHcl(int hcl);
	
	byte[] generarHistoria(int idhistoriaclinica);
	
	byte[] generarHistoriaVacia(int idhistoriaclinica);
	
	List<Medicoxdia> listarMedicosxdia(String fechaexpediciconhistoriaclinica);
	
	byte[] RepMedicoxreconsulta(String fechainicio,String fechafin);
	byte[] RepMedicoxreconsultaXLSX(String fechainicio,String fechafin);
	
	byte[] RepMedicoxreconsulta2(String fechainicio,String fechafin, String reconsulta);
	byte[] RepMedicoxreconsulta2XLSX(String fechainicio,String fechafin, String reconsulta);
	
	void eliminarHistoria(int id);
	
	HistoriaClinica buscarHistoriaPorId(int id);

	List<HistoriaClinica> listarXfecha(String fechainicio,String fechafin);
	
	List<HistoriaClinica> listarXPaciente(String nombresyapellidos);
}