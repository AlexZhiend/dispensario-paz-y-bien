package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.PersonalMedico;

public interface IPersonalMedicoService extends ICRUDService<PersonalMedico>{

	List<PersonalMedico> listarsolomedicos();
	List<PersonalMedico> listarEcoRad();
	
	byte[] generarAtencionEcoRad1(String fechainicio,String fechafin);
	byte[] generarAtencionEcoRad1XLSX(String fechainicio,String fechafin);
	
	byte[] generarAtencionEcoRad2(String fechainicio,String fechafin,String dnipersonalmedico);
	byte[] generarAtencionGEcoRad2XLSX(String fechainicio,String fechafin,String dnipersonalmedico);
	
}
