package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.ExamenesG;

public interface IExamenesGService extends ICRUD2Service<ExamenesG>{

	byte[] generarTotalLab(String fechainicio,String fechafin);

}
