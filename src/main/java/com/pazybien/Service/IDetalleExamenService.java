package com.pazybien.Service;

import com.pazybien.Model.DetalleExamen;

public interface IDetalleExamenService extends ICRUD2Service<DetalleExamen>{
	
	byte[] generarReporteExamenesg(int idpaciente,String fecha);

}
