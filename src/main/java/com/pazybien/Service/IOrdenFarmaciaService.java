package com.pazybien.Service;

import java.util.List;

import com.pazybien.DTO.OrdenListaProductoDTO;
import com.pazybien.EntityReport.TotalxDiaFarmacia;
import com.pazybien.Model.OrdenFarmacia;

public interface IOrdenFarmaciaService{

	OrdenFarmacia actualizar(OrdenFarmacia ordenFarmacia);
	List<OrdenFarmacia> listar();
	OrdenFarmacia registrar(OrdenFarmacia ordenFarmacia);
	OrdenFarmacia buscarultimo();
	OrdenFarmacia buscarVigente(int numeroorden);
	byte[] generarReporteOrdenFarmacia(int numeroorden);
	TotalxDiaFarmacia totalxdia(String fecha);
	byte[] generarRDetalladoFarmacia(String fechaordenfarmacia);
	byte[] generarRDetalladoFarmaciaXLSX(String fechaordenfarmacia);
	byte[] generarNCfarmaciaReport(String fechainicio,String fechafin);
	byte[] generarNCfarmaciaReportXLSX(String fechainicio,String fechafin);
}
