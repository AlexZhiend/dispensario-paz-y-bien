package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.CategoriaExamenMedico;

public interface ICategoriaExamenMedicoService extends ICRUD2Service<CategoriaExamenMedico>{

	List<CategoriaExamenMedico> BuscarCategoriaPorComprobante(int idcomprobantepago);
	
}
