package com.pazybien.Service;

import java.util.List;

import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Model.Resultados;

public interface IResultadosService extends ICRUD2Service<Resultados> {

	List<Resultados> buscarPorPaciente(int idpaciente);
	void eliminarResultados(int id);

}
