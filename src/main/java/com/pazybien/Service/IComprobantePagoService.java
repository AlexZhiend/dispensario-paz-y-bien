package com.pazybien.Service;

import java.util.List;

import com.pazybien.EntityReport.AtencionLabReport;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;

public interface IComprobantePagoService{

	ComprobantePago registrar(ComprobantePago comprobantePago);
	ComprobantePago actualizar(ComprobantePago comprobantePago);
	List<ComprobantePago> listar();
	List<ComprobantePago> listarvigentes();
	ComprobantePago buscarPorNumero(int numerorecibocomprobante);
	ComprobantePago buscarultimo();
	byte[] generarReporteComprobanteU(int numerorecibocomprobante);
	byte[] generarCajaG(String fechainicio,String fechafin);
	byte[] generarAtencion(String fechaatencion);
	void eliminarComprobante(int id);
	ComprobantePago buscarCp(int id);
	List<AtencionLabReport> listaras(String fechaatlab);
	byte[] generarCajaDetallado(String fechacomprobante);
	
	byte[] generarCajaGXLSX(String fechainicio,String fechafin);
	byte[] generarAtencionXLSX(String fechaatencion);
	byte[] generarCajaDetalladoXLSX(String fechacomprobante);
	
	byte[] generarAtencionG(String fechainicio,String fechafin);
	byte[] generarAtencionGXLSX(String fechainicio,String fechafin);
	
	byte[] generarAtencionGMedicos(String fechainicio,String fechafin,String descripcionserviciomedico);
	byte[] generarAtencionGXLSXMedicos(String fechainicio,String fechafin,String descripcionserviciomedico);
	
	byte[] generarAtencionGServ(String fechainicio,String fechafin,String denominacionserviciomedico);
	byte[] generarAtencionGXLSXServ(String fechainicio,String fechafin,String denominacionserviciomedico);
	
	byte[] generarNCCajaciaReport(String fechainicio,String fechafin);
	byte[] generarNCCajaReportXLSX(String fechainicio,String fechafin);
}
