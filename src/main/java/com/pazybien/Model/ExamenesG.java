package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.pazybien.EntityReport.TotalExamenesReport;


@SqlResultSetMappings({
	@SqlResultSetMapping(
		    name = "ReporteTotalExamenes",
		    classes = @ConstructorResult(
		            targetClass = TotalExamenesReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "fecha"),
		                    @ColumnResult(name = "cantidades"),
		                    @ColumnResult(name = "tipo")
		            }
		    	)
			)
	
	}
)

@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteTotalExamenes",resultClass = TotalExamenesReport.class, 
	query = "select count(distinct ah.idanalisisheces) as cantidades, ah.fecha, 'Análisis de heces' as tipo,random() * 1 + 100000 as ids \r\n" + 
			"from analisisheces ah full join  comprobantepago cp on cp.idcomprobantepago = ah.idcomprobante where ah.idanalisisheces != 0 \r\n" + 
			"and ah.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by ah.fecha\r\n" + 
			"union all select count(distinct af.idaglutinacionaf), af.fecha, 'Análisis de aglutinaciones' as tipo,random() * 9999 + 1000 as ids\r\n" + 
			"from aglutinacionaf af full join comprobantepago cp on cp.idcomprobantepago = af.idcomprobante where af.idaglutinacionaf != 0 \r\n" + 
			"and af.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by af.fecha\r\n" + 
			"union all select count(distinct ori.idanalisisorina), ori.fecha, 'Análisis de orina' as tipo,random() * 9999 + 1000 as ids \r\n" + 
			"from analisisorina ori \r\n" + 
			"full join comprobantepago cp on cp.idcomprobantepago = ori.idcomprobante where ori.idanalisisorina != 0 and ori.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by ori.fecha\r\n" + 
			"union all select count(distinct cop.idcorpofuncional), cop.fecha, 'Coprofuncional' as tipo,random() * 9999 + 1000 as ids \r\n" + 
			"from coprofuncional cop\r\n" + 
			"full join comprobantepago cp on cp.idcomprobantepago = cop.idcomprobante where cop.idcorpofuncional !=0 and cop.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') GROUP by cop.fecha\r\n" + 
			"union all select count(distinct hem.idhematograma),hem.fecha, 'Hemograma' as tipo,random() * 9999 + 1000 as ids \r\n" + 
			"from hematograma hem\r\n" + 
			"full join comprobantepago cp on cp.idcomprobantepago = hem.idcomprobante where hem.idhematograma !=0 and hem.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')GROUP by hem.fecha\r\n" + 
			"union all select count(distinct hema.idhematogramacompleto),hema.fecha,'Hemograma completo' as tipo,random() * 9999 + 1000\r\n" + 
			"as ids from hematograma hema\r\n" + 
			"where hema.idhematogramacompleto != 0 and hema.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by hema.fecha\r\n" + 
			"union all select count(distinct sec.idsecrecionesepmb),sec.fecha, 'Análisis de secreciones' as tipo,random() * 9999 + 1000 as ids \r\n" + 
			"from secrecionesepmb sec\r\n" + 
			"full join comprobantepago cp on cp.idcomprobantepago = sec.idcomprobantepago where sec.idsecrecionesepmb !=0 and sec.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by sec.fecha\r\n" + 
			"union all select count(distinct uro.idurocultivo), uro.fecha,'Urocultivo' as tipo,random() * 9999 + 1000 as ids \r\n" + 
			"from urocultivo uro \r\n" + 
			"full join comprobantepago cp on cp.idcomprobantepago = uro.idcomprobantepago where uro.idurocultivo !=0 and uro.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') group by uro.fecha"
	)
})



@Entity
@Table(name = "examenesg")
public class ExamenesG {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idexamenesg;
	
	@Column
	private String denominacion;
	
	@Column
	private String descripcion;

	public int getIdexamenesg() {
		return idexamenesg;
	}

	public void setIdexamenesg(int idexamenesg) {
		this.idexamenesg = idexamenesg;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
