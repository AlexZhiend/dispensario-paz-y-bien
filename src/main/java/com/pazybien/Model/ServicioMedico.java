package com.pazybien.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="serviciomedico")
public class ServicioMedico {	
	
	@Id
	@Column(name = "idserviciomedico")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idserviciomedico;
	
	@Column(name = "denominacionserviciomedico",nullable=false)
	private String denominacionserviciomedico;
	
	@Column(name = "descripcionserviciomedico")
	private String descripcionserviciomedico;
	
	@Column(name = "precioserviciomedico",nullable=false)
	private double precioserviciomedico;

	public int getIdserviciomedico() {
		return idserviciomedico;
	}

	public void setIdserviciomedico(int idserviciomedico) {
		this.idserviciomedico = idserviciomedico;
	}

	public String getDenominacionserviciomedico() {
		return denominacionserviciomedico;
	}

	public void setDenominacionserviciomedico(String denominacionserviciomedico) {
		this.denominacionserviciomedico = denominacionserviciomedico;
	}

	public String getDescripcionserviciomedico() {
		return descripcionserviciomedico;
	}

	public void setDescripcionserviciomedico(String descripcionserviciomedico) {
		this.descripcionserviciomedico = descripcionserviciomedico;
	}

	public double getPrecioserviciomedico() {
		return precioserviciomedico;
	}

	public void setPrecioserviciomedico(double precioserviciomedico) {
		this.precioserviciomedico = precioserviciomedico;
	}

}
