package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="categoriaexamenmedico")
public class CategoriaExamenMedico {
	
	@Id
	@Column(name="idcategoriaexamenmedico")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idcategoriaexamenmedico;
	
	@Column(name="nombrecategoriaexamenmedico")
	private String nombrecategoriaexamenmedico;
	
	@Column(name = "precio")
	private double precio;
	
	public int getIdcategoriaexamenmedico() {
		return idcategoriaexamenmedico;
	}

	public void setIdcategoriaexamenmedico(int idcategoriaexamenmedico) {
		this.idcategoriaexamenmedico = idcategoriaexamenmedico;
	}

	public String getNombrecategoriaexamenmedico() {
		return nombrecategoriaexamenmedico;
	}

	public void setNombrecategoriaexamenmedico(String nombrecategoriaexamenmedico) {
		this.nombrecategoriaexamenmedico = nombrecategoriaexamenmedico;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}
