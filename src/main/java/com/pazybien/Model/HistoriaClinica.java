package com.pazybien.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.HistoriaReport;
import com.pazybien.EntityReport.HvaciaReport;
import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.EntityReport.Medicoxdia;


@SqlResultSetMappings({
	@SqlResultSetMapping(
		    name = "ReporteHistoriaPaciente",
		    classes = @ConstructorResult(
		            targetClass = HistoriaReport.class,
		            columns = {
		            		@ColumnResult(name = "idhistoriaclinica"),
		                    @ColumnResult(name = "nombresyapellidos"),
		                    @ColumnResult(name = "hcl"),
		                    @ColumnResult(name = "edad"),
		                    @ColumnResult(name = "direccionpaciente"),
		                    @ColumnResult(name = "departamentopaciente"),
		                    @ColumnResult(name = "distritopaciente"),
		                    @ColumnResult(name = "provinciapaciente"),
		                    @ColumnResult(name = "fechanacimientopaciente"),
		                    @ColumnResult(name = "ocupacionpaciente"),
		                    @ColumnResult(name = "estadocivilpaciente"),
		                    @ColumnResult(name = "fechaexpediciconhistoriaclinica"),
		                    @ColumnResult(name = "serviciomedico"),
		                    @ColumnResult(name = "nombrespersonalmedico"),
		                    @ColumnResult(name = "anamnesis"),
		                    @ColumnResult(name = "evaluacion"),
		                    @ColumnResult(name = "diagnostico"),
		                    @ColumnResult(name = "tratamiento"),
		                    @ColumnResult(name = "examenes"),
		                    @ColumnResult(name = "reconsulta"),
		                    @ColumnResult(name = "diagnosticor"),
		                    @ColumnResult(name = "tratamientor"),
		                    @ColumnResult(name = "telefonopaciente")
		            }
		    )
		),
	
	@SqlResultSetMapping(
		    name = "ReporteHistoriaVacia",
		    classes = @ConstructorResult(
		            targetClass = HvaciaReport.class,
		            columns = {
		            		@ColumnResult(name = "idhistoriaclinica"),
		                    @ColumnResult(name = "hcl"),
		                    @ColumnResult(name = "anamnesis"),
		                    @ColumnResult(name = "evaluacion"),
		                    @ColumnResult(name = "diagnostico"),
		                    @ColumnResult(name = "tratamiento"),
		                    @ColumnResult(name = "examenes"),
		                    @ColumnResult(name = "reconsulta"),
		                    @ColumnResult(name = "diagnosticor"),
		                    @ColumnResult(name = "tratamientor")
		                    
		            }
		    )
		),
	
	@SqlResultSetMapping(
		    name = "ReporteMedicoxdia",
		    classes = @ConstructorResult(
		            targetClass = Medicoxdia.class,
		            columns = {
		            		@ColumnResult(name = "nombrespersonalmedico"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "fechaexpediciconhistoriaclinica")
		            }
		    )
		),
	
	@SqlResultSetMapping(
		    name = "ReporteMedicoreconsulta",
		    classes = @ConstructorResult(
		            targetClass = Medicoreconsulta.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		            		@ColumnResult(name = "reconsulta"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "fechareconsulta")
		            }
		    )
		),
	@SqlResultSetMapping(
		    name = "ReporteMedicoreconsulta2",
		    classes = @ConstructorResult(
		            targetClass = Medicoreconsulta.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		            		@ColumnResult(name = "reconsulta"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "fechareconsulta")
		            }
		    )
		)
	
	
})


@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteHistoriaPaciente",resultClass = HistoriaReport.class, 
			query = "SELECT hc.idhistoriaclinica,pac.nombresyapellidos,pac.telefonopaciente,pac.hcl,hc.edad,pac.direccionpaciente,pac.departamentopaciente, \r\n" + 
					"pac.distritopaciente,pac.provinciapaciente,\r\n" + 
					"pac.fechanacimientopaciente,\r\n" + 
					"pac.ocupacionpaciente,pac.estadocivilpaciente,\r\n" + 
					"hc.fechaexpediciconhistoriaclinica,\r\n" + 
					"hc.serviciomedico,pm.nombrespersonalmedico,hc.anamnesis,hc.evaluacion,hc.diagnostico,hc.tratamiento,\r\n" + 
					"hc.examenes,hc.reconsulta,hc.tratamientor, hc.diagnosticor \r\n" + 
					"FROM historiaclinica hc inner join paciente pac on pac.idpaciente = hc.idpaciente \r\n" + 
					"inner join personalmedico pm on pm.dnipersonalmedico = hc.dnipersonalmedico\r\n" + 
					"where idhistoriaclinica = :idhistoriaclinica"
			),
	@NamedNativeQuery(name = "ReporteHistoriaVacia",resultClass = HvaciaReport.class, 
	query = "SELECT hc.idhistoriaclinica,pac.hcl,hc.anamnesis,\r\n" + 
			"hc.evaluacion,hc.diagnostico,hc.tratamiento,\r\n" + 
			"hc.examenes,hc.reconsulta,hc.tratamientor, hc.diagnosticor\r\n" + 
			"FROM historiaclinica hc inner join paciente pac on pac.idpaciente = hc.idpaciente \r\n" + 
			"where hc.idhistoriaclinica = :idhistoriaclinica"
	),
	@NamedNativeQuery(name = "ReporteMedicoxdia",resultClass = Medicoxdia.class, 
	query = "select count(pm.nombrespersonalmedico)as total,pm.nombrespersonalmedico ,hc.fechaexpediciconhistoriaclinica \r\n" + 
			"from historiaclinica hc inner join personalmedico pm on pm.dnipersonalmedico = hc.dnipersonalmedico\r\n" + 
			"where hc.fechaexpediciconhistoriaclinica = TO_DATE(:fechaexpediciconhistoriaclinica , 'DD/MM/YYYY') \r\n" + 
			"group by hc.fechaexpediciconhistoriaclinica, pm.nombrespersonalmedico"
	),
	@NamedNativeQuery(name = "ReporteMedicoreconsulta",resultClass = Medicoreconsulta.class, 
	query = "select max(hc.idhistoriaclinica) as ids,count(hc.reconsulta) as total, hc.reconsulta, hc.fechareconsulta \r\n" + 
			"from historiaclinica hc\r\n" + 
			"where hc.reconsulta != '' and hc.fechareconsulta between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') \r\n" + 
			"group by hc.reconsulta, hc.fechareconsulta order by hc.fechareconsulta asc"
	)
	,
	@NamedNativeQuery(name = "ReporteMedicoreconsulta2",resultClass = Medicoreconsulta.class, 
	query = "select max(hc.idhistoriaclinica) as ids,count(hc.reconsulta) as total, hc.reconsulta, hc.fechareconsulta \r\n" + 
			"from historiaclinica hc\r\n" + 
			"where hc.reconsulta != '' and hc.fechareconsulta between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') \r\n" + 
			"and hc.reconsulta = :reconsulta group by hc.reconsulta, hc.fechareconsulta order by hc.fechareconsulta asc"
	)

})



@Entity
@Table(name="historiaclinica")
public class HistoriaClinica {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idhistoriaclinica;
	
	@Column(name="fechaexpediciconhistoriaclinica")
	@JsonSerialize(using= ToStringSerializer.class)
	private LocalDate fechaexpediciconhistoriaclinica;
	
	@Column(name = "serviciomedico")
	private String serviciomedico;
	
	@Column(name="edad")
	private int edad;
	
	@Column
	private String anamnesis;
	
	@Column
	private String evaluacion;
	
	@Column(name="fechareconsulta")
	@JsonSerialize(using= ToStringSerializer.class)
	private LocalDate fechareconsulta;
	
	@Column
	private String diagnostico;
	
	@Column
	private String tratamiento;
	
	@Column
	private String examenes;
	
	@OneToOne
	@JoinColumn(name = "dnipersonalmedico", nullable = false)
	private PersonalMedico personalmedico;
	
	@OneToOne
	@JoinColumn(name="idpaciente", nullable = false)
	private Paciente paciente;
	
	@Column
	private String reconsulta;
	
	@Column
	private String diagnosticor;
	
	@Column
	private String tratamientor;
	
	@Column
	private int estado;

	public int getIdhistoriaclinica() {
		return idhistoriaclinica;
	}

	public void setIdhistoriaclinica(int idhistoriaclinica) {
		this.idhistoriaclinica = idhistoriaclinica;
	}

	public LocalDate getFechaexpediciconhistoriaclinica() {
		return fechaexpediciconhistoriaclinica;
	}

	public void setFechaexpediciconhistoriaclinica(LocalDate fechaexpediciconhistoriaclinica) {
		this.fechaexpediciconhistoriaclinica = fechaexpediciconhistoriaclinica;
	}

	public String getServiciomedico() {
		return serviciomedico;
	}

	public void setServiciomedico(String serviciomedico) {
		this.serviciomedico = serviciomedico;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getExamenes() {
		return examenes;
	}

	public void setExamenes(String examenes) {
		this.examenes = examenes;
	}

	public PersonalMedico getPersonalmedico() {
		return personalmedico;
	}

	public void setPersonalmedico(PersonalMedico personalmedico) {
		this.personalmedico = personalmedico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public String getReconsulta() {
		return reconsulta;
	}

	public void setReconsulta(String reconsulta) {
		this.reconsulta = reconsulta;
	}

	public String getDiagnosticor() {
		return diagnosticor;
	}

	public void setDiagnosticor(String diagnosticor) {
		this.diagnosticor = diagnosticor;
	}

	public String getTratamientor() {
		return tratamientor;
	}

	public void setTratamientor(String tratamientor) {
		this.tratamientor = tratamientor;
	}

	public LocalDate getFechareconsulta() {
		return fechareconsulta;
	}

	public void setFechareconsulta(LocalDate fechareconsulta) {
		this.fechareconsulta = fechareconsulta;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	
}
