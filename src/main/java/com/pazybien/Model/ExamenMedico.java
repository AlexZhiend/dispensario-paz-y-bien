package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.pazybien.EntityReport.ExamenesGReport;
import com.pazybien.EntityReport.LabGenReport;

@SqlResultSetMappings({
@SqlResultSetMapping(
	    name = "ReporteExamenMedicoGeneral",
	    classes = @ConstructorResult(
	            targetClass = LabGenReport.class,
	            columns = {
	            		@ColumnResult(name = "idcategoriaexamenmedico"),
	                    @ColumnResult(name = "cantidad"),
	                    @ColumnResult(name = "nombrecategoriaexamenmedico"),  
	                    @ColumnResult(name = "fecha"),  
	            		  }
	    							)
					)


})

@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteExamenMedicoGeneral",resultClass = LabGenReport.class, 
			query = "select cat.idcategoriaexamenmedico,\r\n" + 
					"count(cat.idcategoriaexamenmedico)/count(distinct det.idexamenmedico)as cantidad, \r\n" + 
					"cat.nombrecategoriaexamenmedico,res.fecha\r\n" + 
					"from resultados res inner join detalleexamen det ON det.idresultados = res.idresultados\r\n" + 
					"inner join examenmedico ex on ex.idexamenmedico = det.idexamenmedico\r\n" + 
					"inner join categoriaexamenmedico cat on cat.idcategoriaexamenmedico = ex.idcategoriaexamenmedico\r\n" + 
					"where res.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')\r\n" + 
					"group by cat.nombrecategoriaexamenmedico, res.fecha, cat.idcategoriaexamenmedico"
			)
})



@Entity
@Table(name = "examenmedico")
public class ExamenMedico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idexamenmedico;
	
	@Column(name = "denominacionexamenmedico", nullable=false)
	private String denominacionexamenmedico;
	
	@Column(name = "unidadmedida")
	private String unidadmedida;
	
	@Column(name = "valorreferencia")
	private String valorreferencia;
	
	@Column(name = "extra")
	private int extra;
	
	@ManyToOne
	@JoinColumn(name = "idcategoriaexamenmedico", nullable = false)
	private CategoriaExamenMedico categoriaexamenmedico;

	public int getIdexamenmedico() {
		return idexamenmedico;
	}

	
	public void setIdexamenmedico(int idexamenmedico) {
		this.idexamenmedico = idexamenmedico;
	}

	public String getDenominacionexamenmedico() {
		return denominacionexamenmedico;
	}

	public void setDenominacionexamenmedico(String denominacionexamenmedico) {
		this.denominacionexamenmedico = denominacionexamenmedico;
	}


	public CategoriaExamenMedico getCategoriaexamenmedico() {
		return categoriaexamenmedico;
	}

	public void setCategoriaexamenmedico(CategoriaExamenMedico categoriaexamenmedico) {
		this.categoriaexamenmedico = categoriaexamenmedico;
	}


	public String getUnidadmedida() {
		return unidadmedida;
	}


	public void setUnidadmedida(String unidadmedida) {
		this.unidadmedida = unidadmedida;
	}


	public String getValorreferencia() {
		return valorreferencia;
	}


	public void setValorreferencia(String valorreferencia) {
		this.valorreferencia = valorreferencia;
	}


	public int getExtra() {
		return extra;
	}


	public void setExtra(int extra) {
		this.extra = extra;
	}





}
