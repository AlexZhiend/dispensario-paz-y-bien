package com.pazybien.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.CajaRecaudo;
import com.pazybien.EntityReport.CajaReport;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.RecaudoReport;



@SqlResultSetMappings({
@SqlResultSetMapping(
	    name = "TotalCajaXDia",
	    classes = @ConstructorResult(
	            targetClass = CajaRecaudo.class,
	            columns = {
	            		@ColumnResult(name = "ids"),
	                    @ColumnResult(name = "fechacomprobante"),
	                    @ColumnResult(name = "totalxdia")
	            }
	    	)
		),
@SqlResultSetMapping(
	    name = "TotalRecaudo",
	    classes = @ConstructorResult(
	            targetClass = RecaudoReport.class,
	            columns = {
	            		@ColumnResult(name = "idrecaudo"),
	                    @ColumnResult(name = "descripcion"),
	                    @ColumnResult(name = "fecha"),
	                    @ColumnResult(name = "monto"),
	                    @ColumnResult(name = "montoanulado"),
	                    @ColumnResult(name = "montofinal")
	            }
	    	)
		),

@SqlResultSetMapping(
	    name = "VentasIngresosReport",
	    classes = @ConstructorResult(
	            targetClass = NCreditoFarmaciaReport.class,
	            columns = {
	            		@ColumnResult(name = "idordenfarmacia"),
	            		@ColumnResult(name = "numeroorden"),
	                    @ColumnResult(name = "tipo"),
	                    @ColumnResult(name = "serie"),
	                    @ColumnResult(name = "consumidorordenfarmacia"),
	                    @ColumnResult(name = "exonera"),
	                    @ColumnResult(name = "inafecto"),
	                    @ColumnResult(name = "total"),
	                    @ColumnResult(name = "fechaordenfarmacia")
	            }
	    )
	)
	}
)


@NamedNativeQueries({
	@NamedNativeQuery(name = "TotalCajaXDia",resultClass = CajaRecaudo.class, 
	query = "select cp.fechacomprobante, max(cp.numerorecibocomprobante) as ids,\r\n" + 
			"sum(cp.total)as totalxdia\r\n" + 
			"from comprobantepago cp \r\n" + 
			"where cp.estado=1 and cp.fechacomprobante = TO_DATE(:fechacomprobante , 'DD/MM/YYYY') \r\n" + 
			"group by cp.fechacomprobante"
	),
	@NamedNativeQuery(name = "TotalRecaudo",resultClass = RecaudoReport.class, 
	query = "select rec.idrecaudo,rec.descripcion,rec.fecha, to_char(rec.monto, '99999999D99') as monto, \r\n" + 
			"to_char(rec.montoanulado, '99999999D99') as montoanulado,\r\n" + 
			"to_char((rec.monto - rec.montoanulado), '99999999D99') as montofinal \r\n" + 
			"from recaudo rec where rec.fecha BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY') \r\n" + 
			"order by rec.fecha"
	),
	@NamedNativeQuery(name = "VentasIngresosReport",resultClass = NCreditoFarmaciaReport.class, 
	query = "select cp.idcomprobantepago as idordenfarmacia,cp.fechacomprobante as fechaordenfarmacia, to_char( cp.tipo, 'FM09') as tipo,to_char(cp.serie, 'FM999099') as serie,\r\n" + 
			"to_char( cp.tiporef, 'FM09') as tiporef,to_char( cp.serieref, 'FM099') as serieref,\r\n" + 
			"to_char( cp.numeroref, 'FM099999') as numeroref, cp.fechacompref as fechaordenref,cp.totalref, \r\n" + 
			" to_char( cp.numerorecibocomprobante, 'FM099999') as numeroorden,pac.nombresyapellidos as consumidorordenfarmacia,\r\n" + 
			"(cast(cp.descuento as DOUBLE PRECISION)/100 )*cp.total as exonera, \r\n" + 
			"cp.total - (cast(cp.descuento as DOUBLE PRECISION)/100)*cp.total as inafecto, cp.total\r\n" + 
			"from comprobantepago cp inner join paciente pac on cp.idpaciente = pac.idpaciente \r\n" + 
			"where cp.fechacomprobante between TO_DATE(:fechainicio,'DD/MM/YYYY') and TO_DATE(:fechafin,'DD/MM/YYYY')\r\n" + 
			"and cp.estado=1 \r\n" + 
			"UNION ALL\r\n" + 
			"select od.idordenfarmacia,od.fechaordenfarmacia, to_char( od.tipo, 'FM09') as tipo,to_char(od.serie, 'FM999099') as serie,\r\n" + 
			"to_char( od.tiporef, 'FM09') as tiporef,to_char( od.serieref, 'FM099') as serieref,\r\n" + 
			"to_char( od.numeroref, 'FM099999') as numeroref, od.fechaordenref,od.totalref,\r\n" + 
			" to_char( od.numeroorden, 'FM099999') as numeroorden,od.consumidorordenfarmacia,\r\n" + 
			"(cast(od.descuento as DOUBLE PRECISION)/100 )*od.total as exonera,\r\n" + 
			"od.total - (cast(od.descuento as DOUBLE PRECISION)/100)*od.total as inafecto, od.total\r\n" + 
			"from ordenfarmacia od where od.fechaordenfarmacia between TO_DATE(:fechainicio,'DD/MM/YYYY') and TO_DATE(:fechafin,'DD/MM/YYYY')\r\n" + 
			"and od.estado=1\r\n" + 
			"order by serie asc,tipo asc, numeroorden asc"
	),
})



@Entity
@Table(name = "recaudo")
public class Recaudo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idrecaudo;
	
	@Column
	private double monto;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@Column
	private LocalDate fecha;
	
	@Column
	private String descripcion;
	
	@Column
	private double montoanulado;

	public int getIdrecaudo() {
		return idrecaudo;
	}

	public void setIdrecaudo(int idrecaudo) {
		this.idrecaudo = idrecaudo;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getMontoanulado() {
		return montoanulado;
	}

	public void setMontoanulado(double montoanulado) {
		this.montoanulado = montoanulado;
	}


}
