package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "detallecomprobante")
public class DetalleComprobante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetallecomprobante;
	
	@Column
	private int cantidad;
	
	@JsonIgnore 
	@ManyToOne 
	@JoinColumn(name = "idcomprobantepago")
	private ComprobantePago comprobantepago;
	
	@OneToOne 
	@JoinColumn(name = "idcategoriaexamenmedico")
	private CategoriaExamenMedico categoriaexamenmedico;
	
	@Column
	private String dnipersonalmedico;
	
	public int getIddetallecomprobante() {
		return iddetallecomprobante;
	}

	public void setIddetallecomprobante(int iddetallecomprobante) {
		this.iddetallecomprobante = iddetallecomprobante;
	}

	public ComprobantePago getComprobantepago() {
		return comprobantepago;
	}

	public void setComprobantepago(ComprobantePago comprobantepago) {
		this.comprobantepago = comprobantepago;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public CategoriaExamenMedico getCategoriaexamenmedico() {
		return categoriaexamenmedico;
	}

	public void setCategoriaexamenmedico(CategoriaExamenMedico categoriaexamenmedico) {
		this.categoriaexamenmedico = categoriaexamenmedico;
	}

	public String getDnipersonalmedico() {
		return dnipersonalmedico;
	}

	public void setDnipersonalmedico(String dnipersonalmedico) {
		this.dnipersonalmedico = dnipersonalmedico;
	}


}

