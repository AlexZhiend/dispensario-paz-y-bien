package com.pazybien.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="paciente")
public class Paciente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpaciente;
	
	@Column(name="hcl")
	private int hcl;
	
	@Column(name="dnipaciente")
	private String dnipaciente;
	
	@Column(name="nombresyapellidos", nullable=false)
	private String nombresyapellidos;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@Column(name="fechanacimientopaciente")
	private LocalDate fechanacimientopaciente;
	
	@Column(name="departamentopaciente")
	private String departamentopaciente;
	
	@Column(name="provinciapaciente")
	private String provinciapaciente;
	
	@Column(name="distritopaciente")
	private String distritopaciente;
	
	@Column(name="direccionpaciente")
	private String direccionpaciente;
	
	@Column(name="telefonopaciente")
	private String telefonopaciente;
	
	@Column(name="ocupacionpaciente")
	private String ocupacionpaciente;
	
	@Column(name="estadocivilpaciente")
	private String estadocivilpaciente;
	
	@Column(name="niveleducacionpaciente")
	private String niveleducacionpaciente;
	
	@Column(name="datospadrepaciente")
	private String padrepaciente;
	
	@Column(name="datosmadrepaciente")
	private String madrepaciente;
	
	@OneToOne
	@JoinColumn(name="idtipopaciente", nullable = false)
	private TipoPaciente tipopaciente;
	
	public String getDnipaciente() {
		return dnipaciente;
	}

	public void setDnipaciente(String dnipaciente) {
		this.dnipaciente = dnipaciente;
	}

	public LocalDate getFechanacimientopaciente() {
		return fechanacimientopaciente;
	}

	public void setFechanacimientopaciente(LocalDate fechanacimientopaciente) {
		this.fechanacimientopaciente = fechanacimientopaciente;
	}

	public String getDepartamentopaciente() {
		return departamentopaciente;
	}

	public void setDepartamentopaciente(String departamentopaciente) {
		this.departamentopaciente = departamentopaciente;
	}

	public String getProvinciapaciente() {
		return provinciapaciente;
	}

	public void setProvinciapaciente(String provinciapaciente) {
		this.provinciapaciente = provinciapaciente;
	}

	public String getDistritopaciente() {
		return distritopaciente;
	}

	public void setDistritopaciente(String distritopaciente) {
		this.distritopaciente = distritopaciente;
	}

	public String getDireccionpaciente() {
		return direccionpaciente;
	}

	public void setDireccionpaciente(String direccionpaciente) {
		this.direccionpaciente = direccionpaciente;
	}

	public String getTelefonopaciente() {
		return telefonopaciente;
	}

	public void setTelefonopaciente(String telefonopaciente) {
		this.telefonopaciente = telefonopaciente;
	}

	public String getOcupacionpaciente() {
		return ocupacionpaciente;
	}

	public void setOcupacionpaciente(String ocupacionpaciente) {
		this.ocupacionpaciente = ocupacionpaciente;
	}

	public String getEstadocivilpaciente() {
		return estadocivilpaciente;
	}

	public void setEstadocivilpaciente(String estadocivilpaciente) {
		this.estadocivilpaciente = estadocivilpaciente;
	}

	public String getNiveleducacionpaciente() {
		return niveleducacionpaciente;
	}

	public void setNiveleducacionpaciente(String niveleducacionpaciente) {
		this.niveleducacionpaciente = niveleducacionpaciente;
	}

	public String getPadrepaciente() {
		return padrepaciente;
	}

	public void setPadrepaciente(String padrepaciente) {
		this.padrepaciente = padrepaciente;
	}

	public String getMadrepaciente() {
		return madrepaciente;
	}

	public void setMadrepaciente(String madrepaciente) {
		this.madrepaciente = madrepaciente;
	}

	public TipoPaciente getTipopaciente() {
		return tipopaciente;
	}

	public void setTipopaciente(TipoPaciente tipopaciente) {
		this.tipopaciente = tipopaciente;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public int getIdpaciente() {
		return idpaciente;
	}

	public void setIdpaciente(int idpaciente) {
		this.idpaciente = idpaciente;
	}

	public int getHcl() {
		return hcl;
	}

	public void setHcl(int hcl) {
		this.hcl = hcl;
	}
	
	
	
}
