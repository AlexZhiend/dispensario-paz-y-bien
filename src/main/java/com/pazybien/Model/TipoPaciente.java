package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipopaciente")
public class TipoPaciente {

	@Id
	@Column(name = "idtipopaciente")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtipopaciente;
	
	@Column(name="tipopaciente",nullable=false)
	private String tipopaciente;

	public int getIdtipopaciente() {
		return idtipopaciente;
	}

	public void setIdtipopaciente(int idtipopaciente) {
		this.idtipopaciente = idtipopaciente;
	}

	public String getTipopaciente() {
		return tipopaciente;
	}

	public void setTipopaciente(String tipopaciente) {
		this.tipopaciente = tipopaciente;
	}

	public TipoPaciente(int idtipopaciente, String tipopaciente) {
		super();
		this.idtipopaciente = idtipopaciente;
		this.tipopaciente = tipopaciente;
	}

	public TipoPaciente() {
		super();
	}
	
}
