package com.pazybien.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.AtencionesGeneral;
import com.pazybien.EntityReport.AtencionesRxEcoReport;
import com.pazybien.EntityReport.ComprobanteReport;

@SqlResultSetMappings({
	@SqlResultSetMapping(
		    name = "ReporteAtencionesEcoRad1",
		    classes = @ConstructorResult(
		            targetClass = AtencionesRxEcoReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "fechaatencion"),
		                    @ColumnResult(name = "atenciones"),
		                    @ColumnResult(name = "nombrespersonalmedico")
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "ReporteAtencionesEcoRad2",
		    classes = @ConstructorResult(
		            targetClass = AtencionesRxEcoReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "fechaatencion"),
		                    @ColumnResult(name = "atenciones"),
		                    @ColumnResult(name = "nombrespersonalmedico")
		            }
		    	)
			)
})



@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteAtencionesEcoRad1",resultClass = AtencionesRxEcoReport.class, 
			query = "select max(cp.idcomprobantepago) as ids,cp.fechaatencion,count(det.dnipersonalmedico) as atenciones, \n" + 
					"per.nombrespersonalmedico from comprobantepago cp inner join detallecomprobante det \n" + 
					"ON det.idcomprobantepago = cp.idcomprobantepago inner join personalmedico per on per.dnipersonalmedico = det.dnipersonalmedico\n" + 
					"where (det.dnipersonalmedico <> '') and (cp.fechaatencion between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY'))\n" + 
					"group by cp.fechaatencion,per.nombrespersonalmedico order by cp.fechaatencion desc"
			),
	@NamedNativeQuery(name = "ReporteAtencionesEcoRad2",resultClass = AtencionesRxEcoReport.class, 
	query = "select max(cp.idcomprobantepago) as ids,cp.fechaatencion,count(det.dnipersonalmedico) as atenciones, \n" + 
			"per.nombrespersonalmedico from comprobantepago cp inner join detallecomprobante det \n" + 
			"ON det.idcomprobantepago = cp.idcomprobantepago inner join personalmedico per on per.dnipersonalmedico = det.dnipersonalmedico\n" + 
			"where (det.dnipersonalmedico <> '') and (cp.fechaatencion between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY'))\n" + 
			"and per.dnipersonalmedico = :dnipersonalmedico group by cp.fechaatencion,per.nombrespersonalmedico order by cp.fechaatencion desc"
	)
})
			
			

@Entity
@Table(name = "personalmedico")
public class PersonalMedico {
	
@Id
@Column(name="dnipersonalmedico", unique=true,nullable=false)
private String dnipersonalmedico; 

@Column(name="nombrespersonalmedico")
private String nombrespersonalmedico; 

@Column(name="correopersonalmedico")
private String correopersonalmedico; 

@Column(name="telefonopersonalmedico") 
private String telefonopersonalmedico; 

@Column(name="rnepersonalmedico")
private String rnepersonalmedico; 

@Column(name="cmppersonalmedico")
private String cmppersonalmedico; 

@Column(name="areapersonalmedico")
private String areapersonalmedico; 

@JsonSerialize(using = ToStringSerializer.class)
@Column(name="fechanacimientopersonalmedico")
private LocalDate fechanacimientopersonalmedico;

@Column(name="estadocivilpersonalmedico")
private String estadocivilpersonalmedico; 

@OneToOne
@JoinColumn(name="idtipopersonalmedico")
private TipoPersonalMedico tipopersonalmedico;

@OneToOne
@JoinColumn(name = "idespecialidad")
private Especialidad especialidad;

public String getDnipersonalmedico() {
	return dnipersonalmedico;
}

public void setDnipersonalmedico(String dnipersonalmedico) {
	this.dnipersonalmedico = dnipersonalmedico;
}

public String getNombrespersonalmedico() {
	return nombrespersonalmedico;
}

public void setNombrespersonalmedico(String nombrespersonalmedico) {
	this.nombrespersonalmedico = nombrespersonalmedico;
}

public String getCorreopersonalmedico() {
	return correopersonalmedico;
}

public void setCorreopersonalmedico(String correopersonalmedico) {
	this.correopersonalmedico = correopersonalmedico;
}

public String getTelefonopersonalmedico() {
	return telefonopersonalmedico;
}

public void setTelefonopersonalmedico(String telefonopersonalmedico) {
	this.telefonopersonalmedico = telefonopersonalmedico;
}

public String getRnepersonalmedico() {
	return rnepersonalmedico;
}

public void setRnepersonalmedico(String rnepersonalmedico) {
	this.rnepersonalmedico = rnepersonalmedico;
}

public String getCmppersonalmedico() {
	return cmppersonalmedico;
}

public void setCmppersonalmedico(String cmppersonalmedico) {
	this.cmppersonalmedico = cmppersonalmedico;
}

public String getAreapersonalmedico() {
	return areapersonalmedico;
}

public void setAreapersonalmedico(String areapersonalmedico) {
	this.areapersonalmedico = areapersonalmedico;
}

public LocalDate getFechanacimientopersonalmedico() {
	return fechanacimientopersonalmedico;
}

public void setFechanacimientopersonalmedico(LocalDate fechanacimientopersonalmedico) {
	this.fechanacimientopersonalmedico = fechanacimientopersonalmedico;
}

public String getEstadocivilpersonalmedico() {
	return estadocivilpersonalmedico;
}

public void setEstadocivilpersonalmedico(String estadocivilpersonalmedico) {
	this.estadocivilpersonalmedico = estadocivilpersonalmedico;
}

public TipoPersonalMedico getTipopersonalmedico() {
	return tipopersonalmedico;
}

public void setTipopersonalmedico(TipoPersonalMedico tipopersonalmedico) {
	this.tipopersonalmedico = tipopersonalmedico;
}

public Especialidad getEspecialidad() {
	return especialidad;
}

public void setEspecialidad(Especialidad especialidad) {
	this.especialidad = especialidad;
}

}
