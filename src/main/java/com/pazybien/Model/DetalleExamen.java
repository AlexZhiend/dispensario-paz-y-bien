package com.pazybien.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.ExamenesGReport;
import com.pazybien.EntityReport.ResExPacienteReport;

@SqlResultSetMappings({
@SqlResultSetMapping(
	    name = "ReporteExamenesgGeneral",
	    classes = @ConstructorResult(
	            targetClass = ExamenesGReport.class,
	            columns = {
	            		@ColumnResult(name = "iddetalleexamen"),
	                    @ColumnResult(name = "fecha"),
	                    @ColumnResult(name = "observaciones"),
	                    @ColumnResult(name = "nombresyapellidos"),
	                    @ColumnResult(name = "resultado"),
	                    @ColumnResult(name = "denominacionexamenmedico"),
	                    @ColumnResult(name = "unidadmedida"),
	                    @ColumnResult(name = "extra"),
	                    @ColumnResult(name = "valorreferencia"),
	                    @ColumnResult(name = "nombrecategoriaexamenmedico")
	            		  }
	    							)
					)


})

@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteExamenesgPaciente",resultClass = ExamenesGReport.class, 
			query = "SELECT det.iddetalleexamen,res.fecha,res.observaciones,pac.nombresyapellidos||'  -  HCL('||pac.hcl||')' as nombresyapellidos,det.resultado,\r\n" + 
					"ex.denominacionexamenmedico,ex.unidadmedida,ex.extra,ex.valorreferencia,cat.nombrecategoriaexamenmedico\r\n" + 
					"from resultados res inner join detalleexamen det on det.idresultados = res.idresultados\r\n" + 
					"inner join examenmedico ex on ex.idexamenmedico = det.idexamenmedico\r\n" + 
					"inner join categoriaexamenmedico cat on cat.idcategoriaexamenmedico = ex.idcategoriaexamenmedico\r\n" + 
					"inner join paciente pac on pac.idpaciente = res.idpaciente\r\n" + 
					"where res.idpaciente = :idpaciente and res.fecha = TO_DATE(:fecha , 'DD/MM/YYYY') "
			)
})


@Entity
@Table(name = "detalleexamen")
public class DetalleExamen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetalleexamen;
	
	@Column
	private String resultado;
	
	@ManyToOne
	@JoinColumn(name = "idexamenmedico")
	private ExamenMedico examenmedico;
	
	@JsonIgnore 
	@ManyToOne 
	@JoinColumn(name = "idresultados")
	private Resultados resultados;
	
	public int getIddetalleexamen() {
		return iddetalleexamen;
	}

	public void setIddetalleexamen(int iddetalleexamen) {
		this.iddetalleexamen = iddetalleexamen;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public ExamenMedico getExamenmedico() {
		return examenmedico;
	}

	public void setExamenmedico(ExamenMedico examenmedico) {
		this.examenmedico = examenmedico;
	}

	public Resultados getResultados() {
		return resultados;
	}

	public void setResultados(Resultados resultados) {
		this.resultados = resultados;
	}

	
}
