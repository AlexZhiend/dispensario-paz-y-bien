package com.pazybien.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="especialidad")
public class Especialidad {

	
	@Id
	@Column(name="idespecialidad")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idespecialidad;
	
	@Column(name="nombreespecialidad")
	private String nombreespecialidad;

	public int getIdespecialidad() {
		return idespecialidad;
	}

	public void setIdespecialidad(int idespecialidad) {
		this.idespecialidad = idespecialidad;
	}

	public String getNombreespecialidad() {
		return nombreespecialidad;
	}

	public void setNombreespecialidad(String nombreespecialidad) {
		this.nombreespecialidad = nombreespecialidad;
	}

	public Especialidad(int idespecialidad, String nombreespecialidad) {
		super();
		this.idespecialidad = idespecialidad;
		this.nombreespecialidad = nombreespecialidad;
	}

	public Especialidad() {
		super();
	}

}
