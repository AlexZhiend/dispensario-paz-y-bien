package com.pazybien.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="resultados")
public class Resultados {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idresultados;
	
	@Column(name="fecha")
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fecha;
	
	@Column
	private String observaciones;
	
	@ManyToOne
	@JoinColumn(name = "idpaciente")
	private Paciente paciente;
	
	@OneToMany(mappedBy = "resultados", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
			fetch=FetchType.LAZY, orphanRemoval = true)
	private List<DetalleExamen> detalleexamen;

	public int getIdresultados() {
		return idresultados;
	}

	public void setIdresultados(int idresultados) {
		this.idresultados = idresultados;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public List<DetalleExamen> getDetalleexamen() {
		return detalleexamen;
	}

	public void setDetalleexamen(List<DetalleExamen> detalleexamen) {
		this.detalleexamen = detalleexamen;
	}
	
	

}
