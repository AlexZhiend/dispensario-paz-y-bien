package com.pazybien.Model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "detalleordenf")
public class DetalleOrdenF {
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int iddetalleordenf;

	@Column
	private int cantidad;
	
	@ManyToOne
	@JoinColumn(name="idproducto", nullable=false)
	private Producto producto;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idordenfarmacia", nullable=false)
	private OrdenFarmacia ordenfarmacia;
	

	public int getIddetalleordenf() {
		return iddetalleordenf;
	}

	public void setIddetalleordenf(int iddetalleordenf) {
		this.iddetalleordenf = iddetalleordenf;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public OrdenFarmacia getOrdenfarmacia() {
		return ordenfarmacia;
	}

	public void setOrdenfarmacia(OrdenFarmacia ordenfarmacia) {
		this.ordenfarmacia = ordenfarmacia;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
				
}
