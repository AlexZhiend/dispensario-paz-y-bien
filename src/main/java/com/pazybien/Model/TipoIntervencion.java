package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tipointervencion")
public class TipoIntervencion {
	
	@Id
	@Column(name="idtipointervencion")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idtipointervencion;
	
	@Column(name="denominaciontipointervencion", nullable=false)
	private String denominaciontipointervencion;
	
	@Column(name="descripciontipointervencion", nullable=false)
	private String descripciontipointervencion;
	
	@Column(name="preciotipointervencion", nullable=false)
	private int preciotipointervencion;
	
	@OneToOne(mappedBy = "tipointervencion")
	private IntervencionTopico intervenciontopico;

	public int getIdtipointervencion() {
		return idtipointervencion;
	}

	public void setIdtipointervencion(int idtipointervencion) {
		this.idtipointervencion = idtipointervencion;
	}

	public String getDenominaciontipointervencion() {
		return denominaciontipointervencion;
	}

	public void setDenominaciontipointervencion(String denominaciontipointervencion) {
		this.denominaciontipointervencion = denominaciontipointervencion;
	}

	public String getDescripciontipointervencion() {
		return descripciontipointervencion;
	}

	public void setDescripciontipointervencion(String descripciontipointervencion) {
		this.descripciontipointervencion = descripciontipointervencion;
	}

	public int getPreciotipointervencion() {
		return preciotipointervencion;
	}

	public void setPreciotipointervencion(int preciotipointervencion) {
		this.preciotipointervencion = preciotipointervencion;
	}

	public IntervencionTopico getIntervenciontopico() {
		return intervenciontopico;
	}

	public void setIntervenciontopico(IntervencionTopico intervenciontopico) {
		this.intervenciontopico = intervenciontopico;
	}

	public TipoIntervencion(int idtipointervencion, String denominaciontipointervencion,
			String descripciontipointervencion, int preciotipointervencion, IntervencionTopico intervenciontopico) {
		super();
		this.idtipointervencion = idtipointervencion;
		this.denominaciontipointervencion = denominaciontipointervencion;
		this.descripciontipointervencion = descripciontipointervencion;
		this.preciotipointervencion = preciotipointervencion;
		this.intervenciontopico = intervenciontopico;
	}

	public TipoIntervencion(int idtipointervencion, String denominaciontipointervencion,
			String descripciontipointervencion, int preciotipointervencion) {
		super();
		this.idtipointervencion = idtipointervencion;
		this.denominaciontipointervencion = denominaciontipointervencion;
		this.descripciontipointervencion = descripciontipointervencion;
		this.preciotipointervencion = preciotipointervencion;
	}

	public TipoIntervencion() {
		super();
	}
	
}

