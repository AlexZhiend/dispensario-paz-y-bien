package com.pazybien.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.DetalladoVentaF;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.OrdenFReport;
import com.pazybien.EntityReport.TotalxDiaFarmacia;

@SqlResultSetMappings({
@SqlResultSetMapping(
	    name = "ReporteOrdenFGeneral",
	    classes = @ConstructorResult(
	            targetClass = OrdenFReport.class,
	            columns = {
	            		@ColumnResult(name = "id"),
	                    @ColumnResult(name = "fechaordenfarmacia"),
	                    @ColumnResult(name = "consumidorordenfarmacia"),
	                    @ColumnResult(name = "numeroorden"),
	                    @ColumnResult(name = "rucordenfarmacia"),
	                    @ColumnResult(name = "pventaproducto"),
	                    @ColumnResult(name = "nombreproducto"),
	                    @ColumnResult(name = "cantidad"),
	                    @ColumnResult(name = "total"),
	                    @ColumnResult(name = "descuento")
	            }
	    )
	),
@SqlResultSetMapping(
		name = "TotalFarmaciaxDia",
		classes = @ConstructorResult(
				targetClass = TotalxDiaFarmacia.class,
				columns = {
	                    @ColumnResult(name = "totalxdia"),
	                    @ColumnResult(name = "fechaordenfarmacia")
				}
			)
		),
@SqlResultSetMapping(
	    name = "ReporteOrdenFDetallado",
	    classes = @ConstructorResult(
	            targetClass = DetalladoVentaF.class,
	            columns = {
	            		@ColumnResult(name = "id"),
	                    @ColumnResult(name = "fechaordenfarmacia"),
	                    @ColumnResult(name = "consumidorordenfarmacia"),
	                    @ColumnResult(name = "numeroorden"),
	                    @ColumnResult(name = "rucordenfarmacia"),
	                    @ColumnResult(name = "pventaproducto"),
	                    @ColumnResult(name = "nombreproducto"),
	                    @ColumnResult(name = "cantidad"),
	                    @ColumnResult(name = "total"),
	                    @ColumnResult(name = "descuento"),
	                    @ColumnResult(name = "estado")
	            }
	    )
	),
@SqlResultSetMapping(
	    name = "NotaCreditoDetallado",
	    classes = @ConstructorResult(
	            targetClass = NCreditoFarmaciaReport.class,
	            columns = {
	            		@ColumnResult(name = "idordenfarmacia"),
	            		@ColumnResult(name = "numeroorden"),
	                    @ColumnResult(name = "tipo"),
	                    @ColumnResult(name = "serie"),
	                    @ColumnResult(name = "consumidorordenfarmacia"),
	                    @ColumnResult(name = "exonera"),
	                    @ColumnResult(name = "inafecto"),
	                    @ColumnResult(name = "total"),
	                    @ColumnResult(name = "fechaordenfarmacia")
	            }
	    )
	)
})

@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteOrdenFPaciente",resultClass = OrdenFReport.class, 
			query = "select det.iddetalleordenf as id,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
					"pro.pventaproducto,pro.nombreproducto,det.cantidad,\r\n" + 
					"round( CAST(sum(pro.pventaproducto * det.cantidad) as numeric), 2) as importe,round( CAST(sum(od.total) as numeric), 2) as total, od.descuento\r\n" + 
					"from ordenfarmacia od inner join detalleordenf det on od.idordenfarmacia = det.idordenfarmacia\r\n" + 
					"inner join producto pro on pro.idproducto = det.idproducto where od.numeroorden = :numeroorden\r\n" + 
					"group by det.iddetalleordenf,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
					"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.descuento,od.total"
			),
	@NamedNativeQuery(name = "TotalFarmaciaxDia",resultClass = TotalxDiaFarmacia.class, 
	query = "select sum(od.total) as totalxdia,od.fechaordenfarmacia from ordenfarmacia od\r\n" + 
			"where od.fechaordenfarmacia = TO_DATE(:fecha,'DD/MM/YYYY') and od.estado = 1 \r\n" + 
			"group by od.fechaordenfarmacia"
			),
	
	@NamedNativeQuery(name = "ReporteOrdenFDetallado",resultClass = DetalladoVentaF.class, 
	query = "select det.iddetalleordenf as id,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
			"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.estado,\r\n" + 
			"round( CAST(sum(pro.pventaproducto * det.cantidad) as numeric), 2) as importe,round( CAST(sum(od.total) as numeric), 2) as total, od.descuento\r\n" + 
			"from ordenfarmacia od inner join detalleordenf det on od.idordenfarmacia = det.idordenfarmacia \r\n" + 
			"inner join producto pro on pro.idproducto = det.idproducto\r\n" + 
			"where od.fechaordenfarmacia = TO_DATE(:fechaordenfarmacia,'DD/MM/YYYY')\r\n" + 
			"group by det.iddetalleordenf,od.fechaordenfarmacia,od.consumidorordenfarmacia,od.numeroorden,od.rucordenfarmacia,\r\n" + 
			"pro.pventaproducto,pro.nombreproducto,det.cantidad,od.descuento,od.total,od.estado"
	),
	@NamedNativeQuery(name = "NotaCreditoDetallado",resultClass = NCreditoFarmaciaReport.class, 
	query = "select od.idordenfarmacia,od.fechaordenfarmacia, to_char( od.tipo, 'FM09') as tipo,to_char(od.serie, 'FM999099') as serie,to_char( od.tiporef, 'FM09') as tiporef,to_char( od.serieref, 'FM099') as serieref,\r\n" + 
			"to_char( od.numeroref, 'FM099999') as numeroref, od.fechaordenref,od.totalref,\r\n" + 
			" to_char( od.numeroorden, 'FM099999') as numeroorden,od.consumidorordenfarmacia,\r\n" + 
			"(cast(od.descuento as DOUBLE PRECISION)/100 )*od.total as exonera, \r\n" + 
			"od.total - (cast(od.descuento as DOUBLE PRECISION)/100)*od.total as inafecto, od.total\r\n" + 
			"from ordenfarmacia od where od.fechaordenfarmacia between TO_DATE(:fechainicio,'DD/MM/YYYY') and TO_DATE(:fechafin,'DD/MM/YYYY') \r\n" + 
			"and od.estado=1 \r\n" + 
			"order by od.serie asc,od.tipo asc,od.numeroorden asc"
			),
})


@Entity
@Table(name = "ordenfarmacia")
public class OrdenFarmacia {
	
	@Id
	@Column(name = "idordenfarmacia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idordenfarmacia;
	
	@Column(name = "fechaordenfarmacia")
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenfarmacia;
	
	@Column 
	private int numeroorden; 
	
	@Column(name ="rucordenfarmacia")
	private String rucordenfarmacia;
	
	@Column(name = "consumidorordenfarmacia")
	private String consumidorordenfarmacia; 
	
	@Column
	private double total; 
	
	@Column
	private int descuento;
	
	@Column
	private int estado;
	
	@Column
	private int tipo;
	
	@Column
	private int serie;
	
	@Column
	private int serieref;
	
	@Column(name = "fechaordenref")
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenref;
	
	@Column
	private int tiporef;
	
	@Column
	private int numeroref;
	
	@Column
	private double totalref;
	
	@OneToMany( mappedBy = "ordenfarmacia", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true )
	private List<DetalleOrdenF> detalleordenF;
	
	
	public int getIdordenfarmacia() {
		return idordenfarmacia;
	}

	public void setIdordenfarmacia(int idordenfarmacia) {
		this.idordenfarmacia = idordenfarmacia;
	}

	public LocalDate getFechaordenfarmacia() {
		return fechaordenfarmacia;
	}

	public void setFechaordenfarmacia(LocalDate fechaordenfarmacia) {
		this.fechaordenfarmacia = fechaordenfarmacia;
	}

	public String getRucordenfarmacia() {
		return rucordenfarmacia;
	}

	public void setRucordenfarmacia(String rucordenfarmacia) {
		this.rucordenfarmacia = rucordenfarmacia;
	}

	public String getConsumidorordenfarmacia() {
		return consumidorordenfarmacia;
	}

	public void setConsumidorordenfarmacia(String consumidorordenfarmacia) {
		this.consumidorordenfarmacia = consumidorordenfarmacia;
	}

	public int getNumeroorden() {
		return numeroorden;
	}

	public void setNumeroorden(int numeroorden) {
		this.numeroorden = numeroorden;
	}

	public List<DetalleOrdenF> getDetalleordenF() {
		return detalleordenF;
	}

	public void setDetalleordenF(List<DetalleOrdenF> detalleordenF) {
		this.detalleordenF = detalleordenF;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}

	public int getSerieref() {
		return serieref;
	}

	public void setSerieref(int serieref) {
		this.serieref = serieref;
	}

	public LocalDate getFechaordenref() {
		return fechaordenref;
	}

	public void setFechaordenref(LocalDate fechaordenref) {
		this.fechaordenref = fechaordenref;
	}

	public int getTiporef() {
		return tiporef;
	}

	public void setTiporef(int tiporef) {
		this.tiporef = tiporef;
	}

	public int getNumeroref() {
		return numeroref;
	}

	public void setNumeroref(int numeroref) {
		this.numeroref = numeroref;
	}

	public double getTotalref() {
		return totalref;
	}

	public void setTotalref(double totalref) {
		this.totalref = totalref;
	}

	
}
