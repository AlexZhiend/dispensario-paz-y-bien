package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "presentacionproducto")
public class PresentacionProducto {
	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idpresentacionproducto;
	
	@Column(name = "nombrepresentacionproducto", nullable=false)
	private String nombrepresentacionproducto;
	
	@Column(name = "cantidadpresentacionproducto", nullable=false)
	private String cantidadpresentacionproducto;
	
	@Column(name = "unidadpresentacionproducto", nullable=false)
	private String unidadpresentacionproducto;

	public int getIdpresentacionproducto() {
		return idpresentacionproducto;
	}

	public void setIdpresentacionproducto(int idpresentacionproducto) {
		this.idpresentacionproducto = idpresentacionproducto;
	}

	public String getNombrepresentacionproducto() {
		return nombrepresentacionproducto;
	}

	public void setNombrepresentacionproducto(String nombrepresentacionproducto) {
		this.nombrepresentacionproducto = nombrepresentacionproducto;
	}

	public String getCantidadpresentacionproducto() {
		return cantidadpresentacionproducto;
	}

	public void setCantidadpresentacionproducto(String cantidadpresentacionproducto) {
		this.cantidadpresentacionproducto = cantidadpresentacionproducto;
	}

	public String getUnidadpresentacionproducto() {
		return unidadpresentacionproducto;
	}

	public void setUnidadpresentacionproducto(String unidadpresentacionproducto) {
		this.unidadpresentacionproducto = unidadpresentacionproducto;
	}

	public PresentacionProducto(int idpresentacionproducto, String nombrepresentacionproducto,
			String cantidadpresentacionproducto, String unidadpresentacionproducto) {
		super();
		this.idpresentacionproducto = idpresentacionproducto;
		this.nombrepresentacionproducto = nombrepresentacionproducto;
		this.cantidadpresentacionproducto = cantidadpresentacionproducto;
		this.unidadpresentacionproducto = unidadpresentacionproducto;
	}

	public PresentacionProducto() {
		super();
	}
	
	
}
