package com.pazybien.Model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.EntityReport.AtencionLabReport;
import com.pazybien.EntityReport.AtencionesGeneral;
import com.pazybien.EntityReport.AtencionesReport;
import com.pazybien.EntityReport.CajaDetalladoReport;
import com.pazybien.EntityReport.CajaReport;
import com.pazybien.EntityReport.ComprobanteReport;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.ProductoReport;
import com.pazybien.EntityReport.ProductoVentasReport;


@SqlResultSetMappings({
	@SqlResultSetMapping(
	    name = "ReporteComporbanteUnit",
	    classes = @ConstructorResult(
	            targetClass = ComprobanteReport.class,
	            columns = {
	            		@ColumnResult(name = "ids"),
	                    @ColumnResult(name = "numerorecibocomprobante"),
	                    @ColumnResult(name = "cantidadotros"),
	                    @ColumnResult(name = "cantidadfarmacia"),
	                    @ColumnResult(name = "cantidadtopico"),
	                    @ColumnResult(name = "cantidad"),
	                    @ColumnResult(name = "denominacionexamenmedico"),
	                    @ColumnResult(name = "precioexmenmedico"),
	                    @ColumnResult(name = "nombresyapellidos"),
	                    @ColumnResult(name = "denominacionserviciomedico"),
	                    @ColumnResult(name = "precioserviciomedico"),
	                    @ColumnResult(name = "subtotal"),
	                    @ColumnResult(name = "totalgeneral"),
	                    @ColumnResult(name = "turno"),
	                    @ColumnResult(name = "fechacomprobante"),
	                    @ColumnResult(name = "fechaatencion"),
	                    @ColumnResult(name = "descripcionserviciomedico"),
	                    @ColumnResult(name = "descuento"),
	            }
	    	)
		),
	@SqlResultSetMapping(
		    name = "ReporteCajaGeneral",
		    classes = @ConstructorResult(
		            targetClass = CajaReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "fechacomprobante"),
		                    @ColumnResult(name = "totalpordia")
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "ReporteAtenciones",
		    classes = @ConstructorResult(
		            targetClass = AtencionesReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "denominacionserviciomedico"),
		                    @ColumnResult(name = "descripcionserviciomedico"),
		                    @ColumnResult(name = "cantidad"),
		                    @ColumnResult(name = "turno")
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "Reportelab",
		    classes = @ConstructorResult(
		            targetClass = AtencionLabReport.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		            		@ColumnResult(name = "cant"),
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "ReporteDetallado",
		    classes = @ConstructorResult(
		            targetClass = CajaDetalladoReport.class,
		            columns = {
		            		@ColumnResult(name = "iddetallecomprobante"),
		                    @ColumnResult(name = "numerorecibocomprobante"),
		                    @ColumnResult(name = "nombrecategoriaexamenmedico"),
		                    @ColumnResult(name = "precio"),
		                    @ColumnResult(name = "precioserviciomedico"),
		                    @ColumnResult(name = "denominacionserviciomedico"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "cantidadfarmacia"),
		                    @ColumnResult(name = "cantidadtopico"),
		                    @ColumnResult(name = "cantidad"),
		                    @ColumnResult(name = "fechacomprobante"),
		                    @ColumnResult(name = "nombresyapellidos"),
		                    @ColumnResult(name = "estado"),
		            		 @ColumnResult(name = "descuento")
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "ReporteGeneralAtenciones",
		    classes = @ConstructorResult(
		            targetClass = AtencionesGeneral.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "denominacionserviciomedico"),
		                    @ColumnResult(name = "descripcionserviciomedico"),
		                    @ColumnResult(name = "cantidad"),
		                    @ColumnResult(name = "turno"),
		                    @ColumnResult(name = "fechaatencion")
		            }
		    	)
			),

	@SqlResultSetMapping(
		    name = "ReporteGeneralAMedico",
		    classes = @ConstructorResult(
		            targetClass = AtencionesGeneral.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "denominacionserviciomedico"),
		                    @ColumnResult(name = "descripcionserviciomedico"),
		                    @ColumnResult(name = "cantidad"),
		                    @ColumnResult(name = "turno"),
		                    @ColumnResult(name = "fechaatencion")
		            }
		    	)
			),

	@SqlResultSetMapping(
		    name = "ReporteGeneralAServicio",
		    classes = @ConstructorResult(
		            targetClass = AtencionesGeneral.class,
		            columns = {
		            		@ColumnResult(name = "ids"),
		                    @ColumnResult(name = "denominacionserviciomedico"),
		                    @ColumnResult(name = "descripcionserviciomedico"),
		                    @ColumnResult(name = "cantidad"),
		                    @ColumnResult(name = "turno"),
		                    @ColumnResult(name = "fechaatencion")
		            }
		    	)
			),
	@SqlResultSetMapping(
		    name = "NotacreditoCaja",
		    classes = @ConstructorResult(
		            targetClass = NCreditoFarmaciaReport.class,
		            columns = {
		            		@ColumnResult(name = "idordenfarmacia"),
		            		@ColumnResult(name = "numeroorden"),
		                    @ColumnResult(name = "tipo"),
		                    @ColumnResult(name = "serie"),
		                    @ColumnResult(name = "consumidorordenfarmacia"),
		                    @ColumnResult(name = "exonera"),
		                    @ColumnResult(name = "inafecto"),
		                    @ColumnResult(name = "total"),
		                    @ColumnResult(name = "fechaordenfarmacia")
		            }
		    	)
			)
	}
)

@NamedNativeQueries({
	@NamedNativeQuery(name = "ReporteComporbanteUnit",resultClass = ComprobanteReport.class, 
			query = "select cp.numerorecibocomprobante,cp.cantidadotros,cp.cantidadfarmacia,cp.cantidadtopico,cp.turno,\r\n" + 
					"cp.fechaatencion,cp.fechacomprobante,se.descripcionserviciomedico,\r\n" + 
					"det.iddetallecomprobante as ids, det.cantidad,ex.nombrecategoriaexamenmedico as denominacionexamenmedico,\r\n" + 
					"ex.precio as precioexmenmedico,pac.nombresyapellidos||'  -  HCL('||pac.hcl||')' as nombresyapellidos,\r\n" + 
					"se.denominacionserviciomedico, se.precioserviciomedico, sum(det.cantidad*ex.precio)as subtotal,\r\n" + 
					"cp.total as totalgeneral,cp.descuento  \r\n" + 
					"from comprobantepago cp full join detallecomprobante det on det.idcomprobantepago = cp.idcomprobantepago \r\n" + 
					"full join categoriaexamenmedico ex on ex.idcategoriaexamenmedico = det.idcategoriaexamenmedico \r\n" + 
					"full join serviciomedico se on se.idserviciomedico = cp.idserviciomedico\r\n" + 
					"full join paciente pac on pac.idpaciente = cp.idpaciente\r\n" + 
					"where numerorecibocomprobante = :numerorecibocomprobante \r\n" + 
					"group by cp.idcomprobantepago,det.cantidad,ex.nombrecategoriaexamenmedico,ex.precio,se.descripcionserviciomedico, \r\n" + 
					"pac.nombresyapellidos,se.denominacionserviciomedico,se.precioserviciomedico,pac.hcl,det.iddetallecomprobante\r\n" + 
					"order by totalgeneral desc"
			),
	@NamedNativeQuery(name = "ReporteCajaGeneral",resultClass = CajaReport.class, 
	query = "select cp.fechacomprobante, max(cp.numerorecibocomprobante) as ids,\r\n" + 
			"to_char(sum(cp.total), '99999999D99') as totalpordia \r\n" + 
			"from comprobantepago cp \r\n" + 
			"where cp.estado=1 and (cp.fechacomprobante BETWEEN TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY'))\r\n" + 
			"group by cp.fechacomprobante order by cp.fechacomprobante asc"
	),
	@NamedNativeQuery(name = "ReporteAtenciones",resultClass = AtencionesReport.class, 
	query = "select trunc(random() * 99999 + 10000) as ids,sv.denominacionserviciomedico,sv.descripcionserviciomedico,\r\n" + 
			"count(sv.denominacionserviciomedico) as cantidad, cp.turno \r\n" + 
			"from comprobantepago cp INNER join serviciomedico sv on sv.idserviciomedico = cp.idserviciomedico\r\n" + 
			"where cp.estado=1 and cp.fechaatencion = TO_DATE(:fechaatencion , 'DD/MM/YYYY')\r\n" + 
			"GROUP by sv.denominacionserviciomedico, sv.descripcionserviciomedico, cp.turno"
	),
	@NamedNativeQuery(name = "Reportelab",resultClass = AtencionLabReport.class, 
	query = "select count(ex.idcategoriaexamenmedico) as cant,cp.numerorecibocomprobante as ids from comprobantepago cp \r\n" + 
			"inner join detallecomprobante det ON det.idcomprobantepago = cp.idcomprobantepago\r\n" + 
			"inner join categoriaexamenmedico cat on cat.idcategoriaexamenmedico = det.idcategoriaexamenmedico\r\n" + 
			"inner join examenmedico ex on ex.idcategoriaexamenmedico = cat.idcategoriaexamenmedico\r\n" + 
			"where cp.fechaatlab = TO_DATE(:fechaatlab , 'DD/MM/YYYY') and cp.estado=1 and cat.nombrecategoriaexamenmedico<>'Ninguno'\r\n" + 
			"group by fechaatlab,numerorecibocomprobante\r\n" + 
			"order by numerorecibocomprobante desc"
	),
	@NamedNativeQuery(name = "ReporteDetallado",resultClass = CajaDetalladoReport.class, 
	query = "select det.iddetallecomprobante,cp.numerorecibocomprobante,cat.nombrecategoriaexamenmedico,cat.precio,se.precioserviciomedico,\r\n" + 
			"se.denominacionserviciomedico,cp.total,cp.cantidadfarmacia,cp.cantidadtopico,\r\n" + 
			"cp.fechacomprobante,pac.nombresyapellidos,cp.estado,det.cantidad,cp.descuento\r\n" + 
			"from comprobantepago cp inner join paciente pac on pac.idpaciente = cp.idpaciente \r\n" + 
			"inner join detallecomprobante det on det.idcomprobantepago = cp.idcomprobantepago\r\n" + 
			"inner join categoriaexamenmedico cat on cat.idcategoriaexamenmedico = det.idcategoriaexamenmedico\r\n" + 
			"inner join serviciomedico se on se.idserviciomedico = cp.idserviciomedico\r\n" + 
			"where cp.fechacomprobante = TO_DATE(:fechacomprobante , 'DD/MM/YYYY')\r\n" + 
			"order by cp.numerorecibocomprobante desc"
	),
	@NamedNativeQuery(name = "ReporteGeneralAtenciones",resultClass = AtencionesGeneral.class, 
	query = "SELECT trunc(random() * 99999 + 10000) as ids,count(cp.idcomprobantepago) as cantidad,\r\n" + 
			"cp.fechaatencion ,se.descripcionserviciomedico, se.denominacionserviciomedico, cp.turno \r\n" + 
			"FROM comprobantepago cp inner join serviciomedico se on se.idserviciomedico = cp.idserviciomedico\r\n" + 
			"WHERE (cp.fechaatencion between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')) and se.denominacionserviciomedico <> 'Ninguno'\r\n" + 
			"group by se.descripcionserviciomedico,cp.fechaatencion,se.denominacionserviciomedico,cp.turno \r\n"+
			"order by cp.fechaatencion desc"
	),
	@NamedNativeQuery(name = "ReporteGeneralAMedico",resultClass = AtencionesGeneral.class, 
	query = "SELECT trunc(random() * 99999 + 10000) as ids,count(cp.idcomprobantepago) as cantidad,\r\n" + 
			"cp.fechaatencion ,se.descripcionserviciomedico, se.denominacionserviciomedico, cp.turno \r\n" + 
			"FROM comprobantepago cp inner join serviciomedico se on se.idserviciomedico = cp.idserviciomedico\r\n" + 
			"WHERE (cp.fechaatencion between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY'))\r\n" + 
			"AND ( se.descripcionserviciomedico = :descripcionserviciomedico) and se.denominacionserviciomedico <> 'Ninguno' \r\n" + 
			"group by se.descripcionserviciomedico,cp.fechaatencion,se.denominacionserviciomedico,cp.turno \r\n"+
			"order by cp.fechaatencion desc"
	),
	@NamedNativeQuery(name = "ReporteGeneralAServicio",resultClass = AtencionesGeneral.class, 
	query = "SELECT trunc(random() * 99999 + 10000) as ids,count(cp.idcomprobantepago) as cantidad,\r\n" + 
			"cp.fechaatencion ,se.descripcionserviciomedico, se.denominacionserviciomedico, cp.turno \r\n" + 
			"FROM comprobantepago cp inner join serviciomedico se on se.idserviciomedico = cp.idserviciomedico\r\n" + 
			"WHERE (cp.fechaatencion between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')) and \r\n" + 
			"(se.denominacionserviciomedico = :denominacionserviciomedico) and se.denominacionserviciomedico <> 'Ninguno' \r\n" + 
			"group by se.descripcionserviciomedico,cp.fechaatencion,se.denominacionserviciomedico,cp.turno \r\n" + 
			"order by cp.fechaatencion desc"
	),
	@NamedNativeQuery(name = "NotacreditoCaja",resultClass = NCreditoFarmaciaReport.class, 
	query = "select cp.idcomprobantepago as idordenfarmacia,cp.fechacomprobante as fechaordenfarmacia, to_char( cp.tipo, 'FM09') as tipo,to_char(cp.serie, 'FM999099') as serie,\r\n" + 
			"to_char( cp.tiporef, 'FM09') as tiporef,to_char( cp.serieref, 'FM099') as serieref,\r\n" + 
			"to_char( cp.numeroref, 'FM099999') as numeroref, cp.fechacompref as fechaordenref,cp.totalref, \r\n" + 
			" to_char( cp.numerorecibocomprobante, 'FM099999') as numeroorden,pac.nombresyapellidos as consumidorordenfarmacia,\r\n" + 
			"(cast(cp.descuento as DOUBLE PRECISION)/100 )*cp.total as exonera, \r\n" + 
			"cp.total - (cast(cp.descuento as DOUBLE PRECISION)/100)*cp.total as inafecto, cp.total\r\n" + 
			"from comprobantepago cp inner join paciente pac on cp.idpaciente = pac.idpaciente \r\n" + 
			"where cp.fechacomprobante between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')\r\n" + 
			"and cp.estado=1 order by cp.serie asc,cp.tipo asc,cp.numerorecibocomprobante asc"
	)
})




@Entity
@Table(name="comprobantepago")
public class ComprobantePago { 
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idcomprobantepago;
	
	@Column(name="fechacomprobante")
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechacomprobante;
	
	@Column(name="numerorecibocomprobante", unique=true)
	private int numerorecibocomprobante;
		
	@Column
	private double cantidadotros;
	
	@Column
	private double cantidadtopico;
	
	@Column
	private String turno;
	
	@Column
	private double total;
	
	@Column
	private int descuento;
	
	@Column
	private int estado;
	
	@Column
	private int serieref;
	
	@Column(name = "fechacompref")
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechacompref;
	
	@Column
	private int tiporef;
	
	@Column
	private int numeroref;
	
	@Column
	private double totalref;
	
	@Column
	private int tipo;
	
	@Column
	private int serie;
	
	@Column
	private double cantidadfarmacia;
	
	@Column(name="fechaatencion")
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechaatencion;
	
	@Column(name="fechaatlab")
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechaatlab;
	
	@OneToOne
	@JoinColumn(name="idpaciente")
	private Paciente paciente; 
	
	@OneToOne
	@JoinColumn(name ="idserviciomedico")
	private ServicioMedico serviciomedico; 
	
	@OneToMany(mappedBy = "comprobantepago", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
			fetch=FetchType.LAZY, orphanRemoval = true)
	private List<DetalleComprobante> detallecomprobante;

	public int getIdcomprobantepago() {
		return idcomprobantepago;
	}

	public void setIdcomprobantepago(int idcomprobantepago) {
		this.idcomprobantepago = idcomprobantepago;
	}

	public LocalDate getFechacomprobante() {
		return fechacomprobante;
	}

	public void setFechacomprobante(LocalDate fechacomprobante) {
		this.fechacomprobante = fechacomprobante;
	}

	public int getNumerorecibocomprobante() {
		return numerorecibocomprobante;
	}

	public void setNumerorecibocomprobante(int numerorecibocomprobante) {
		this.numerorecibocomprobante = numerorecibocomprobante;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public ServicioMedico getServiciomedico() {
		return serviciomedico;
	}

	public void setServiciomedico(ServicioMedico serviciomedico) {
		this.serviciomedico = serviciomedico;
	}

	public List<DetalleComprobante> getDetallecomprobante() {
		return detallecomprobante;
	}

	public void setDetallecomprobante(List<DetalleComprobante> detallecomprobante) {
		this.detallecomprobante = detallecomprobante;
	}

	public double getCantidadotros() {
		return cantidadotros;
	}

	public void setCantidadotros(double cantidadotros) {
		this.cantidadotros = cantidadotros;
	}

	public double getCantidadtopico() {
		return cantidadtopico;
	}

	public void setCantidadtopico(double cantidadtopico) {
		this.cantidadtopico = cantidadtopico;
	}

	public double getCantidadfarmacia() {
		return cantidadfarmacia;
	}

	public void setCantidadfarmacia(double cantidadfarmacia) {
		this.cantidadfarmacia = cantidadfarmacia;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public LocalDate getFechaatencion() {
		return fechaatencion;
	}

	public void setFechaatencion(LocalDate fechaatencion) {
		this.fechaatencion = fechaatencion;
	}
	
	

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	public LocalDate getFechaatlab() {
		return fechaatlab;
	}

	public void setFechaatlab(LocalDate fechaatlab) {
		this.fechaatlab = fechaatlab;
	}
	
	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}
	

	public int getSerieref() {
		return serieref;
	}

	public void setSerieref(int serieref) {
		this.serieref = serieref;
	}

	public LocalDate getFechacompref() {
		return fechacompref;
	}

	public void setFechacompref(LocalDate fechacompref) {
		this.fechacompref = fechacompref;
	}

	public int getTiporef() {
		return tiporef;
	}

	public void setTiporef(int tiporef) {
		this.tiporef = tiporef;
	}

	public int getNumeroref() {
		return numeroref;
	}

	public void setNumeroref(int numeroref) {
		this.numeroref = numeroref;
	}

	public double getTotalref() {
		return totalref;
	}

	public void setTotalref(double totalref) {
		this.totalref = totalref;
	}

	@Override
	public String toString() {
		return "ComprobantePago [idcomprobantepago=" + idcomprobantepago + ", fechacomprobante=" + fechacomprobante
				+ ", numerorecibocomprobante=" + numerorecibocomprobante + ", cantidadotros=" + cantidadotros
				+ ", cantidadtopico=" + cantidadtopico + ", cantidadfarmacia=" + cantidadfarmacia + ", paciente="
				+ paciente + ", serviciomedico=" + serviciomedico + ", detallecomprobante=" + detallecomprobante + "]";
	}

}
