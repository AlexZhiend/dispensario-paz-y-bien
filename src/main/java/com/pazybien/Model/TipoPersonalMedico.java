package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tipopersonalmedico")
public class TipoPersonalMedico {

	@Id
	@Column(name = "idtipopersonalmedico")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtipopersonalmedico;
	
	@Column(name="cargopersonalmedico",nullable=false)
	private String cargopersonalmedico;
	
	@Column(name="descripcionpersonalmedico",nullable=false)
	private String descripcionpersonalmedico;

	public int getIdtipopersonalmedico() {
		return idtipopersonalmedico;
	}

	public void setIdtipopersonalmedico(int idtipopersonalmedico) {
		this.idtipopersonalmedico = idtipopersonalmedico;
	}

	public String getCargopersonalmedico() {
		return cargopersonalmedico;
	}

	public void setCargopersonalmedico(String cargopersonalmedico) {
		this.cargopersonalmedico = cargopersonalmedico;
	}

	public String getDescripcionpersonalmedico() {
		return descripcionpersonalmedico;
	}

	public void setDescripcionpersonalmedico(String descripcionpersonalmedico) {
		this.descripcionpersonalmedico = descripcionpersonalmedico;
	}

	public TipoPersonalMedico(int idtipopersonalmedico, String cargopersonalmedico, String descripcionpersonalmedico) {
		super();
		this.idtipopersonalmedico = idtipopersonalmedico;
		this.cargopersonalmedico = cargopersonalmedico;
		this.descripcionpersonalmedico = descripcionpersonalmedico;
	}

	public TipoPersonalMedico() {
		super();
	}


	
}
