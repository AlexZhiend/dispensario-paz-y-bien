package com.pazybien.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="intervenciontopico")
public class IntervencionTopico {
	
	@Id
	@Column(name="idintervenciontopico")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idintervenciontopico;
	
	@Column(name="observacionintervenciontopico", nullable=false)
	private String observacionintervenciontopico;
	
	@Column(name="cantidadintervenciontopico", nullable=false)
	private int cantidadintervenciontopico;
	
	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "idtipointervencion")
	private TipoIntervencion tipointervencion;

	public int getIdintervenciontopico() {
		return idintervenciontopico;
	}

	public void setIdintervenciontopico(int idintervenciontopico) {
		this.idintervenciontopico = idintervenciontopico;
	}

	public String getObservacionintervenciontopico() {
		return observacionintervenciontopico;
	}

	public void setObservacionintervenciontopico(String observacionintervenciontopico) {
		this.observacionintervenciontopico = observacionintervenciontopico;
	}

	public int getCantidadintervenciontopico() {
		return cantidadintervenciontopico;
	}

	public void setCantidadintervenciontopico(int cantidadintervenciontopico) {
		this.cantidadintervenciontopico = cantidadintervenciontopico;
	}

	public TipoIntervencion getTipointervencion() {
		return tipointervencion;
	}

	public void setTipointervencion(TipoIntervencion tipointervencion) {
		this.tipointervencion = tipointervencion;
	}

	public IntervencionTopico(int idintervenciontopico, String observacionintervenciontopico,
			int cantidadintervenciontopico, TipoIntervencion tipointervencion) {
		super();
		this.idintervenciontopico = idintervenciontopico;
		this.observacionintervenciontopico = observacionintervenciontopico;
		this.cantidadintervenciontopico = cantidadintervenciontopico;
		this.tipointervencion = tipointervencion;
	}

	public IntervencionTopico(int idintervenciontopico, String observacionintervenciontopico,
			int cantidadintervenciontopico) {
		super();
		this.idintervenciontopico = idintervenciontopico;
		this.observacionintervenciontopico = observacionintervenciontopico;
		this.cantidadintervenciontopico = cantidadintervenciontopico;
	}

	public IntervencionTopico() {
		super();
	}
	
	
}
