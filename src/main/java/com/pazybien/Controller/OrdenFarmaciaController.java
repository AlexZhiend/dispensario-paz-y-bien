package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.DTO.OrdenListaProductoDTO;
import com.pazybien.EntityReport.TotalxDiaFarmacia;
import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.OrdenFarmacia;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.ServiceImpl.OrdenFarmaciaServiceImpl;

@RestController
@RequestMapping("/ordenfarmacia")
public class OrdenFarmaciaController {

	@Autowired
	private OrdenFarmaciaServiceImpl ordenFarmaciaServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> RegistrarOrdenFarmacia(@Valid @RequestBody OrdenFarmacia ordenFarmacia){
		
		OrdenFarmacia of= new OrdenFarmacia();
		of= ordenFarmaciaServiceImpl.registrar(ordenFarmacia);
		URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ordenFarmacia.getIdordenfarmacia()).toUri();
		
		return ResponseEntity.created(location).build();	
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenFarmacia> ActualizarOrdenFarmacia(@Valid @RequestBody OrdenFarmacia ordenFarmacia){
		ordenFarmaciaServiceImpl.actualizar(ordenFarmacia);
		return new ResponseEntity<OrdenFarmacia>(HttpStatus.OK);
	}
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrdenFarmacia>> ListarOrdenFarmacia(){
		List<OrdenFarmacia> ordenFarmacias=new ArrayList<>();
		ordenFarmacias = ordenFarmaciaServiceImpl.listar();
		return new ResponseEntity<List<OrdenFarmacia>>(ordenFarmacias, HttpStatus.OK);
	}
	
	@GetMapping(value="/ultimo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenFarmacia> buscarOrden(){
		OrdenFarmacia ordenfarmacia = new OrdenFarmacia();
		ordenfarmacia = ordenFarmaciaServiceImpl.buscarultimo();
		return new ResponseEntity<OrdenFarmacia>(ordenfarmacia, HttpStatus.OK);
	
	}
	
	@GetMapping(value = "/reporteOrdenF/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteOrdenF(@PathVariable("id") Integer numeroorden) {
		byte[] data = null;
		data = ordenFarmaciaServiceImpl.generarReporteOrdenFarmacia(numeroorden);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/totalxdia/{fecha}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TotalxDiaFarmacia> GenerarTotalxDiaFarmacia(@PathVariable("fecha") String fecha) {
		TotalxDiaFarmacia totalF=new TotalxDiaFarmacia();
		totalF = ordenFarmaciaServiceImpl.totalxdia(fecha);
		return new ResponseEntity<TotalxDiaFarmacia>(totalF, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteDetallado/{fechaordenfarmacia}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteDetalladoFarmacia(@PathVariable("fechaordenfarmacia") String fechaordenfarmacia) {
		byte[] data = null;
		data = ordenFarmaciaServiceImpl.generarRDetalladoFarmacia(fechaordenfarmacia);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteDetalladoxlsx/{fechaordenfarmacia}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteDetalladoFarmaciaXLSX(@PathVariable("fechaordenfarmacia") String fechaordenfarmacia) {
		byte[] data = null;
		data = ordenFarmaciaServiceImpl.generarRDetalladoFarmaciaXLSX(fechaordenfarmacia);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/vigente/{numeroorden}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrdenFarmacia> buscarOrdenPorNumero(@PathVariable("numeroorden") int numeroorden){
		OrdenFarmacia ordenfarmacia = new OrdenFarmacia();
		ordenfarmacia = ordenFarmaciaServiceImpl.buscarVigente(numeroorden);
		return new ResponseEntity<OrdenFarmacia>(ordenfarmacia, HttpStatus.OK);
	
	}
	
	
	
	@GetMapping(value = "/reporteNCFarmacia/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteNCFarmacia(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = ordenFarmaciaServiceImpl.generarNCfarmaciaReport(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteNCFarmaciaxlsx/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteNCFarmaciaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = ordenFarmaciaServiceImpl.generarNCfarmaciaReportXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
}
