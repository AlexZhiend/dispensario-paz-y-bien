package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.PersonalMedico;
import com.pazybien.Service.ServiceImpl.PersonalMedicoServiceImpl;

@RestController
@RequestMapping("/personalmedico")
public class PersonalMedicoController {
	@Autowired
	private PersonalMedicoServiceImpl personalMedicoServiceImpl;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonalMedico>> listarpersonalmedico(){
		List<PersonalMedico> personalesmedicos = new ArrayList<>();
		personalesmedicos = personalMedicoServiceImpl.listar();
		return new ResponseEntity<List<PersonalMedico>>(personalesmedicos,HttpStatus.OK);
	}
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> RegistrarPersonalMedico(@Valid @RequestBody PersonalMedico personalmedico){
		personalMedicoServiceImpl.registrar(personalmedico);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(personalmedico.getDnipersonalmedico()).toUri();
		return ResponseEntity.created(location).build();
	}
	
		
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarPersonalMedico(@Valid @RequestBody PersonalMedico personalmedico){
		personalMedicoServiceImpl.actualizar(personalmedico);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/{dnipersonalmedico}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PersonalMedico> BuscarPersonalMedico(@PathVariable ("dnipersonalmedico") String dnipersonalmedico){
		PersonalMedico personalmedico=new PersonalMedico();
		personalmedico=personalMedicoServiceImpl.buscarId(dnipersonalmedico);
		if (personalmedico==null) {
			throw new ModeloNotFoundException("ID:"+ dnipersonalmedico);
		}

		return new ResponseEntity<PersonalMedico>(personalmedico, HttpStatus.OK);
	}
	
	@GetMapping(value = "/medicos",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonalMedico>> listarSoloMedicos(){
		List<PersonalMedico> personalesmedicos = new ArrayList<>();
		personalesmedicos = personalMedicoServiceImpl.listarsolomedicos();
		return new ResponseEntity<List<PersonalMedico>>(personalesmedicos,HttpStatus.OK);
	}
	
	@GetMapping(value = "/radyeco",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonalMedico>> listarRadyEco(){
		List<PersonalMedico> personalesmedicos = new ArrayList<>();
		personalesmedicos = personalMedicoServiceImpl.listarEcoRad();
		return new ResponseEntity<List<PersonalMedico>>(personalesmedicos,HttpStatus.OK);
	}
	
	
	
	
	
	@GetMapping(value = "/reporteAtencionEcoRad/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionEcoRad(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = personalMedicoServiceImpl.generarAtencionEcoRad1(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionEcoRadXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionEcoRadXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = personalMedicoServiceImpl.generarAtencionEcoRad1XLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	@GetMapping(value = "/reporteAtencionEcoRad2/{fechainicio}/{fechafin}/{dnipersonalmedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionEcoRad2(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("dnipersonalmedico") String dnipersonalmedico) {
		byte[] data = null;
		data = personalMedicoServiceImpl.generarAtencionEcoRad2(fechainicio, fechafin, dnipersonalmedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionEcoRad2XLSX/{fechainicio}/{fechafin}/{dnipersonalmedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionEcoRad2XLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("dnipersonalmedico") String dnipersonalmedico) {
		byte[] data = null;
		data = personalMedicoServiceImpl.generarAtencionGEcoRad2XLSX(fechainicio, fechafin, dnipersonalmedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
}
