package com.pazybien.Controller;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.ServiceImpl.PacienteServiceImpl;
import com.pazybien.Util.Httpsocket;


@RestController
@RequestMapping("/paciente")
public class PacienteController {
	
	@Autowired
	private PacienteServiceImpl pacienteServiceImpl;
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Paciente>> listarPacientes(){
		List<Paciente> pacientes = new ArrayList<>();
		pacientes=pacienteServiceImpl.listar();
		return new ResponseEntity<List<Paciente>>(pacientes,HttpStatus.OK); 
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> RegistrarPaciente(@Valid @RequestBody Paciente paciente) {
		System.out.println("controller");
		try {
			Paciente pac = pacienteServiceImpl.registrar(paciente);
			URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(paciente.getHcl()).toUri();
			return ResponseEntity.created(location).build();
		} catch (Exception e) {
			return null;
		}
		
//		String hcl=pac.getHcl();
//		Httpsocket socket = new Httpsocket();
//		socket.socket(hcl);
		
//		String uri = "http://127.0.0.1:3000/notificacion/{hcl}";
//		ResponseEntity<String> responseentity = restTemp.exchange(uri, HttpMethod.GET, null,String.class );
		
	}
	
	@PutMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizarPaciente(@Valid @RequestBody Paciente paciente) {
		pacienteServiceImpl.actualizar(paciente);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	
	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> buscarPaciente( @PathVariable ("id") int id){
		Paciente paciente = new Paciente();
		paciente = pacienteServiceImpl.buscarPorid(id);
		if (paciente==null) {
			throw new ModeloNotFoundException("ID:"+ id);			
		}
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
		
}
	@GetMapping(value="/ultimoInterno", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> buscarInterno(){
		Paciente paciente = new Paciente();
		paciente = pacienteServiceImpl.buscarInterno();
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
	
	@GetMapping(value="/ultimoExterno", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> buscarExterno(){
		Paciente paciente = new Paciente();
		paciente = pacienteServiceImpl.buscarExterno();	
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
	
	@Autowired
	private RestTemplate restTemp;
	
	@GetMapping(value = "/getLeads/{id}/{hcl}")
	public String getLeads(@PathVariable("id") int id,@PathVariable(value="hcl", required = false) int hcl){
		System.out.println("probando");
		int ids=id;
		int hclsel= hcl;
		String uri = "http://127.0.0.1:3000/notificacion/"+ids+"/"+hclsel;
		ResponseEntity<String> responseentity = restTemp.exchange(uri, HttpMethod.GET, null,String.class );
		return responseentity.getBody();
	}
	
	
	
	@GetMapping(value="/buscarrepetido/{hcl}/{nombresyapellidos}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> buscarHCLexistente(@PathVariable("hcl") int hcl,@PathVariable("nombresyapellidos") String nombresyapellidos){
		Paciente paciente = new Paciente();
		paciente = pacienteServiceImpl.buscarhcl(hcl,nombresyapellidos);
		if (paciente==null) {
			return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);	
		}
		return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
	}
	
	@GetMapping(value="/buscarPorNombre/{nombresyapellidos}", produces = "application/json")
	public ResponseEntity<List<Paciente>> BuscarPorNombre(@PathVariable("nombresyapellidos") String nombresyapellidos){
		List<Paciente> pacientes = new ArrayList<>();
		pacientes=pacienteServiceImpl.BuscarPorNombre(nombresyapellidos);
		return new ResponseEntity<List<Paciente>>(pacientes,HttpStatus.OK); 
	}
	
}
