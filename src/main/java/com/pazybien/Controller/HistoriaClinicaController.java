package com.pazybien.Controller;


import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.EntityReport.Medicoxdia;
import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.HistoriaClinica;
import com.pazybien.Service.ServiceImpl.ComprobantePagoServiceImpl;
import com.pazybien.Service.ServiceImpl.HistoriaClinicaServiceImpl;

@RestController
@RequestMapping("/historiaclinica")
public class HistoriaClinicaController {
	
	@Autowired
	private HistoriaClinicaServiceImpl historiaClinicaServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoriaClinica> RegistrarHistoriaClinica(@Valid @RequestBody HistoriaClinica historiaclinica){
		
		HistoriaClinica hc= new HistoriaClinica();
		hc= historiaClinicaServiceImpl.registrar(historiaclinica);

		return new ResponseEntity<HistoriaClinica>(hc, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoriaClinica>> ListarHistoriaClinica() {
		List<HistoriaClinica> historiasclinicas= new ArrayList<>();
		historiasclinicas=historiaClinicaServiceImpl.listar();
		return new ResponseEntity<List<HistoriaClinica>>(historiasclinicas,HttpStatus.OK);		
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarHistoriaClinica(@Valid @RequestBody HistoriaClinica historiaclinica){
		historiaClinicaServiceImpl.actualizar(historiaclinica);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/historia/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarHistoria(@PathVariable("id") Integer idhistoriaclinica) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.generarHistoria(idhistoriaclinica);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/historiavacia/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarHistoriaVacia(@PathVariable("id") Integer idhistoriaclinica) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.generarHistoriaVacia(idhistoriaclinica); 
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
		
	@GetMapping(value = "/medicoxdia/{fecha}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medicoxdia>> generarMedicoxdia(@PathVariable("fecha") String fecha) {
		List<Medicoxdia> medicoxdia= new ArrayList<>();
		medicoxdia=historiaClinicaServiceImpl.listarMedicosxdia(fecha);
		return new ResponseEntity<List<Medicoxdia>>(medicoxdia, HttpStatus.OK);
	}
	
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminarAtencionHistoria(@PathVariable Integer id) {
		HistoriaClinica hc = historiaClinicaServiceImpl.buscarHistoriaPorId(id);
		if (hc == null) {
			throw new ModeloNotFoundException("ID: " + id);
		} else {
			historiaClinicaServiceImpl.eliminarHistoria(id);
		}
	}
	
	
	
	
	@GetMapping(value = "/medicoreconsulta/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<byte[]> GenerarMedicoReconsulta(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.RepMedicoxreconsulta(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/medicoreconsultaXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<byte[]> GenerarMedicoReconsultaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.RepMedicoxreconsultaXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/medicoreconsulta2/{fechainicio}/{fechafin}/{reconsulta}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<byte[]> GenerarMedicoReconsulta2(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("reconsulta") String reconsulta) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.RepMedicoxreconsulta2(fechainicio, fechafin, reconsulta);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/medicoreconsulta2XLSX/{fechainicio}/{fechafin}/{reconsulta}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<byte[]> GenerarMedicoReconsulta2XLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("reconsulta") String reconsulta) {
		byte[] data = null;
		data = historiaClinicaServiceImpl.RepMedicoxreconsulta2XLSX(fechainicio, fechafin, reconsulta);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
		
	@GetMapping(value = "/listarXfecha/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoriaClinica>> ListarHistoriaClinicaPorFecha(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		List<HistoriaClinica> historiasclinicas= new ArrayList<>();
		historiasclinicas=historiaClinicaServiceImpl.listarXfecha(fechainicio, fechafin);
		return new ResponseEntity<List<HistoriaClinica>>(historiasclinicas,HttpStatus.OK);		
	}
	
	@GetMapping(value = "/listarXpaciente/{nombresyapellidos}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoriaClinica>> ListarHistoriaClinicaPorPaciente(@PathVariable("nombresyapellidos") String nombresyapellidos) {
		List<HistoriaClinica> historiasclinicas= new ArrayList<>();
		historiasclinicas=historiaClinicaServiceImpl.listarXPaciente(nombresyapellidos);
		return new ResponseEntity<List<HistoriaClinica>>(historiasclinicas,HttpStatus.OK);		
	}
	
	
	@GetMapping(value = "/listarXhcl/{hcl}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<HistoriaClinica>> ListarHistoriaClinicaPorHCL(@PathVariable("hcl") int hcl) {
		List<HistoriaClinica> historiasclinicas= new ArrayList<>();
		historiasclinicas=historiaClinicaServiceImpl.listarporHcl(hcl);
		return new ResponseEntity<List<HistoriaClinica>>(historiasclinicas,HttpStatus.OK);		
	}
	
	
	
	/**
	 @GetMapping(value = "/buscarhistoriaclinica/{numero}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HistoriaClinica> BuscarHistoriaClinica(@PathVariable ("numero") String hcl) {
		HistoriaClinica historiasclinicas =new HistoriaClinica();
		try {
			historiasclinicas=historiaClinicaServiceImpl.buscarHistoriaClinica(hcl);
		} catch (Exception e) {
			new ResponseEntity<HistoriaClinica>(historiasclinicas,HttpStatus.INTERNAL_SERVER_ERROR);
			// TODO: handle exception
		}
		return new ResponseEntity<HistoriaClinica>(historiasclinicas,HttpStatus.OK);
	}
	 **/

}
