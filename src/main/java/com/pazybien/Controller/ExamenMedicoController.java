package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.ExamenMedico;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.ServiceImpl.ExamenMedicoServiceImpl;

@RestController
@RequestMapping("/examenmedico")
public class ExamenMedicoController {
	
	@Autowired
	private ExamenMedicoServiceImpl examenMedicoServiceImpl;
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarExamenMedico(@Valid @RequestBody ExamenMedico examenMedico) {

		examenMedicoServiceImpl.registrar(examenMedico);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(examenMedico.getIdexamenmedico()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarExamenMedico(@Valid @RequestBody ExamenMedico examenMedico) {

		examenMedicoServiceImpl.actualizar(examenMedico);

		return new ResponseEntity<Object>(HttpStatus.OK);	
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ExamenMedico>> ListarExamenMedico() {
	List<ExamenMedico> examenMedicos=new ArrayList<>();
	examenMedicos=examenMedicoServiceImpl.listar();

	return new ResponseEntity<List<ExamenMedico>>(examenMedicos,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ExamenMedico> listarId(@PathVariable("id") Integer id) {
		ExamenMedico examenMedico = new ExamenMedico();
		examenMedico = examenMedicoServiceImpl.buscarId(id);
		if (examenMedico == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<ExamenMedico>(examenMedico, HttpStatus.OK);
	}
	
	@GetMapping(value = "/buscar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ExamenMedico>> BuscarExamenMedicoPorCategoria(@PathVariable("id") int id) {
		List<ExamenMedico> examenMedicos=new ArrayList<>();
		examenMedicos = examenMedicoServiceImpl.buscarPorCategoria(id);
		
		if (examenMedicos == null) {
			throw new ModeloNotFoundException("ID:" + id);
		}	
		return new ResponseEntity<List<ExamenMedico>>(examenMedicos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteLab/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCaja(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = examenMedicoServiceImpl.generarReporteGeneral(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteLabXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCajaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = examenMedicoServiceImpl.generarReporteGeneralXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
		
}
