package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ServicioMedico;
import com.pazybien.Service.ServiceImpl.ServicioMedicoServiceImpl;

@RestController
@RequestMapping("/serviciomedico")
public class ServicioMedicoController {
	
	@Autowired
	private ServicioMedicoServiceImpl servicioMedicoServiceImpl;
	
	@PostMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarServicioMedico(@Valid @RequestBody ServicioMedico servicioMedico) {

		servicioMedicoServiceImpl.registrar(servicioMedico);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(servicioMedico.getIdserviciomedico()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE , produces=MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Object> ListarServicioMedico(@Valid @RequestBody ServicioMedico servicioMedico){
		servicioMedicoServiceImpl.actualizar(servicioMedico);

		return new ResponseEntity<Object>(HttpStatus.OK );
		
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ServicioMedico>> ListarServicioMedico(){
		List<ServicioMedico> servicioMedicos = new ArrayList<ServicioMedico>();
		servicioMedicos=servicioMedicoServiceImpl.listar();
		
		return new ResponseEntity<List<ServicioMedico>>(servicioMedicos, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ServicioMedico> listarId(@PathVariable("id") Integer id) {
		ServicioMedico servicioMedico = new ServicioMedico();
		servicioMedico = servicioMedicoServiceImpl.buscarId(id);
		if (servicioMedico == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<ServicioMedico>(servicioMedico, HttpStatus.OK);
	}
	
}
