package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.ServiceImpl.CategoriaExamenMedicoServiceImpl;


@RestController
@RequestMapping("/categoriaexamenmedico")
public class CategoriaExamenMedicoController {
	
	@Autowired
	private CategoriaExamenMedicoServiceImpl categoriaExamenMedicoServiceImpl;
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Integer> RegistrarCategoriaExamenMedico(@Valid @RequestBody CategoriaExamenMedico categoriaExamenMedico) {

		categoriaExamenMedicoServiceImpl.registrar(categoriaExamenMedico);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(categoriaExamenMedico.getIdcategoriaexamenmedico()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarCategoriaExamenMedico(@Valid @RequestBody CategoriaExamenMedico categoriaExamenMedico) {
		categoriaExamenMedicoServiceImpl.actualizar(categoriaExamenMedico);
		
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CategoriaExamenMedico>> ListarCategoriaExamenMedico() {
		List<CategoriaExamenMedico> categoriaExamenMedicos= new ArrayList<>();
		categoriaExamenMedicos= categoriaExamenMedicoServiceImpl.listar();
		
		return 	new ResponseEntity<List<CategoriaExamenMedico>>(categoriaExamenMedicos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CategoriaExamenMedico> listarId(@PathVariable("id") Integer id) {
		CategoriaExamenMedico categoriaExamenMedico = new CategoriaExamenMedico();
		categoriaExamenMedico = categoriaExamenMedicoServiceImpl.buscarId(id);
		if (categoriaExamenMedico == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<CategoriaExamenMedico>(categoriaExamenMedico, HttpStatus.OK);
	}
	
	@GetMapping(value = "/buscar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CategoriaExamenMedico>> BuscarCategoriaId(@PathVariable("id") Integer id) {
		List<CategoriaExamenMedico> cates= new ArrayList<>();
		cates= categoriaExamenMedicoServiceImpl.BuscarCategoriaPorComprobante(id);

		return new ResponseEntity<List<CategoriaExamenMedico>>(cates, HttpStatus.OK);
	}	
}
