package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ExamenesG;
import com.pazybien.Service.ServiceImpl.ExamenesGServiceImpl;

@RestController
@RequestMapping("/examenesg")
public class ExamenesGController {

	@Autowired
	private ExamenesGServiceImpl examenesGServiceImpl;
	
	@PostMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> RegistrarExamenesG(@Valid @RequestBody ExamenesG examenesg) {
		
		examenesGServiceImpl.registrar(examenesg);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(examenesg.getIdexamenesg()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping( consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarExamenesG(@Valid @RequestBody ExamenesG examenesg) {
		
		examenesGServiceImpl.actualizar(examenesg);

		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping( produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ExamenesG>> ListarExamenesG() {
		List<ExamenesG> examenesGs =new ArrayList<>();
		examenesGs= examenesGServiceImpl.listar();

		return new ResponseEntity<List<ExamenesG>>(examenesGs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ExamenesG> listarId(@PathVariable("id") Integer id) {
		ExamenesG examenesG = new ExamenesG();
		examenesG = examenesGServiceImpl.buscarId(id);
		if (examenesG == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<ExamenesG>(examenesG, HttpStatus.OK);
	}
	

	@GetMapping(value = "/reporteLab/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarTotalLab(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = examenesGServiceImpl.generarTotalLab(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
}
