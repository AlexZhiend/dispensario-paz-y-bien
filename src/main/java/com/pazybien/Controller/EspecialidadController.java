package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.Especialidad;
import com.pazybien.Service.ServiceImpl.EspecialidadServiceImpl;

@RestController
@RequestMapping("/especialidad")
public class EspecialidadController {

	@Autowired
	private EspecialidadServiceImpl especialidadServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarEspecialidad(@Valid	@RequestBody Especialidad especialidad){
		
		especialidadServiceImpl.registrar(especialidad);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(especialidad.getIdespecialidad()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarEspecialidad(@Valid @RequestBody Especialidad especialidad) {
		especialidadServiceImpl.actualizar(especialidad);
		return new ResponseEntity<Object>( HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Especialidad>> ListarEspecialidad(){
		List<Especialidad> especialidades = new ArrayList<>();
		especialidades=especialidadServiceImpl.listar();
		
		return new ResponseEntity<List<Especialidad>>(especialidades, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id) {
		Especialidad especialidad = new Especialidad();
		especialidad = especialidadServiceImpl.buscarId(id);
		if (especialidad == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Especialidad>(especialidad, HttpStatus.OK);
	}
	
}
