package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.TipoPaciente;
import com.pazybien.Service.ServiceImpl.TipoPacienteServiceImpl;

@RestController
@RequestMapping("/tipopaciente")
public class TipoPacienteController {

	@Autowired
	private TipoPacienteServiceImpl tipoPacienteServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarTipoPaciente(@Valid @RequestBody TipoPaciente tipoPaciente) {
		
		tipoPacienteServiceImpl.registrar(tipoPaciente);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(tipoPaciente.getIdtipopaciente()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> ActualizarTipoPaciente(@Valid @RequestBody TipoPaciente tipoPaciente) {

		tipoPacienteServiceImpl.actualizar(tipoPaciente);

		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TipoPaciente>> ListarTipoPaciente() {
		List<TipoPaciente> tipopacientes = new ArrayList<>();
		tipopacientes=tipoPacienteServiceImpl.listar();
	
		return new ResponseEntity<List<TipoPaciente>>(tipopacientes, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TipoPaciente> listarId(@PathVariable("id") Integer id) {
		TipoPaciente tipoPaciente = new TipoPaciente();
		tipoPaciente = tipoPacienteServiceImpl.buscarId(id);
		if (tipoPaciente == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<TipoPaciente>(tipoPaciente, HttpStatus.OK);
	}
}
