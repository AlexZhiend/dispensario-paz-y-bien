package com.pazybien.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.TipoPersonalMedico;
import com.pazybien.Service.ServiceImpl.TipoPersonalMedicoServiceImpl;

@RestController
@RequestMapping("/tipopersonal")
public class TipoPersonalMedicoController {
	
	@Autowired
	private TipoPersonalMedicoServiceImpl tipoPersonalMedicoServiceImpl;
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarTipoPersonal(@Valid @RequestBody TipoPersonalMedico tipoPersonalMedico) {

		tipoPersonalMedicoServiceImpl.registrar(tipoPersonalMedico);
		URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(tipoPersonalMedico.getIdtipopersonalmedico()).toUri();
				
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<Object> ActualizarTipoPersonal(@Valid @RequestBody TipoPersonalMedico tipoPersonalMedico) {

		tipoPersonalMedicoServiceImpl.actualizar(tipoPersonalMedico);

		return new ResponseEntity<Object>( HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TipoPersonalMedico>>ListarPersonalMedico() {
		List<TipoPersonalMedico> tipoPersonalMedicos=new ArrayList<>();
		tipoPersonalMedicos=tipoPersonalMedicoServiceImpl.listar();

		return new ResponseEntity<List<TipoPersonalMedico>>(tipoPersonalMedicos, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TipoPersonalMedico> listarId(@PathVariable("id") Integer id) {
		TipoPersonalMedico tipoPersonalMedico = new TipoPersonalMedico();
		tipoPersonalMedico = tipoPersonalMedicoServiceImpl.buscarId(id);
		if (tipoPersonalMedico == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<TipoPersonalMedico>(tipoPersonalMedico, HttpStatus.OK);
	}
}
