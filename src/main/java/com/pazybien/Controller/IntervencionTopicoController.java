package com.pazybien.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pazybien.Model.IntervencionTopico;
import com.pazybien.Service.ServiceImpl.IntervencionTopicoServiceImpl;

@Controller
@RequestMapping("/intervenciontopico")
public class IntervencionTopicoController {

	@Autowired
	private IntervencionTopicoServiceImpl intervencionTopicoServiceImpl;
	
	/**
	@PostMapping(value = "/registrarintervencion", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> RegistrarIntervencionTopico(@RequestBody IntervencionTopico intervencionTopico) {
		int respuesta=0;
		
		try {
			intervencionTopicoServiceImpl.registrarIntervencionTopico(intervencionTopico);
			respuesta=1;
		} catch (Exception e) {
			new ResponseEntity<Integer>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return new ResponseEntity<Integer>(respuesta, HttpStatus.OK);
	}
	
	@PutMapping(value = "/actualizarintervencion", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> ActualizarIntervencionTopico(@RequestBody IntervencionTopico intervencionTopico) {
		int respuesta=0;
		
		try {
			intervencionTopicoServiceImpl.actualizarIntervencionTopico(intervencionTopico);
			respuesta=1;
		} catch (Exception e) {
			new ResponseEntity<Integer>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return new ResponseEntity<Integer>(respuesta, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listarintervencion", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IntervencionTopico>> ListarIntervencionTopico() {
		List<IntervencionTopico> intervencionTopicos=new ArrayList<>();
			try {
				intervencionTopicos=intervencionTopicoServiceImpl.listarIntervencionTopico();
			} catch (Exception e) {
				new ResponseEntity<List<IntervencionTopico>>(intervencionTopicos, HttpStatus.INTERNAL_SERVER_ERROR);
				// TODO: handle exception
			}
		return new ResponseEntity<List<IntervencionTopico>>(intervencionTopicos, HttpStatus.OK);
	}
	**/
}
