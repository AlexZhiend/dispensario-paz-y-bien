package com.pazybien.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pazybien.Service.ServiceImpl.DetalleExamenServiceImpl;

@RestController
@RequestMapping("/detalleexamen")
public class DetalleExamenController {
	
	@Autowired
	private DetalleExamenServiceImpl detalleExamenServiceImpl;
	
	@GetMapping(value = "/reporteExamenesg/{id}/{fecha}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarDetalle(@PathVariable("id") int idpaciente,@PathVariable("fecha") String fecha) {
		byte[] data = null;
		data = detalleExamenServiceImpl.generarReporteExamenesg(idpaciente, fecha);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
}
