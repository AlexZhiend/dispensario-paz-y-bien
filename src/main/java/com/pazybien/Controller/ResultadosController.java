package com.pazybien.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Model.Resultados;
import com.pazybien.Service.ServiceImpl.ResultadosServiceImpl;

@RestController
@RequestMapping("/resultados")
public class ResultadosController {

	@Autowired
	private ResultadosServiceImpl resultadosServiceImpl;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Resultados>> ListarResultados() {
		List<Resultados> resultados=new ArrayList<>();
		resultados=resultadosServiceImpl.listar(); 
		return new ResponseEntity<List<Resultados>>(resultados, HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resultados> RegistrarResultados(@Valid @RequestBody Resultados resultados) {
		Resultados res= new Resultados();
		res= resultadosServiceImpl.registrar(resultados);
		
		return new ResponseEntity<Resultados>(res,HttpStatus.OK);	
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Resultados>> BuscarResultadosId(@PathVariable("id") int idpaciente) {
		List<Resultados> res= new ArrayList<>();
		res= resultadosServiceImpl.buscarPorPaciente(idpaciente);
		if (res==null) { 
			throw new ModeloNotFoundException("ID:" + idpaciente);
		}
		return new ResponseEntity<List<Resultados>>(res, HttpStatus.OK);
	}
	
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminarComprobante(@PathVariable Integer id) {
		Resultados res = resultadosServiceImpl.buscarId(id);
		if (res == null) {
			throw new ModeloNotFoundException("ID: " + id);
		} else {
			resultadosServiceImpl.eliminarResultados(id);
		}
	}
	
}
