package com.pazybien.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pazybien.EntityReport.CajaRecaudo;
import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.HistoriaClinica;
import com.pazybien.Model.Paciente;
import com.pazybien.Model.Recaudo;
import com.pazybien.Service.ServiceImpl.RecaudoServiceImpl;

@RestController
@RequestMapping("/recaudo")
public class RecaudoController {

	@Autowired
	private RecaudoServiceImpl recaudoServiceImpl;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Recaudo> RegistrarRecaudo(@Valid @RequestBody Recaudo recaudo){
		
		Recaudo rec= new Recaudo();
		rec= recaudoServiceImpl.registrar(recaudo);

		return new ResponseEntity<Recaudo>(rec, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Recaudo>> ListarRecaudo() {
		List<Recaudo> recaudos= new ArrayList<>();
		recaudos=recaudoServiceImpl.listar();
		return new ResponseEntity<List<Recaudo>>(recaudos,HttpStatus.OK);		
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Recaudo> ActualizarRecaudo(@Valid @RequestBody Recaudo recaudo){
		recaudoServiceImpl.actualizar(recaudo);
		return new ResponseEntity<Recaudo>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Recaudo> BuscarRecaudoId(@PathVariable("id") int id) {
		Recaudo rec= new Recaudo();
		rec= recaudoServiceImpl.buscarId(id);
		if (rec==null) {
			throw new ModeloNotFoundException("ID:" + id);
		}
		return new ResponseEntity<Recaudo>(rec, HttpStatus.OK);
	}	
	
	
	@GetMapping(value= {"/recaudoxdia/{fechacomprobante}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CajaRecaudo> MontoRecaudoXdia(@PathVariable("fechacomprobante") String fechacomprobante) {
		CajaRecaudo rec= new CajaRecaudo();
		rec=recaudoServiceImpl.recaudoXdiaCaja(fechacomprobante);
		return new ResponseEntity<CajaRecaudo>(rec,HttpStatus.OK);		
	}
	
	@GetMapping(value= {"/buscarrecaudo/{fecha}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Recaudo> BuscarRecaudoXfecha(@PathVariable("fecha") String fecha) {
		Recaudo rec= new Recaudo();
		rec=recaudoServiceImpl.recaudoXfecha(fecha);
		return new ResponseEntity<Recaudo>(rec,HttpStatus.OK);		
	}
	
	
	
	
	
	
	@GetMapping(value = "/reporteRecaudoPDF/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteRecaudo(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = recaudoServiceImpl.totalrecaudopdf(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteRecaudoXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteRecaudoXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = recaudoServiceImpl.totalrecaudoXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/FormatoCajaPDF/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarFormatoCaja(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = recaudoServiceImpl.FormatoRecaudo(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	@GetMapping(value = "/ventasingresos/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteVentasCaja(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = recaudoServiceImpl.generarVentasIngresosReport(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/ventasingresosxlsx/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteVentasCajaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data =recaudoServiceImpl.generarVentasIngresosReportXlsx(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
}
