package com.pazybien.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pazybien.EntityReport.AtencionLabReport;
import com.pazybien.Exception.ModeloNotFoundException;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;
import com.pazybien.Service.ServiceImpl.ComprobantePagoServiceImpl;

@RestController
@RequestMapping("/comprobantepago")
public class ComprobantePagoController {

	@Autowired
	private ComprobantePagoServiceImpl comprobantePagoServiceImpl;
	
	
	private Log log = LogFactory.getLog(ComprobantePagoController.class);
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ComprobantePago>> ListarComprobante() {
		List<ComprobantePago> comprobantes=new ArrayList<>();
		comprobantes=comprobantePagoServiceImpl.listar(); 
		return new ResponseEntity<List<ComprobantePago>>(comprobantes, HttpStatus.OK);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComprobantePago> RegistrarComprobante(@Valid @RequestBody ComprobantePago comprobantePago) {
		ComprobantePago comprobante= new ComprobantePago();
		comprobante= comprobantePagoServiceImpl.registrar(comprobantePago);
		
		 
		log.info(comprobantePago.toString());
		return new ResponseEntity<ComprobantePago>(comprobante,HttpStatus.OK);	
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComprobantePago> BuscarComprobanteId(@PathVariable("id") int id) {
		ComprobantePago comprobantePago= new ComprobantePago();
		comprobantePago= comprobantePagoServiceImpl.buscarPorNumero(id);
		if (comprobantePago==null) {
			throw new ModeloNotFoundException("No se encuentra el número: " + id);
		}
		return new ResponseEntity<ComprobantePago>(comprobantePago, HttpStatus.OK);
	}	
	
	
	@GetMapping(value="/ultimo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ComprobantePago> buscarPaciente(){
		ComprobantePago comprobante = new ComprobantePago();
		comprobante = comprobantePagoServiceImpl.buscarultimo();
		return new ResponseEntity<ComprobantePago>(comprobante, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteComporbante/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteComprobante(@PathVariable("id") Integer numerorecibocomprobante) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarReporteComprobanteU(numerorecibocomprobante);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteCaja/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCaja(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarCajaG(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencion/{fechaatencion}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarAtenciones(@PathVariable("fechaatencion") String fechaatencion) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencion(fechaatencion);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminarComprobante(@PathVariable Integer id) {
		ComprobantePago cp = comprobantePagoServiceImpl.buscarCp(id);
		if (cp == null) {
			throw new ModeloNotFoundException("ID: " + id);
		} else {
			comprobantePagoServiceImpl.eliminarComprobante(id);
		}
	}
	
	@GetMapping(value = "/vigentes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ComprobantePago>> listarVigentes() {
		List<ComprobantePago> comprobantes=new ArrayList<>();
		comprobantes=comprobantePagoServiceImpl.listarvigentes(); 
		return new ResponseEntity<List<ComprobantePago>>(comprobantes, HttpStatus.OK);
	}
	
	@GetMapping(value = "/fechaatlab/{fechaatlab}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AtencionLabReport>> AtencionesLab(@PathVariable("fechaatlab") String fechaatlab ) {
		List<AtencionLabReport> at=new ArrayList<AtencionLabReport>();
		at=comprobantePagoServiceImpl.listaras(fechaatlab); 
		return new ResponseEntity<List<AtencionLabReport>>(at, HttpStatus.OK);
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<Object> actualizar(@Valid @RequestBody ComprobantePago comprobantePago) {
		comprobantePagoServiceImpl.actualizar(comprobantePago);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/reportedetallado/{fechacomprobante}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCajaDetallado(@PathVariable("fechacomprobante") String fechacomprobante) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarCajaDetallado(fechacomprobante);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	
	
	@GetMapping(value = "/reporteCajaXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCajaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarCajaGXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionXLSX/{fechaatencion}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarAtencionesXLSX(@PathVariable("fechaatencion") String fechaatencion) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionXLSX(fechaatencion);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reportedetalladoXLSX/{fechacomprobante}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteCajaDetalladoXLSX(@PathVariable("fechacomprobante") String fechacomprobante) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarCajaDetalladoXLSX(fechacomprobante);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	@GetMapping(value = "/reporteAtencionG/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionG(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionG(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionGXLSX/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionGXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionGXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	@GetMapping(value = "/reporteAtencionGMed/{fechainicio}/{fechafin}/{descripcionserviciomedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionGMed(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("descripcionserviciomedico") String descripcionserviciomedico) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionGMedicos(fechainicio, fechafin, descripcionserviciomedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionGXLSXMed/{fechainicio}/{fechafin}/{descripcionserviciomedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionGXLSXMed(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("descripcionserviciomedico") String descripcionserviciomedico) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionGXLSXMedicos(fechainicio, fechafin, descripcionserviciomedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	
	
	
	@GetMapping(value = "/reporteAtencionGServ/{fechainicio}/{fechafin}/{denominacionserviciomedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionGServ(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("denominacionserviciomedico") String denominacionserviciomedico) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionGServ(fechainicio, fechafin, denominacionserviciomedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteAtencionGXLSXServ/{fechainicio}/{fechafin}/{denominacionserviciomedico}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteAtencionGXLSXServ(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin,@PathVariable("denominacionserviciomedico") String denominacionserviciomedico) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarAtencionGXLSXServ(fechainicio, fechafin, denominacionserviciomedico);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	
	
	
	
	
	@GetMapping(value = "/reporteNCCaja/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteVentasCaja(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarNCCajaciaReport(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@GetMapping(value = "/reporteNCCajaxlsx/{fechainicio}/{fechafin}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporteVentasCajaXLSX(@PathVariable("fechainicio") String fechainicio,@PathVariable("fechafin") String fechafin) {
		byte[] data = null;
		data = comprobantePagoServiceImpl.generarNCCajaReportXLSX(fechainicio, fechafin);
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
}
