package com.pazybien.Util;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import okhttp3.*;

public class Httpsocket {
	
	@Autowired
	private RestTemplate restTemp;
	
	OkHttpClient client = new OkHttpClient.Builder().connectTimeout(180, TimeUnit.SECONDS)
  .readTimeout(180, TimeUnit.SECONDS).build();

	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	
	public void post(String jsondata){
		
		try {
			RequestBody body = RequestBody.create(JSON, jsondata);
			Request request = new Request.Builder()
	                .url("http://localhost:3000/notificacion")
//	                .header("Authorization","Bearer " + api_key)
	                .post(body)
	                .build();
			Response response=client.newCall(request).execute();
			System.out.println("LLamo api");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("error");
		}
		
	} 
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
	
	public void socket(String hcl){
		String uri = "http://127.0.0.1:3000/notificacion/ooops";
		ResponseEntity<String> responseentity = restTemp.exchange(uri, HttpMethod.GET, null,String.class );
	}
	
}
