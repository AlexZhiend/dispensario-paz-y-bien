package com.pazybien.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.AtencionesRxEcoReport;
import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.Model.PersonalMedico;

@Repository
public interface IPersonalMedicoDAO extends JpaRepository<PersonalMedico, String>{
	
	public PersonalMedico findPersonalMedicoByDnipersonalmedico(String dnipersonalmedico);
	
	@Query(value = "select * from personalmedico per where per.idespecialidad<>13",nativeQuery = true)
	List<PersonalMedico> listarsolomedicos();
	
	@Query(value = "select * from personalmedico pers \n" + 
			"where pers.idtipopersonalmedico = 4 or pers.idtipopersonalmedico = 6",nativeQuery = true)
	List<PersonalMedico> listarRadioEco();
	
	@Query(nativeQuery = true, name = "ReporteAtencionesEcoRad1")
	List<AtencionesRxEcoReport[]> medicoEcoRad1(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
	
	@Query(nativeQuery = true, name = "ReporteAtencionesEcoRad2")
	List<AtencionesRxEcoReport[]> medicoEcoRad2(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin,@Param("dnipersonalmedico") String dnipersonalmedico);
}
