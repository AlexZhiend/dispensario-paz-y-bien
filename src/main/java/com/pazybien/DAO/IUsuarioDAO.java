package com.pazybien.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pazybien.Model.Usuario;


public interface IUsuarioDAO extends JpaRepository<Usuario, Long> {
	
	Usuario findOneByUsername(String username);
}