package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.DetalladoVentaF;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.OrdenFReport;
import com.pazybien.EntityReport.TotalxDiaFarmacia;
import com.pazybien.Model.OrdenFarmacia;
import com.pazybien.Model.Producto;

@Repository
public interface IOrdenFarmaciaDAO extends JpaRepository<OrdenFarmacia, Serializable>{

	@Query(value = "select * from ordenfarmacia od where od.estado=1 and od.numeroorden = :numeroorden", nativeQuery = true)
	public OrdenFarmacia buscarvigentesXnumero(@Param("numeroorden") int numeroorden);
	
	@Query(value = "select * from ordenfarmacia order by idordenfarmacia desc limit 1", nativeQuery = true)
	OrdenFarmacia buscarnumero();
	
	@Query(value = "select * from ordenfarmacia of where of.estado = 1 order by idordenfarmacia ", nativeQuery = true)
	List<OrdenFarmacia> listarvigentes();
	
	@Query(nativeQuery = true, name = "ReporteOrdenFPaciente")
	List<OrdenFReport[]> reporteOrdenFPaciente(@Param("numeroorden") int numeroorden);
	
	@Query(nativeQuery = true, name = "TotalFarmaciaxDia")
	TotalxDiaFarmacia totalxdia(@Param("fecha") String fecha);
	
	@Query(nativeQuery = true, name = "ReporteOrdenFDetallado")
	List<DetalladoVentaF[]> detalladoFarmacia(@Param("fechaordenfarmacia") String fechaordenfarmacia);
	
	@Query(nativeQuery = true, name = "NotaCreditoDetallado")
	List<NCreditoFarmaciaReport[]>  NCFarmaciaReport(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
}
