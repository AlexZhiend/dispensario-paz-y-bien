package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.Paciente;
import com.pazybien.Model.Resultados;

@Repository
public interface IResultadosDAO extends JpaRepository<Resultados, Serializable>{

	public List<Resultados> findResultadosByPaciente(Paciente paciente);

}
