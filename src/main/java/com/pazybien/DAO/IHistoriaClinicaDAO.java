package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.HistoriaReport;
import com.pazybien.EntityReport.HvaciaReport;
import com.pazybien.EntityReport.Medicoreconsulta;
import com.pazybien.EntityReport.Medicoxdia;
import com.pazybien.Model.HistoriaClinica;
import com.pazybien.Model.Paciente;

@Repository
public interface IHistoriaClinicaDAO extends JpaRepository<HistoriaClinica, Serializable>{

	public HistoriaClinica findHistoriaClinicaByPaciente(Paciente paciente);
	
	@Query(value = "select * from historiaclinica hc inner join paciente pac on pac.idpaciente = hc.idpaciente where pac.hcl = :hcl ", nativeQuery = true)
	List<HistoriaClinica> historiaPorHcl(@Param("hcl") int hcl);

	@Query(nativeQuery = true, name = "ReporteHistoriaPaciente")
	List<HistoriaReport[]> reporteHistoriaPaciente(@Param("idhistoriaclinica") int idhistoriaclinica);
	
	@Query(nativeQuery = true, name = "ReporteHistoriaVacia")
	List<HvaciaReport[]> reporteHistoriaVacia(@Param("idhistoriaclinica") int idhist);
	
	@Query(nativeQuery = true, name = "ReporteMedicoreconsulta")
	List<Medicoreconsulta[]> medicoReconsulta(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
	
	@Query(nativeQuery = true, name = "ReporteMedicoreconsulta2")
	List<Medicoreconsulta[]> medicoReconsulta2(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin,@Param("reconsulta") String reconsulta);
	
	@Query(nativeQuery = true, name = "ReporteMedicoxdia")
	List<Medicoxdia> reporteMedicoxdia(@Param("fechaexpediciconhistoriaclinica") String fechaexpediciconhistoriaclinica);
	
	
	@Query(value = "select * from historiaclinica hc\r\n" + 
			"where hc.fechaexpediciconhistoriaclinica between TO_DATE(:fechainicio , 'DD/MM/YYYY') and TO_DATE(:fechafin , 'DD/MM/YYYY')", nativeQuery = true)
	List<HistoriaClinica> listarHistoriaXfecha(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
	
	@Query(value = "select * from historiaclinica hc inner join paciente pac on pac.idpaciente = hc.idpaciente\r\n" + 
			"where UPPER(pac.nombresyapellidos) like upper(concat('%',:nombresyapellidos,'%'));", nativeQuery = true)
	List<HistoriaClinica> listarHistoriaXPaciente(@Param("nombresyapellidos") String nombresyapellidos);
	
}
