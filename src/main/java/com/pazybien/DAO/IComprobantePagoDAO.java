package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.AtencionesReport;
import com.pazybien.EntityReport.CajaDetalladoReport;
import com.pazybien.EntityReport.AtencionLabReport;
import com.pazybien.EntityReport.AtencionesGeneral;
import com.pazybien.EntityReport.CajaReport;
import com.pazybien.EntityReport.ComprobanteReport;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.ProductoVentasReport;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.OrdenFarmacia;
import com.pazybien.Model.Paciente;

@Repository
public interface IComprobantePagoDAO extends JpaRepository<ComprobantePago, Serializable>{

	@Query(value = "select * from comprobantepago cp where cp.estado=1 and cp.numerorecibocomprobante = :numerorecibocomprobante", nativeQuery = true)
	public ComprobantePago BuscarVigentesPorNumero(@Param("numerorecibocomprobante") int numerorecibocomprobante);
	
	@Query(value = "select * from comprobantepago cp where cp.estado=1", nativeQuery = true)
	public List<ComprobantePago> listarvigentes();
	
	@Query(value = "select * from comprobantepago order by numerorecibocomprobante desc limit 1", nativeQuery = true)
	public ComprobantePago buscarnumero();
	
	@Query(nativeQuery = true, name = "ReporteComporbanteUnit")
	List<ComprobanteReport[]> reporteComprobanteU(@Param("numerorecibocomprobante") int numerorecibocomprobante);
	
	@Query(nativeQuery = true, name = "ReporteCajaGeneral")
	List<CajaReport[]> reporteCajaG(@Param("fechainicio") String fechainicio, @Param("fechafin") String fechafin);
	
	@Query(nativeQuery = true, name = "ReporteAtenciones")
	List<AtencionesReport[]> reporteAtencion(@Param("fechaatencion") String fechainicio);
	
	@Query(nativeQuery = true, name = "Reportelab")
	List<AtencionLabReport> ReporteAtencionesdeLab(@Param("fechaatlab") String fechaatlab);
	
	@Query(nativeQuery = true, name = "ReporteDetallado")
	List<CajaDetalladoReport> ReporteDetalladoCaja(@Param("fechacomprobante") String fechacomprobante);
	
	@Query(nativeQuery = true, name = "ReporteGeneralAtenciones")
	List<AtencionesGeneral[]> reporteAtencionGeneral(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);

	@Query(nativeQuery = true, name = "ReporteGeneralAMedico")
	List<AtencionesGeneral[]> reporteAtencionMedicos(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin,@Param("descripcionserviciomedico") String descripcionserviciomedico);

	@Query(nativeQuery = true, name = "ReporteGeneralAServicio")
	List<AtencionesGeneral[]> reporteAtencionServicio(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin,@Param("denominacionserviciomedico") String denominacionserviciomedico);

	@Query(nativeQuery = true, name = "NotacreditoCaja")
	List<NCreditoFarmaciaReport[]>  NCFarmaciaReport(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
}
