package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.TipoIntervencion;

@Repository
public interface ITipoIntervencionDAO extends JpaRepository<TipoIntervencion, Serializable>{

}
