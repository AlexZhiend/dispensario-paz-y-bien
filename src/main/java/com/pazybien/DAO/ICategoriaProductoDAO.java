package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.CategoriaProducto;

@Repository
public interface ICategoriaProductoDAO extends JpaRepository<CategoriaProducto, Serializable>{
	public CategoriaProducto findCategoriaProductoByIdcategoriaproducto(int idcategoriaproducto);

}
