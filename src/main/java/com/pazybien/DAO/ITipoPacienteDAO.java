package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.TipoPaciente;

@Repository
public interface ITipoPacienteDAO extends JpaRepository<TipoPaciente, Serializable>{
public TipoPaciente findTipoPacienteByIdtipopaciente(int idtipopaciente);
}
