package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.AtencionesReport;
import com.pazybien.EntityReport.TotalExamenesReport;
import com.pazybien.Model.ExamenesG;

@Repository
public interface IExamenesGDAO extends JpaRepository<ExamenesG, Serializable>{

	
	@Query(nativeQuery = true, name = "ReporteTotalExamenes")
	List<TotalExamenesReport[]> reporteTotalExamenes(@Param("fechainicio") String fechainicio, @Param("fechafin") String fechafin);
}
