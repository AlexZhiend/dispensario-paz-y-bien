package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.CajaRecaudo;
import com.pazybien.EntityReport.ComprobanteReport;
import com.pazybien.EntityReport.NCreditoFarmaciaReport;
import com.pazybien.EntityReport.RecaudoReport;
import com.pazybien.Model.Paciente;
import com.pazybien.Model.Recaudo;

@Repository
public interface IRecaudoDAO extends JpaRepository<Recaudo, Serializable> {
	
	@Query(nativeQuery = true, name = "TotalCajaXDia")
	CajaRecaudo RecaudoCaja(@Param("fechacomprobante") String fechacomprobante);
	
	@Query(value = "select * from recaudo rec where rec.fecha = TO_DATE(:fecha , 'DD/MM/YYYY') ", nativeQuery = true)
	public Recaudo RecaudoXfecha(@Param("fecha") String fecha);
	
	@Query(nativeQuery = true, name = "TotalRecaudo")
	List<RecaudoReport[]> TotalRecaudo(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
	
	@Query(nativeQuery = true, name = "VentasIngresosReport")
	List<NCreditoFarmaciaReport[]> TotalVentasIngresos(@Param("fechainicio") String fechainicio,@Param("fechafin") String fechafin);
}
