package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.ServicioMedico;


@Repository
public interface IServicioMedicoDAO extends JpaRepository<ServicioMedico, Serializable>{
public ServicioMedico findServicioMedicoByIdserviciomedico(int idserviciomedico);	
}
