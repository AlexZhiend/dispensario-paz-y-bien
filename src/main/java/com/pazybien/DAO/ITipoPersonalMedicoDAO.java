package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.TipoPersonalMedico;

@Repository
public interface ITipoPersonalMedicoDAO extends JpaRepository<TipoPersonalMedico, Serializable>{
public TipoPersonalMedico findTipoPersonalMedicoByIdtipopersonalmedico(int idtipopersonalmedico);
}
