package com.pazybien.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.Paciente;

@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, String>{

	public Paciente findPacienteByIdpaciente(int id);
	
	@Query(value = "select * from paciente pac where (pac.hcl = :hcl and pac.hcl <> 0) or (pac.nombresyapellidos = upper(:nombresyapellidos))", nativeQuery = true)
	public Paciente buscarHcl(@Param("hcl") int hcl,@Param("nombresyapellidos") String nombresyapellidos);
	
	@Query(value = "select * from paciente pac where pac.idtipopaciente = 1 order by pac.hcl desc limit 1 ", nativeQuery = true)
	public Paciente ultimoInterno();
	
	@Query(value = "select * from paciente pac where pac.idtipopaciente = 2 order by pac.hcl desc limit 1 ", nativeQuery = true)
	public Paciente ultimoExterno();
	
	@Query(value = "select * from paciente pac where UPPER(pac.nombresyapellidos) like upper(concat('%',:nombresyapellidos,'%'));", nativeQuery = true)
	public List<Paciente> buscarPacientePorNombre(@Param("nombresyapellidos") String nombresyapellidos);
}


