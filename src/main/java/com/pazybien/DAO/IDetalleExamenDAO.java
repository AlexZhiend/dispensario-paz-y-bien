package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.ExamenesGReport;
import com.pazybien.Model.DetalleExamen;

@Repository
public interface IDetalleExamenDAO extends JpaRepository<DetalleExamen, Serializable>{

	@Query(nativeQuery = true, name = "ReporteExamenesgPaciente")
	List<ExamenesGReport[]> reporteExamenesgPaciente(@Param("idpaciente") int idpaciente, @Param("fecha") String fecha);
	
}
