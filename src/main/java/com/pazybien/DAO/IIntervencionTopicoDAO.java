package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.IntervencionTopico;

@Repository
public interface IIntervencionTopicoDAO extends JpaRepository<IntervencionTopico, Serializable>{

}
