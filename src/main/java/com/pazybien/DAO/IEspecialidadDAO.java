package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.Especialidad;

@Repository
public interface IEspecialidadDAO extends JpaRepository<Especialidad, Serializable>{
public Especialidad findEspecialidadByIdespecialidad(int idespecialidad);
}
