package com.pazybien.DAO;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.DetalleOrdenF;
import com.pazybien.Model.OrdenFarmacia;

@Repository
public interface IDetalleOrdenFDAO extends JpaRepository<DetalleOrdenF, Integer>{

	/*
	@Modifying
	@Query(value = "INSERT INTO detalle_ordenf(idordenfarmacia,idproducto) VALUES(:idordenfarmacia, :idproducto)", nativeQuery = true)
	int registrar(@Param("idordenfarmacia") Integer idordenfarmacia, @Param("idproducto") Integer idproducto);
	*/
}
