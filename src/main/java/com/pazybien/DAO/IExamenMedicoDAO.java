package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.EntityReport.LabGenReport;
import com.pazybien.Model.CategoriaExamenMedico;
import com.pazybien.Model.ComprobantePago;
import com.pazybien.Model.ExamenMedico;
import com.pazybien.Model.Paciente;

@Repository
public interface IExamenMedicoDAO extends JpaRepository<ExamenMedico, Serializable>{

	public List<ExamenMedico> findExamenMedicoByCategoriaexamenmedico(CategoriaExamenMedico categoriaexamenmedico);
		
	@Query(value = "select * from examenmedico order by extra desc", nativeQuery = true)
	public List<ExamenMedico> listarordenado();
	
	@Query(nativeQuery = true, name = "ReporteExamenMedicoGeneral")
	List<LabGenReport[]> reporteDeLaboratorioGeneral(@Param("fechainicio") String fechainicio, @Param("fechafin") String fechafin);
}
