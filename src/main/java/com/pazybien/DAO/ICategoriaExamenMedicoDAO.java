package com.pazybien.DAO;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pazybien.Model.CategoriaExamenMedico;

@Repository
public interface ICategoriaExamenMedicoDAO extends JpaRepository<CategoriaExamenMedico, Serializable>{

	public CategoriaExamenMedico findCategoriaExamenMedicoByIdcategoriaexamenmedico(int idcategoriaexamenmedico);
	
	@Query(value = "select cat.* from comprobantepago cp \r\n" + 
			"inner join detallecomprobante det on det.idcomprobantepago = cp.idcomprobantepago\r\n" + 
			"inner join categoriaexamenmedico cat on cat.idcategoriaexamenmedico  = det.idcategoriaexamenmedico\r\n" + 
			"where cp.idcomprobantepago = :idcomprobantepago", nativeQuery = true)
	public List<CategoriaExamenMedico> buscarCatePorCompro(@Param("idcomprobantepago") int idcomprobantepago);
}
