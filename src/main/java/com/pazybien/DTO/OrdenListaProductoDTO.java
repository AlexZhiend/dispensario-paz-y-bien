package com.pazybien.DTO;

import java.util.List;

import com.pazybien.Model.OrdenFarmacia;
import com.pazybien.Model.Producto;

public class OrdenListaProductoDTO {

	private OrdenFarmacia ordenfarmacia;
	
	private List<Producto> listaproducto;

	public OrdenFarmacia getOrdenfarmacia() {
		return ordenfarmacia;
	}

	public void setOrdenfarmacia(OrdenFarmacia ordenfarmacia) {
		this.ordenfarmacia = ordenfarmacia;
	}

	public List<Producto> getListaproducto() {
		return listaproducto;
	}

	public void setListaproducto(List<Producto> listaproducto) {
		this.listaproducto = listaproducto;
	}

	@Override
	public String toString() {
		return "OrdenListaProductoDTO [ordenfarmacia=" + ordenfarmacia + ", listaproducto=" + listaproducto + "]";
	}

}
