package com.pazybien.EntityReport;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AtencionesReport {
	
	@Id
	private double ids;
	
	private String denominacionserviciomedico;
	
	private String descripcionserviciomedico;
	
	private int cantidad;
	
	private String turno;

	public String getDenominacionserviciomedico() {
		return denominacionserviciomedico;
	}

	public void setDenominacionserviciomedico(String denominacionserviciomedico) {
		this.denominacionserviciomedico = denominacionserviciomedico;
	}

	public String getDescripcionserviciomedico() {
		return descripcionserviciomedico;
	}

	public void setDescripcionserviciomedico(String descripcionserviciomedico) {
		this.descripcionserviciomedico = descripcionserviciomedico;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public double getIds() {
		return ids;
	}

	public void setIds(double ids) {
		this.ids = ids;
	}

	
	
}
