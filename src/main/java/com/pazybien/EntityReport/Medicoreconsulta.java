package com.pazybien.EntityReport;


import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class Medicoreconsulta {
	
	@Id
	private int ids;
	
	private String reconsulta;
	
	private BigInteger total;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechareconsulta;

	public String getReconsulta() {
		return reconsulta;
	}

	public void setReconsulta(String reconsulta) {
		this.reconsulta = reconsulta;
	}

	public BigInteger getTotal() {
		return total;
	}

	public void setTotal(BigInteger total) {
		this.total = total;
	}

	public LocalDate getFechareconsulta() {
		return fechareconsulta;
	}

	public void setFechareconsulta(LocalDate fechareconsulta) {
		this.fechareconsulta = fechareconsulta;
	}

	public int getIds() {
		return ids;
	}

	public void setIds(int ids) {
		this.ids = ids;
	}

}
