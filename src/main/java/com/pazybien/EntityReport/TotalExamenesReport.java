package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class TotalExamenesReport {

	@Id
	private double ids;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fecha;
	
	private String tipo;
	
	private int cantidades;

	public double getIds() {
		return ids;
	}

	public void setIds(double ids) {
		this.ids = ids;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getCantidades() {
		return cantidades;
	}

	public void setCantidades(int cantidades) {
		this.cantidades = cantidades;
	}
	
	
	
}
