package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;


@Entity
public class CajaRecaudo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ids;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechacomprobante;
	
	private double totalxdia;

	public int getIds() {
		return ids;
	}

	public void setIds(int ids) {
		this.ids = ids;
	}

	public LocalDate getFechacomprobante() {
		return fechacomprobante;
	}

	public void setFechacomprobante(LocalDate fechacomprobante) {
		this.fechacomprobante = fechacomprobante;
	}

	public double getTotalxdia() {
		return totalxdia;
	}

	public void setTotalxdia(double totalxdia) {
		this.totalxdia = totalxdia;
	}
	
	
}
