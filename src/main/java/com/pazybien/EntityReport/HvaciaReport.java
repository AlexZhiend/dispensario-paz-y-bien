package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class HvaciaReport {

	@Id
	private int idhistoriaclinica;
	
	@Column
	private String hcl;
	
	@Column
	private String anamnesis;
	
	@Column
	private String evaluacion;
	
	@Column
	private String diagnostico;
	
	@Column
	private String tratamiento;
	
	@Column
	private String examenes;
	
	@Column
	private String reconsulta;
	
	@Column
	private String diagnosticor;
	
	@Column
	private String tratamientor;

	public int getIdhistoriaclinica() {
		return idhistoriaclinica;
	}

	public void setIdhistoriaclinica(int idhistoriaclinica) {
		this.idhistoriaclinica = idhistoriaclinica;
	}

	public String getHcl() {
		return hcl;
	}

	public void setHcl(String hcl) {
		this.hcl = hcl;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getExamenes() {
		return examenes;
	}

	public void setExamenes(String examenes) {
		this.examenes = examenes;
	}

	public String getReconsulta() {
		return reconsulta;
	}

	public void setReconsulta(String reconsulta) {
		this.reconsulta = reconsulta;
	}

	public String getDiagnosticor() {
		return diagnosticor;
	}

	public void setDiagnosticor(String diagnosticor) {
		this.diagnosticor = diagnosticor;
	}

	public String getTratamientor() {
		return tratamientor;
	}

	public void setTratamientor(String tratamientor) {
		this.tratamientor = tratamientor;
	}
	
	
}
