package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class TotalxDiaFarmacia {

	@Id
	private double totalxdia;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenfarmacia;
	

	public double getTotalxdia() {
		return totalxdia;
	}

	public void setTotalxdia(double totalxdia) {
		this.totalxdia = totalxdia;
	}

	public LocalDate getFechaordenfarmacia() {
		return fechaordenfarmacia;
	}

	public void setFechaordenfarmacia(LocalDate fechaordenfarmacia) {
		this.fechaordenfarmacia = fechaordenfarmacia;
	}
	
}
