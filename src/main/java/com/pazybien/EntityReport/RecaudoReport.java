package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class RecaudoReport {
	
	@Id
	private int idrecaudo;
	
	private String descripcion;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fecha;
	
	private String monto;
	
	private String montoanulado;
	
	private String montofinal;

	public int getIdrecaudo() {
		return idrecaudo;
	}

	public void setIdrecaudo(int idrecaudo) {
		this.idrecaudo = idrecaudo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getMontoanulado() {
		return montoanulado;
	}

	public void setMontoanulado(String montoanulado) {
		this.montoanulado = montoanulado;
	}

	public String getMontofinal() {
		return montofinal;
	}

	public void setMontofinal(String montofinal) {
		this.montofinal = montofinal;
	}
	
	
}
