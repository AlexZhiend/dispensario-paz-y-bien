package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class ExamenesGReport {

	@Id
	private int iddetalleexamen;
	
	private String resultado;
	
	private String observaciones;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fecha;
	
	private String nombresyapellidos;
	
	private int extra;
	
	private String denominacionexamenmedico;
	
	private String unidadmedida;
	
	private String valorreferencia;
	
	private String nombrecategoriaexamenmedico;

	public int getIddetalleexamen() {
		return iddetalleexamen;
	}

	public void setIddetalleexamen(int iddetalleexamen) {
		this.iddetalleexamen = iddetalleexamen;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public String getDenominacionexamenmedico() {
		return denominacionexamenmedico;
	}

	public void setDenominacionexamenmedico(String denominacionexamenmedico) {
		this.denominacionexamenmedico = denominacionexamenmedico;
	}

	public String getUnidadmedida() {
		return unidadmedida;
	}

	public void setUnidadmedida(String unidadmedida) {
		this.unidadmedida = unidadmedida;
	}

	public String getValorreferencia() {
		return valorreferencia;
	}

	public void setValorreferencia(String valorreferencia) {
		this.valorreferencia = valorreferencia;
	}

	public String getNombrecategoriaexamenmedico() {
		return nombrecategoriaexamenmedico;
	}

	public void setNombrecategoriaexamenmedico(String nombrecategoriaexamenmedico) {
		this.nombrecategoriaexamenmedico = nombrecategoriaexamenmedico;
	}

	public int getExtra() {
		return extra;
	}

	public void setExtra(int extra) {
		this.extra = extra;
	}
	
}
