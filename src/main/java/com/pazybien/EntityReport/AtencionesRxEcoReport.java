package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class AtencionesRxEcoReport {

	@Id
	private int ids;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaatencion;
	
	private int atenciones;
	
	private String nombrespersonalmedico;

	public int getIds() {
		return ids;
	}

	public void setIds(int ids) {
		this.ids = ids;
	}

	public LocalDate getFechaatencion() {
		return fechaatencion;
	}

	public void setFechaatencion(LocalDate fechaatencion) {
		this.fechaatencion = fechaatencion;
	}

	public int getAtenciones() {
		return atenciones;
	}

	public void setAtenciones(int atenciones) {
		this.atenciones = atenciones;
	}

	public String getNombrespersonalmedico() {
		return nombrespersonalmedico;
	}

	public void setNombrespersonalmedico(String nombrespersonalmedico) {
		this.nombrespersonalmedico = nombrespersonalmedico;
	}
	
	
}
