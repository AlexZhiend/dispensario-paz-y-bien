package com.pazybien.EntityReport;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ProductoVentasReport {
	
	@Id
	private int idproducto;
	
	private String nombreproducto;
	
	private String nombrepresentacionproducto;
	
	private String cantidadpresentacionproducto;
	
	private String unidadpresentacionproducto;
	
	private double pventaproducto;
	
	private int cantidadtotal;
	
	private double total;

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public String getNombrepresentacionproducto() {
		return nombrepresentacionproducto;
	}

	public void setNombrepresentacionproducto(String nombrepresentacionproducto) {
		this.nombrepresentacionproducto = nombrepresentacionproducto;
	}

	public String getCantidadpresentacionproducto() {
		return cantidadpresentacionproducto;
	}

	public void setCantidadpresentacionproducto(String cantidadpresentacionproducto) {
		this.cantidadpresentacionproducto = cantidadpresentacionproducto;
	}

	public String getUnidadpresentacionproducto() {
		return unidadpresentacionproducto;
	}

	public void setUnidadpresentacionproducto(String unidadpresentacionproducto) {
		this.unidadpresentacionproducto = unidadpresentacionproducto;
	}

	public double getPventaproducto() {
		return pventaproducto;
	}

	public void setPventaproducto(double pventaproducto) {
		this.pventaproducto = pventaproducto;
	}

	public int getCantidadtotal() {
		return cantidadtotal;
	}

	public void setCantidadtotal(int cantidadtotal) {
		this.cantidadtotal = cantidadtotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}


}
