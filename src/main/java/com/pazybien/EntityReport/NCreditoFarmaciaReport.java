package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class NCreditoFarmaciaReport {

	@Id
	private int idordenfarmacia;
	
	private String numeroorden;
	
	private String tipo;
	
	private String serie;
	
	private String consumidorordenfarmacia;
	
	private double exonera;
	
	private double inafecto;
	
	private double total;
	
	private String serieref;
	
	private String tiporef;
	
	private String numeroref;
	
	private double totalref;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenfarmacia;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenref;
	
	public String getNumeroorden() {
		return numeroorden;
	}


	public void setNumeroorden(String numeroorden) {
		this.numeroorden = numeroorden;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public String getConsumidorordenfarmacia() {
		return consumidorordenfarmacia;
	}


	public void setConsumidorordenfarmacia(String consumidorordenfarmacia) {
		this.consumidorordenfarmacia = consumidorordenfarmacia;
	}


	public double getExonera() {
		return exonera;
	}


	public void setExonera(double exonera) {
		this.exonera = exonera;
	}


	public double getInafecto() {
		return inafecto;
	}


	public void setInafecto(double inafecto) {
		this.inafecto = inafecto;
	}


	public double getTotal() {
		return total;
	}


	public void setTotal(double total) {
		this.total = total;
	}


	public LocalDate getFechaordenfarmacia() {
		return fechaordenfarmacia;
	}


	public void setFechaordenfarmacia(LocalDate fechaordenfarmacia) {
		this.fechaordenfarmacia = fechaordenfarmacia;
	}


	public String getSerieref() {
		return serieref;
	}


	public void setSerieref(String serieref) {
		this.serieref = serieref;
	}


	public String getTiporef() {
		return tiporef;
	}


	public void setTiporef(String tiporef) {
		this.tiporef = tiporef;
	}


	public String getNumeroref() {
		return numeroref;
	}


	public void setNumeroref(String numeroref) {
		this.numeroref = numeroref;
	}


	public double getTotalref() {
		return totalref;
	}


	public void setTotalref(double totalref) {
		this.totalref = totalref;
	}


	public LocalDate getFechaordenref() {
		return fechaordenref;
	}


	public void setFechaordenref(LocalDate fechaordenref) {
		this.fechaordenref = fechaordenref;
	}


	public int getIdordenfarmacia() {
		return idordenfarmacia;
	}


	public void setIdordenfarmacia(int idordenfarmacia) {
		this.idordenfarmacia = idordenfarmacia;
	}
	
	
}
