package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class OrdenFReport {

	@Id
	private int id;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaordenfarmacia;
	
	private int numeroorden; 
	
	private String rucordenfarmacia;
	
	private String consumidorordenfarmacia; 
	
	private double total;
	
	private String nombreproducto;
	
	private double pventaproducto;
	
	private int cantidad;
	
	private int descuento;
	
	private double importe;


	public LocalDate getFechaordenfarmacia() {
		return fechaordenfarmacia;
	}

	public void setFechaordenfarmacia(LocalDate fechaordenfarmacia) {
		this.fechaordenfarmacia = fechaordenfarmacia;
	}

	public int getNumeroorden() {
		return numeroorden;
	}

	public void setNumeroorden(int numeroorden) {
		this.numeroorden = numeroorden;
	}

	public String getRucordenfarmacia() {
		return rucordenfarmacia;
	}

	public void setRucordenfarmacia(String rucordenfarmacia) {
		this.rucordenfarmacia = rucordenfarmacia;
	}

	public String getConsumidorordenfarmacia() {
		return consumidorordenfarmacia;
	}

	public void setConsumidorordenfarmacia(String consumidorordenfarmacia) {
		this.consumidorordenfarmacia = consumidorordenfarmacia;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public double getPventaproducto() {
		return pventaproducto;
	}

	public void setPventaproducto(double pventaproducto) {
		this.pventaproducto = pventaproducto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	
	
}
