package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class AtencionesGeneral {

	@Id
	private double ids;
	
	private String denominacionserviciomedico;
	
	private String descripcionserviciomedico;
	
	private int cantidad;
	
	private String turno;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaatencion;

	public double getIds() {
		return ids;
	}

	public void setIds(double ids) {
		this.ids = ids;
	}

	public String getDenominacionserviciomedico() {
		return denominacionserviciomedico;
	}

	public void setDenominacionserviciomedico(String denominacionserviciomedico) {
		this.denominacionserviciomedico = denominacionserviciomedico;
	}

	public String getDescripcionserviciomedico() {
		return descripcionserviciomedico;
	}

	public void setDescripcionserviciomedico(String descripcionserviciomedico) {
		this.descripcionserviciomedico = descripcionserviciomedico;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public LocalDate getFechaatencion() {
		return fechaatencion;
	}

	public void setFechaatencion(LocalDate fechaatencion) {
		this.fechaatencion = fechaatencion;
	}
	
	
	
}
