package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.pazybien.Model.PresentacionProducto;

@Entity
public class ProductoReport {

	@Id
	private int idproducto;
	
	private String nombreproducto;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fvproducto;
	
	private int cantidadproducto; 
	
	private double pventaproducto;
	
	private double pingresoproducto;
	
	private String marcaproducto;
	
	private String loteproducto;
	
	private String nombrepresentacionproducto;
	
	private String cantidadpresentacionproducto;
	
	private String unidadpresentacionproducto;

	public int getIdproducto() {
		return idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getNombreproducto() {
		return nombreproducto;
	}

	public void setNombreproducto(String nombreproducto) {
		this.nombreproducto = nombreproducto;
	}

	public LocalDate getFvproducto() {
		return fvproducto;
	}

	public void setFvproducto(LocalDate fvproducto) {
		this.fvproducto = fvproducto;
	}

	public int getCantidadproducto() {
		return cantidadproducto;
	}

	public void setCantidadproducto(int cantidadproducto) {
		this.cantidadproducto = cantidadproducto;
	}

	public double getPventaproducto() {
		return pventaproducto;
	}

	public void setPventaproducto(double pventaproducto) {
		this.pventaproducto = pventaproducto;
	}

	public double getPingresoproducto() {
		return pingresoproducto;
	}

	public void setPingresoproducto(double pingresoproducto) {
		this.pingresoproducto = pingresoproducto;
	}

	public String getMarcaproducto() {
		return marcaproducto;
	}

	public void setMarcaproducto(String marcaproducto) {
		this.marcaproducto = marcaproducto;
	}

	public String getLoteproducto() {
		return loteproducto;
	}

	public void setLoteproducto(String loteproducto) {
		this.loteproducto = loteproducto;
	}

	public String getNombrepresentacionproducto() {
		return nombrepresentacionproducto;
	}

	public void setNombrepresentacionproducto(String nombrepresentacionproducto) {
		this.nombrepresentacionproducto = nombrepresentacionproducto;
	}

	public String getCantidadpresentacionproducto() {
		return cantidadpresentacionproducto;
	}

	public void setCantidadpresentacionproducto(String cantidadpresentacionproducto) {
		this.cantidadpresentacionproducto = cantidadpresentacionproducto;
	}

	public String getUnidadpresentacionproducto() {
		return unidadpresentacionproducto;
	}

	public void setUnidadpresentacionproducto(String unidadpresentacionproducto) {
		this.unidadpresentacionproducto = unidadpresentacionproducto;
	}
	
	
}
