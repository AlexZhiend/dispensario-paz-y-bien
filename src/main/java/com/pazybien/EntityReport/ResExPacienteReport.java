package com.pazybien.EntityReport;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ResExPacienteReport {

	@Id
	private int idcomprobantepago;
	
	private String tiphicoo;
	private String tiphicoh;
	private String paratiphicoa;
	private String paratiphicob;
	private String brucella;
	private String obsaf;
	private String nombresyapellidos;
	
	
	private String sudam;
	private String benedict;
	private String sangreoculta;
	private String reacinflamatoria;
	private String obscop;

		
	private String hemoglobina;
	private String hematocrito;
	private String hematies;
	private String leucocitos;
	private String plaquetas;	
	private String tcoagulacion;
	private String tsangria;
	private String vsg;
	private String gruposanguineo;
	private String rh;
	private String obshm;
	private String neotrofilos;
	private String eosinofilos;
	private String basofilos;
	private String monocitos;
	private String linfocitos;
	private String mielositos;
	private String juveniles;
	private String abastonados;
	private String segmentados;
	
	private String color;
	private String aspecto;
	private String densidad;
	private String reaccion;
	private String leucocitosori;
	private String hematiesori;
	private String cepitelial;
	private String cristales;
	private String cilindros;
	private String germenes;
	private String otros;
	private String sangre;
	private String urobilina;
	private String bilirrubina;
	private String proteinas;
	private String nitritos;
	private String cetonas;
	private String glucosa;
	private String obsori;
	
	private String tipo;
	private String leucsec;
	private String hemsec;
	private String cepiteliales;
	private String trichonomavaginalis;
	private String formasfungicas;
	private String germsec;
	private String otrossec;
	private String leucgram;
	private String bacilosgram;
	private String baciloscortosgram;
	private String diplococosgram;
	private String cocosgram;
	private String lactobacilosgram;
	private String otrosgram;
	private String obssec;
	public int getIdcomprobantepago() {
		return idcomprobantepago;
	}
	public void setIdcomprobantepago(int idcomprobantepago) {
		this.idcomprobantepago = idcomprobantepago;
	}
	public String getTiphicoo() {
		return tiphicoo;
	}
	public void setTiphicoo(String tiphicoo) {
		this.tiphicoo = tiphicoo;
	}
	public String getTiphicoh() {
		return tiphicoh;
	}
	public void setTiphicoh(String tiphicoh) {
		this.tiphicoh = tiphicoh;
	}
	public String getParatiphicoa() {
		return paratiphicoa;
	}
	public void setParatiphicoa(String paratiphicoa) {
		this.paratiphicoa = paratiphicoa;
	}
	public String getParatiphicob() {
		return paratiphicob;
	}
	public void setParatiphicob(String paratiphicob) {
		this.paratiphicob = paratiphicob;
	}
	public String getBrucella() {
		return brucella;
	}
	public void setBrucella(String brucella) {
		this.brucella = brucella;
	}
	public String getObsaf() {
		return obsaf;
	}
	public void setObsaf(String obsaf) {
		this.obsaf = obsaf;
	}
	public String getNombresyapellidos() {
		return nombresyapellidos;
	}
	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}
	public String getSudam() {
		return sudam;
	}
	public void setSudam(String sudam) {
		this.sudam = sudam;
	}
	public String getBenedict() {
		return benedict;
	}
	public void setBenedict(String benedict) {
		this.benedict = benedict;
	}
	public String getSangreoculta() {
		return sangreoculta;
	}
	public void setSangreoculta(String sangreoculta) {
		this.sangreoculta = sangreoculta;
	}
	public String getReacinflamatoria() {
		return reacinflamatoria;
	}
	public void setReacinflamatoria(String reacinflamatoria) {
		this.reacinflamatoria = reacinflamatoria;
	}
	public String getObscop() {
		return obscop;
	}
	public void setObscop(String obscop) {
		this.obscop = obscop;
	}
	public String getHemoglobina() {
		return hemoglobina;
	}
	public void setHemoglobina(String hemoglobina) {
		this.hemoglobina = hemoglobina;
	}
	public String getHematocrito() {
		return hematocrito;
	}
	public void setHematocrito(String hematocrito) {
		this.hematocrito = hematocrito;
	}
	public String getHematies() {
		return hematies;
	}
	public void setHematies(String hematies) {
		this.hematies = hematies;
	}
	public String getLeucocitos() {
		return leucocitos;
	}
	public void setLeucocitos(String leucocitos) {
		this.leucocitos = leucocitos;
	}
	public String getPlaquetas() {
		return plaquetas;
	}
	public void setPlaquetas(String plaquetas) {
		this.plaquetas = plaquetas;
	}
	public String getTcoagulacion() {
		return tcoagulacion;
	}
	public void setTcoagulacion(String tcoagulacion) {
		this.tcoagulacion = tcoagulacion;
	}
	public String getTsangria() {
		return tsangria;
	}
	public void setTsangria(String tsangria) {
		this.tsangria = tsangria;
	}
	public String getVsg() {
		return vsg;
	}
	public void setVsg(String vsg) {
		this.vsg = vsg;
	}
	public String getGruposanguineo() {
		return gruposanguineo;
	}
	public void setGruposanguineo(String gruposanguineo) {
		this.gruposanguineo = gruposanguineo;
	}
	public String getRh() {
		return rh;
	}
	public void setRh(String rh) {
		this.rh = rh;
	}
	public String getObshm() {
		return obshm;
	}
	public void setObshm(String obshm) {
		this.obshm = obshm;
	}
	public String getNeotrofilos() {
		return neotrofilos;
	}
	public void setNeotrofilos(String neotrofilos) {
		this.neotrofilos = neotrofilos;
	}
	public String getEosinofilos() {
		return eosinofilos;
	}
	public void setEosinofilos(String eosinofilos) {
		this.eosinofilos = eosinofilos;
	}
	public String getBasofilos() {
		return basofilos;
	}
	public void setBasofilos(String basofilos) {
		this.basofilos = basofilos;
	}
	public String getMonocitos() {
		return monocitos;
	}
	public void setMonocitos(String monocitos) {
		this.monocitos = monocitos;
	}
	public String getLinfocitos() {
		return linfocitos;
	}
	public void setLinfocitos(String linfocitos) {
		this.linfocitos = linfocitos;
	}
	public String getMielositos() {
		return mielositos;
	}
	public void setMielositos(String mielositos) {
		this.mielositos = mielositos;
	}
	public String getJuveniles() {
		return juveniles;
	}
	public void setJuveniles(String juveniles) {
		this.juveniles = juveniles;
	}
	public String getAbastonados() {
		return abastonados;
	}
	public void setAbastonados(String abastonados) {
		this.abastonados = abastonados;
	}
	public String getSegmentados() {
		return segmentados;
	}
	public void setSegmentados(String segmentados) {
		this.segmentados = segmentados;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getAspecto() {
		return aspecto;
	}
	public void setAspecto(String aspecto) {
		this.aspecto = aspecto;
	}
	public String getDensidad() {
		return densidad;
	}
	public void setDensidad(String densidad) {
		this.densidad = densidad;
	}
	public String getReaccion() {
		return reaccion;
	}
	public void setReaccion(String reaccion) {
		this.reaccion = reaccion;
	}
	public String getLeucocitosori() {
		return leucocitosori;
	}
	public void setLeucocitosori(String leucocitosori) {
		this.leucocitosori = leucocitosori;
	}
	public String getHematiesori() {
		return hematiesori;
	}
	public void setHematiesori(String hematiesori) {
		this.hematiesori = hematiesori;
	}
	public String getCepitelial() {
		return cepitelial;
	}
	public void setCepitelial(String cepitelial) {
		this.cepitelial = cepitelial;
	}
	public String getCristales() {
		return cristales;
	}
	public void setCristales(String cristales) {
		this.cristales = cristales;
	}
	public String getCilindros() {
		return cilindros;
	}
	public void setCilindros(String cilindros) {
		this.cilindros = cilindros;
	}
	public String getGermenes() {
		return germenes;
	}
	public void setGermenes(String germenes) {
		this.germenes = germenes;
	}
	public String getOtros() {
		return otros;
	}
	public void setOtros(String otros) {
		this.otros = otros;
	}
	public String getSangre() {
		return sangre;
	}
	public void setSangre(String sangre) {
		this.sangre = sangre;
	}
	public String getUrobilina() {
		return urobilina;
	}
	public void setUrobilina(String urobilina) {
		this.urobilina = urobilina;
	}
	public String getBilirrubina() {
		return bilirrubina;
	}
	public void setBilirrubina(String bilirrubina) {
		this.bilirrubina = bilirrubina;
	}
	public String getProteinas() {
		return proteinas;
	}
	public void setProteinas(String proteinas) {
		this.proteinas = proteinas;
	}
	public String getNitritos() {
		return nitritos;
	}
	public void setNitritos(String nitritos) {
		this.nitritos = nitritos;
	}
	public String getCetonas() {
		return cetonas;
	}
	public void setCetonas(String cetonas) {
		this.cetonas = cetonas;
	}
	public String getGlucosa() {
		return glucosa;
	}
	public void setGlucosa(String glucosa) {
		this.glucosa = glucosa;
	}
	public String getObsori() {
		return obsori;
	}
	public void setObsori(String obsori) {
		this.obsori = obsori;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getLeucsec() {
		return leucsec;
	}
	public void setLeucsec(String leucsec) {
		this.leucsec = leucsec;
	}
	public String getHemsec() {
		return hemsec;
	}
	public void setHemsec(String hemsec) {
		this.hemsec = hemsec;
	}
	public String getCepiteliales() {
		return cepiteliales;
	}
	public void setCepiteliales(String cepiteliales) {
		this.cepiteliales = cepiteliales;
	}
	public String getTrichonomavaginalis() {
		return trichonomavaginalis;
	}
	public void setTrichonomavaginalis(String trichonomavaginalis) {
		this.trichonomavaginalis = trichonomavaginalis;
	}
	public String getFormasfungicas() {
		return formasfungicas;
	}
	public void setFormasfungicas(String formasfungicas) {
		this.formasfungicas = formasfungicas;
	}
	public String getGermsec() {
		return germsec;
	}
	public void setGermsec(String germsec) {
		this.germsec = germsec;
	}
	public String getOtrossec() {
		return otrossec;
	}
	public void setOtrossec(String otrossec) {
		this.otrossec = otrossec;
	}
	public String getLeucgram() {
		return leucgram;
	}
	public void setLeucgram(String leucgram) {
		this.leucgram = leucgram;
	}
	public String getBacilosgram() {
		return bacilosgram;
	}
	public void setBacilosgram(String bacilosgram) {
		this.bacilosgram = bacilosgram;
	}
	public String getBaciloscortosgram() {
		return baciloscortosgram;
	}
	public void setBaciloscortosgram(String baciloscortosgram) {
		this.baciloscortosgram = baciloscortosgram;
	}
	public String getDiplococosgram() {
		return diplococosgram;
	}
	public void setDiplococosgram(String diplococosgram) {
		this.diplococosgram = diplococosgram;
	}
	public String getCocosgram() {
		return cocosgram;
	}
	public void setCocosgram(String cocosgram) {
		this.cocosgram = cocosgram;
	}
	public String getLactobacilosgram() {
		return lactobacilosgram;
	}
	public void setLactobacilosgram(String lactobacilosgram) {
		this.lactobacilosgram = lactobacilosgram;
	}
	public String getOtrosgram() {
		return otrosgram;
	}
	public void setOtrosgram(String otrosgram) {
		this.otrosgram = otrosgram;
	}
	public String getObssec() {
		return obssec;
	}
	public void setObssec(String obssec) {
		this.obssec = obssec;
	}
	
	
}
