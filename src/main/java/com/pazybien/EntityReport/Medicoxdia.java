package com.pazybien.EntityReport;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class Medicoxdia {

	@Id
	private String nombrespersonalmedico;
	
	private BigInteger total;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaexpediciconhistoriaclinica;

	public String getNombrespersonalmedico() {
		return nombrespersonalmedico;
	}

	public void setNombrespersonalmedico(String nombrespersonalmedico) {
		this.nombrespersonalmedico = nombrespersonalmedico;
	}

	public LocalDate getFechaexpediciconhistoriaclinica() {
		return fechaexpediciconhistoriaclinica;
	}

	public void setFechaexpediciconhistoriaclinica(LocalDate fechaexpediciconhistoriaclinica) {
		this.fechaexpediciconhistoriaclinica = fechaexpediciconhistoriaclinica;
	}

	public BigInteger getTotal() {
		return total;
	}

	public void setTotal(BigInteger total) {
		this.total = total;
	}


	
	
}
