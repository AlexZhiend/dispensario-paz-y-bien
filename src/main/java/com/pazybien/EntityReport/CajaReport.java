package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class CajaReport {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ids;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechacomprobante;
	
	private String totalpordia;

	public LocalDate getFechacomprobante() {
		return fechacomprobante;
	}

	public void setFechacomprobante(LocalDate fechacomprobante) {
		this.fechacomprobante = fechacomprobante;
	}



	public String getTotalpordia() {
		return totalpordia;
	}

	public void setTotalpordia(String totalpordia) {
		this.totalpordia = totalpordia;
	}

	public int getIds() {
		return ids;
	}

	public void setIds(int ids) {
		this.ids = ids;
	}

}
