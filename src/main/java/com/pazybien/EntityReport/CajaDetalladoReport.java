package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class CajaDetalladoReport {
	
	@Id
	private int iddetallecomprobante;
	
	private int numerorecibocomprobante;
	
	private String nombrecategoriaexamenmedico;
	
	private double precio;
	
	private double precioserviciomedico;
	
	private String denominacionserviciomedico;

	private double total;

	private double cantidadfarmacia;

	private double cantidadtopico;
	
	private int cantidad;
	
	private int descuento;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fechacomprobante;
	
	private String nombresyapellidos;

	private int estado;

	public int getIddetallecomprobante() {
		return iddetallecomprobante;
	}

	public void setIddetallecomprobante(int iddetallecomprobante) {
		this.iddetallecomprobante = iddetallecomprobante;
	}

	public int getNumerorecibocomprobante() {
		return numerorecibocomprobante;
	}

	public void setNumerorecibocomprobante(int numerorecibocomprobante) {
		this.numerorecibocomprobante = numerorecibocomprobante;
	}

	public String getNombrecategoriaexamenmedico() {
		return nombrecategoriaexamenmedico;
	}

	public void setNombrecategoriaexamenmedico(String nombrecategoriaexamenmedico) {
		this.nombrecategoriaexamenmedico = nombrecategoriaexamenmedico;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getPrecioserviciomedico() {
		return precioserviciomedico;
	}

	public void setPrecioserviciomedico(double precioserviciomedico) {
		this.precioserviciomedico = precioserviciomedico;
	}

	public String getDenominacionserviciomedico() {
		return denominacionserviciomedico;
	}

	public void setDenominacionserviciomedico(String denominacionserviciomedico) {
		this.denominacionserviciomedico = denominacionserviciomedico;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getCantidadfarmacia() {
		return cantidadfarmacia;
	}

	public void setCantidadfarmacia(double cantidadfarmacia) {
		this.cantidadfarmacia = cantidadfarmacia;
	}

	public double getCantidadtopico() {
		return cantidadtopico;
	}

	public void setCantidadtopico(double cantidadtopico) {
		this.cantidadtopico = cantidadtopico;
	}

	public LocalDate getFechacomprobante() {
		return fechacomprobante;
	}

	public void setFechacomprobante(LocalDate fechacomprobante) {
		this.fechacomprobante = fechacomprobante;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	
}
