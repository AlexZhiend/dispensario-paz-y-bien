package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class LabGenReport {

	@Id
	private int idcategoriaexamenmedico;
	
	private String nombrecategoriaexamenmedico;
	private Integer cantidad;
	
	@JsonSerialize(using=ToStringSerializer.class)
	private LocalDate fecha;

	public int getIdcategoriaexamenmedico() {
		return idcategoriaexamenmedico;
	}

	public void setIdcategoriaexamenmedico(int idcategoriaexamenmedico) {
		this.idcategoriaexamenmedico = idcategoriaexamenmedico;
	}

	public String getNombrecategoriaexamenmedico() {
		return nombrecategoriaexamenmedico;
	}

	public void setNombrecategoriaexamenmedico(String nombrecategoriaexamenmedico) {
		this.nombrecategoriaexamenmedico = nombrecategoriaexamenmedico;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	
}
