package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class HistoriaReport {

	@Id
	private int idhistoriaclinica;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaexpediciconhistoriaclinica;
	
	private String serviciomedico;
	
	private int edad;
	
	private String anamnesis;

	private String evaluacion;

	private String diagnostico;
	
	private String telefonopaciente;

	private String tratamiento;

	private String examenes;
	
	private String nombresyapellidos;
	
	private String hcl;
	
	private String direccionpaciente;
	
	private String departamentopaciente;
	
	private String distritopaciente;
	
	private String provinciapaciente;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechanacimientopaciente;
	
	private String ocupacionpaciente;
	
	private String estadocivilpaciente;
	
	private String nombrespersonalmedico;
	
	private String reconsulta;
	
	private String diagnosticor;

	private String tratamientor;

	public int getIdhistoriaclinica() {
		return idhistoriaclinica;
	}

	public void setIdhistoriaclinica(int idhistoriaclinica) {
		this.idhistoriaclinica = idhistoriaclinica;
	}

	public String getServiciomedico() {
		return serviciomedico;
	}

	public void setServiciomedico(String serviciomedico) {
		this.serviciomedico = serviciomedico;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getEvaluacion() {
		return evaluacion;
	}

	public void setEvaluacion(String evaluacion) {
		this.evaluacion = evaluacion;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getExamenes() {
		return examenes;
	}

	public void setExamenes(String examenes) {
		this.examenes = examenes;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public String getHcl() {
		return hcl;
	}

	public void setHcl(String hcl) {
		this.hcl = hcl;
	}

	public String getDireccionpaciente() {
		return direccionpaciente;
	}

	public void setDireccionpaciente(String direccionpaciente) {
		this.direccionpaciente = direccionpaciente;
	}

	public String getDepartamentopaciente() {
		return departamentopaciente;
	}

	public void setDepartamentopaciente(String departamentopaciente) {
		this.departamentopaciente = departamentopaciente;
	}

	public String getDistritopaciente() {
		return distritopaciente;
	}

	public void setDistritopaciente(String distritopaciente) {
		this.distritopaciente = distritopaciente;
	}

	public String getProvinciapaciente() {
		return provinciapaciente;
	}

	public void setProvinciapaciente(String provinciapaciente) {
		this.provinciapaciente = provinciapaciente;
	}

	public String getOcupacionpaciente() {
		return ocupacionpaciente;
	}

	public void setOcupacionpaciente(String ocupacionpaciente) {
		this.ocupacionpaciente = ocupacionpaciente;
	}

	public String getEstadocivilpaciente() {
		return estadocivilpaciente;
	}

	public void setEstadocivilpaciente(String estadocivilpaciente) {
		this.estadocivilpaciente = estadocivilpaciente;
	}

	public String getNombrespersonalmedico() {
		return nombrespersonalmedico;
	}

	public void setNombrespersonalmedico(String nombrespersonalmedico) {
		this.nombrespersonalmedico = nombrespersonalmedico;
	}

	public String getReconsulta() {
		return reconsulta;
	}

	public void setReconsulta(String reconsulta) {
		this.reconsulta = reconsulta;
	}

	public String getDiagnosticor() {
		return diagnosticor;
	}

	public void setDiagnosticor(String diagnosticor) {
		this.diagnosticor = diagnosticor;
	}

	public String getTratamientor() {
		return tratamientor;
	}

	public void setTratamientor(String tratamientor) {
		this.tratamientor = tratamientor;
	}

	public LocalDate getFechaexpediciconhistoriaclinica() {
		return fechaexpediciconhistoriaclinica;
	}

	public void setFechaexpediciconhistoriaclinica(LocalDate fechaexpediciconhistoriaclinica) {
		this.fechaexpediciconhistoriaclinica = fechaexpediciconhistoriaclinica;
	}

	public LocalDate getFechanacimientopaciente() {
		return fechanacimientopaciente;
	}

	public void setFechanacimientopaciente(LocalDate fechanacimientopaciente) {
		this.fechanacimientopaciente = fechanacimientopaciente;
	}

	public String getTelefonopaciente() {
		return telefonopaciente;
	}

	public void setTelefonopaciente(String telefonopaciente) {
		this.telefonopaciente = telefonopaciente;
	}
	
	
}
