package com.pazybien.EntityReport;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
public class ComprobanteReport {
	
	@Id
	private String ids;
	
	private int numerorecibocomprobante;
	
	private double cantidadotros;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechacomprobante;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaatencion;
	
	private double cantidadfarmacia;
	
	private double cantidadtopico;
	
	private int cantidad;
	
	private String denominacionexamenmedico;
	
	private String descripcionserviciomedico;
	
	private String turno;
	
	private double precioexmenmedico;
	
	private int descuento;
	
	private String nombresyapellidos;
	
	private String denominacionserviciomedico;
	
	private double precioserviciomedico;
	
	private double subtotal;
	
	private double totalgeneral;

	public int getNumerorecibocomprobante() {
		return numerorecibocomprobante;
	}

	public void setNumerorecibocomprobante(int numerorecibocomprobante) {
		this.numerorecibocomprobante = numerorecibocomprobante;
	}

	public double getCantidadotros() {
		return cantidadotros;
	}

	public void setCantidadotros(double cantidadotros) {
		this.cantidadotros = cantidadotros;
	}

	public double getCantidadfarmacia() {
		return cantidadfarmacia;
	}

	public void setCantidadfarmacia(double cantidadfarmacia) {
		this.cantidadfarmacia = cantidadfarmacia;
	}

	public double getCantidadtopico() {
		return cantidadtopico;
	}

	public void setCantidadtopico(double cantidadtopico) {
		this.cantidadtopico = cantidadtopico;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDenominacionexamenmedico() {
		return denominacionexamenmedico;
	}

	public void setDenominacionexamenmedico(String denominacionexamenmedico) {
		this.denominacionexamenmedico = denominacionexamenmedico;
	}

	public double getPrecioexmenmedico() {
		return precioexmenmedico;
	}

	public void setPrecioexmenmedico(double precioexmenmedico) {
		this.precioexmenmedico = precioexmenmedico;
	}

	public String getNombresyapellidos() {
		return nombresyapellidos;
	}

	public void setNombresyapellidos(String nombresyapellidos) {
		this.nombresyapellidos = nombresyapellidos;
	}

	public String getDenominacionserviciomedico() {
		return denominacionserviciomedico;
	}

	public void setDenominacionserviciomedico(String denominacionserviciomedico) {
		this.denominacionserviciomedico = denominacionserviciomedico;
	}

	public double getPrecioserviciomedico() {
		return precioserviciomedico;
	}

	public void setPrecioserviciomedico(double precioserviciomedico) {
		this.precioserviciomedico = precioserviciomedico;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotalgeneral() {
		return totalgeneral;
	}

	public void setTotalgeneral(double totalgeneral) {
		this.totalgeneral = totalgeneral;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public LocalDate getFechacomprobante() {
		return fechacomprobante;
	}

	public void setFechacomprobante(LocalDate fechacomprobante) {
		this.fechacomprobante = fechacomprobante;
	}

	public LocalDate getFechaatencion() {
		return fechaatencion;
	}

	public void setFechaatencion(LocalDate fechaatencion) {
		this.fechaatencion = fechaatencion;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getDescripcionserviciomedico() {
		return descripcionserviciomedico;
	}

	public void setDescripcionserviciomedico(String descripcionserviciomedico) {
		this.descripcionserviciomedico = descripcionserviciomedico;
	}

	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}



}
