package com.pazybien.EntityReport;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AtencionLabReport {
	
	@Id
	private int ids;
	
	private int cant;

	public int getIds() {
		return ids;
	}

	public void setIds(int ids) {
		this.ids = ids;
	}

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}
	
	
	
}
